/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.task;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import org.junit.Test;

/**
 * Tests {@link TaskState}.
 */
public class TaskStateTest {
  @Test(expected = NullPointerException.class)
  public void nullState() {
    new TaskState(null, false, null, null);
  }

  @Test
  public void isRunning() {
    final Date completionMoment = new Date();
    final Exception exception = new IllegalStateException();
    assertEquals(false, new TaskState(TaskExecutionState.NEW, false, null, null).isRunning());
    assertEquals(true, new TaskState(TaskExecutionState.RUNNING, false, null, null).isRunning());
    assertEquals(false,
        new TaskState(TaskExecutionState.COMPLETED, false, null, completionMoment).isRunning());
    assertEquals(false,
        new TaskState(TaskExecutionState.COMPLETED, true, null, completionMoment).isRunning());
    assertEquals(false,
        new TaskState(TaskExecutionState.COMPLETED, false, exception, completionMoment)
            .isRunning());
  }

  @Test
  public void isDone() {
    final Date completionMoment = new Date();
    final Exception exception = new IllegalStateException();
    assertEquals(false, new TaskState(TaskExecutionState.NEW, false, null, null).isDone());
    assertEquals(false, new TaskState(TaskExecutionState.RUNNING, false, null, null).isDone());
    assertEquals(true,
        new TaskState(TaskExecutionState.COMPLETED, false, null, completionMoment).isDone());
    assertEquals(true,
        new TaskState(TaskExecutionState.COMPLETED, true, null, completionMoment).isDone());
    assertEquals(true,
        new TaskState(TaskExecutionState.COMPLETED, false, exception, completionMoment).isDone());
  }

  @Test
  public void isCancelled() {
    final Date completionMoment = new Date();
    final Exception exception = new IllegalStateException();
    assertEquals(false, new TaskState(TaskExecutionState.NEW, false, null, null).isCancelled());
    assertEquals(false, new TaskState(TaskExecutionState.RUNNING, false, null, null).isCancelled());
    assertEquals(false,
        new TaskState(TaskExecutionState.COMPLETED, false, null, completionMoment).isCancelled());
    assertEquals(true,
        new TaskState(TaskExecutionState.COMPLETED, true, null, completionMoment).isCancelled());
    assertEquals(false,
        new TaskState(TaskExecutionState.COMPLETED, false, exception, completionMoment)
            .isCancelled());
  }

  @Test
  public void getException() {
    final Date completionMoment = new Date();
    final Exception exception = new IllegalStateException();
    assertEquals(null, new TaskState(TaskExecutionState.NEW, false, null, null).getException());
    assertEquals(null, new TaskState(TaskExecutionState.RUNNING, false, null, null).getException());
    assertEquals(null,
        new TaskState(TaskExecutionState.COMPLETED, false, null, completionMoment).getException());
    assertEquals(null,
        new TaskState(TaskExecutionState.COMPLETED, true, null, completionMoment).getException());
    assertEquals(exception,
        new TaskState(TaskExecutionState.COMPLETED, false, exception, completionMoment)
            .getException());
  }

  @Test
  public void getCompletionMoment() {
    final Date completionMoment = new Date();
    final Exception exception = new IllegalStateException();
    assertEquals(null,
        new TaskState(TaskExecutionState.NEW, false, null, null).getCompletionMoment());
    assertEquals(null,
        new TaskState(TaskExecutionState.RUNNING, false, null, null).getCompletionMoment());
    assertEquals(completionMoment,
        new TaskState(TaskExecutionState.COMPLETED, false, null, completionMoment)
            .getCompletionMoment());
    assertEquals(completionMoment,
        new TaskState(TaskExecutionState.COMPLETED, true, null, completionMoment)
            .getCompletionMoment());
    assertEquals(completionMoment,
        new TaskState(TaskExecutionState.COMPLETED, false, exception, completionMoment)
            .getCompletionMoment());
  }
}
