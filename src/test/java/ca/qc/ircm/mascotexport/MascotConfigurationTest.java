/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport;

import static org.junit.Assert.assertEquals;

import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.inject.Inject;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class MascotConfigurationTest {
  @Inject
  private MascotConfiguration mascotConfiguration;
  @Rule
  public TemporaryFolder temporaryFolder = new TemporaryFolder();
  private Path temporaryHome;
  private Path originalConfiguration;

  @Before
  public void beforeTest() {
    temporaryHome = temporaryFolder.getRoot().toPath();
    originalConfiguration = mascotConfiguration.getConfiguration();
  }

  @After
  public void afterTest() {
    setConfiguration(originalConfiguration);
  }

  private void setConfiguration(Path configuration) {
    ((MascotConfigurationSpringBoot) mascotConfiguration).setConfiguration(configuration);
  }

  @Test
  public void defaultConfiguration() {
    Path userHome = Paths.get(System.getProperty("user.home"));
    assertEquals(userHome.resolve("mascot"), mascotConfiguration.getHome());
    assertEquals(userHome.resolve("mascot").resolve("data"), mascotConfiguration.getData());
    assertEquals(userHome.resolve("mascot").resolve("config").resolve("mascot.dat"),
        mascotConfiguration.getConfiguration());
  }

  @Test
  public void getMinimumPeptideLenghtSummary() throws Throwable {
    Path configuration = temporaryHome.resolve("mascot.dat");
    setConfiguration(configuration);
    Files.copy(Paths.get(getClass().getResource("/mascot.dat").toURI()), configuration);

    int minimumPeptideLenghtSummary = mascotConfiguration.getMinimumPeptideLenghtSummary();

    assertEquals(7, minimumPeptideLenghtSummary);
  }

  @Test
  public void getMinimumPeptideLenghtSummary_MissingConfigurationFile() throws Throwable {
    Path configuration = temporaryHome.resolve("mascot.dat");
    setConfiguration(configuration);

    int minimumPeptideLenghtSummary = mascotConfiguration.getMinimumPeptideLenghtSummary();

    assertEquals(6, minimumPeptideLenghtSummary);
  }

  @Test
  public void getMinimumPeptideLenghtSummary_Invalid() throws Throwable {
    Path configuration = temporaryHome.resolve("mascot.dat");
    setConfiguration(configuration);
    Files.copy(Paths.get(getClass().getResource("/mascot_invalid.dat").toURI()), configuration);

    int minimumPeptideLenghtSummary = mascotConfiguration.getMinimumPeptideLenghtSummary();

    assertEquals(6, minimumPeptideLenghtSummary);
  }

  @Test
  public void getMinimumPeptideLenghtSummary_MissingParameter() throws Throwable {
    Path configuration = temporaryHome.resolve("mascot.dat");
    setConfiguration(configuration);
    Files.copy(Paths.get(getClass().getResource("/mascot_missing.dat").toURI()), configuration);

    int minimumPeptideLenghtSummary = mascotConfiguration.getMinimumPeptideLenghtSummary();

    assertEquals(6, minimumPeptideLenghtSummary);
  }
}
