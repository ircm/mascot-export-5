/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.concurrent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.MascotExportConfiguration;
import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class ExecutorServiceProviderTest {
  private ExecutorServiceProvider provider;
  @Mock
  private MascotExportConfiguration configuration;

  /**
   * Before test.
   */
  @Before
  public void beforeTest() {
    provider = new ExecutorServiceProvider(configuration);
    when(configuration.getThreads()).thenReturn(2);
  }

  @Test
  public void executorService() {
    ExecutorService executorService = provider.executorService();
    assertTrue(executorService instanceof ThreadPoolExecutor);
    ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) executorService;
    assertEquals(2, threadPoolExecutor.getCorePoolSize());
    assertEquals(2, threadPoolExecutor.getMaximumPoolSize());
    assertEquals(0, threadPoolExecutor.getKeepAliveTime(TimeUnit.MILLISECONDS));
  }
}
