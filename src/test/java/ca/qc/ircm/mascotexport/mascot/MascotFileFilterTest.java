/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.mascot;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import java.nio.file.Paths;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class MascotFileFilterTest {
  private MascotFileFilter filter = new MascotFileFilter();

  private MascotFile file(String filename) {
    MascotFile mfile = mock(MascotFile.class);
    mfile.file = filename != null ? Paths.get(filename) : null;
    return mfile;
  }

  private MascotFile inputFile(String filename) {
    MascotFile mfile = mock(MascotFile.class);
    mfile.inputFile = filename;
    return mfile;
  }

  @Test
  public void test_NameContains() {
    filter.nameContains = "chris";

    assertTrue(filter.test(file("chris")));
    assertTrue(filter.test(file("CHRIS")));
    assertTrue(filter.test(file("achris")));
    assertTrue(filter.test(file("ACHRIS")));
    assertTrue(filter.test(file("chrisa")));
    assertTrue(filter.test(file("CHRISA")));
    assertFalse(filter.test(file("test")));
    assertFalse(filter.test(file("TEST")));
    assertFalse(filter.test(file("")));
    assertFalse(filter.test(file(null)));
  }

  @Test
  public void test_ParentContains() {
    filter.parentContains = "chris";

    assertTrue(filter.test(file("chris/1.dat")));
    assertTrue(filter.test(file("CHRIS/1.dat")));
    assertTrue(filter.test(file("achris/1.dat")));
    assertTrue(filter.test(file("ACHRIS/1.dat")));
    assertTrue(filter.test(file("chrisa/1.dat")));
    assertTrue(filter.test(file("CHRISA/1.dat")));
    assertFalse(filter.test(file("test/1.dat")));
    assertFalse(filter.test(file("TEST/1.dat")));
    assertFalse(filter.test(file("test/chris.dat")));
    assertFalse(filter.test(file("TEST/chris.dat")));
    assertFalse(filter.test(file("chris")));
    assertFalse(filter.test(file("CHRIS")));
    assertFalse(filter.test(file("TEST/")));
    assertFalse(filter.test(file("")));
    assertFalse(filter.test(file(null)));
  }

  @Test
  public void test_InputFileContains() {
    filter.inputFileContains = "chris";

    assertTrue(filter.test(inputFile("chris")));
    assertTrue(filter.test(inputFile("CHRIS")));
    assertTrue(filter.test(inputFile("achris")));
    assertTrue(filter.test(inputFile("ACHRIS")));
    assertTrue(filter.test(inputFile("chrisa")));
    assertTrue(filter.test(inputFile("CHRISA")));
    assertFalse(filter.test(inputFile("test")));
    assertFalse(filter.test(inputFile("TEST")));
    assertFalse(filter.test(inputFile(null)));
  }
}
