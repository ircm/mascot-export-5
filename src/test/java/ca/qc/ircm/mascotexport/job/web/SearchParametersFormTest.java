/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.SearchParametersProperties.MASTER;
import static ca.qc.ircm.mascotexport.job.SearchParametersProperties.SHOW_FORMAT;
import static ca.qc.ircm.mascotexport.job.SearchParametersProperties.SHOW_HEADER;
import static ca.qc.ircm.mascotexport.job.SearchParametersProperties.SHOW_MASSES;
import static ca.qc.ircm.mascotexport.job.SearchParametersProperties.SHOW_MODS;
import static ca.qc.ircm.mascotexport.job.SearchParametersProperties.SHOW_PARAMS;
import static ca.qc.ircm.mascotexport.job.web.SearchParametersForm.STYLE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.job.SearchParameters;
import ca.qc.ircm.mascotexport.test.config.AbstractViewTestCase;
import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import ca.qc.ircm.text.MessageResource;
import com.vaadin.flow.data.binder.BinderValidationStatus;
import com.vaadin.flow.i18n.LocaleChangeEvent;
import java.util.Locale;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class SearchParametersFormTest extends AbstractViewTestCase {
  private SearchParametersForm view = new SearchParametersForm();
  private Locale locale = Locale.ENGLISH;
  private MessageResource resources = new MessageResource(SearchParameters.class, locale);
  private SearchParameters parameters = new SearchParameters();

  @Before
  public void beforeTest() {
    when(ui.getLocale()).thenReturn(locale);
    view.localeChange(mock(LocaleChangeEvent.class));
  }

  @Test
  public void styles() {
    assertTrue(view.getContent().hasClassName(STYLE));
    assertTrue(view.master.hasClassName(MASTER));
    assertTrue(view.master.hasClassName("large"));
    assertTrue(view.showHeader.hasClassName(SHOW_HEADER));
    assertTrue(view.showMods.hasClassName(SHOW_MODS));
    assertTrue(view.showParams.hasClassName(SHOW_PARAMS));
    assertTrue(view.showFormat.hasClassName(SHOW_FORMAT));
    assertTrue(view.showMasses.hasClassName(SHOW_MASSES));
  }

  @Test
  public void labels() {
    assertEquals(resources.message(MASTER), view.master.getLabel());
    assertEquals(resources.message(SHOW_HEADER), view.showHeader.getLabel());
    assertEquals(resources.message(SHOW_MODS), view.showMods.getLabel());
    assertEquals(resources.message(SHOW_PARAMS), view.showParams.getLabel());
    assertEquals(resources.message(SHOW_FORMAT), view.showFormat.getLabel());
    assertEquals(resources.message(SHOW_MASSES), view.showMasses.getLabel());
  }

  @Test
  public void values() {
    assertEquals(false, view.master.getValue());
    assertEquals(false, view.showHeader.getValue());
    assertEquals(false, view.showMods.getValue());
    assertEquals(false, view.showParams.getValue());
    assertEquals(false, view.showFormat.getValue());
    assertEquals(false, view.showMasses.getValue());
  }

  @Test
  public void readOnly_Default() {
    assertFalse(view.isReadOnly());

    assertFalse(view.master.isReadOnly());
    assertFalse(view.showHeader.isReadOnly());
    assertFalse(view.showMods.isReadOnly());
    assertFalse(view.showParams.isReadOnly());
    assertFalse(view.showFormat.isReadOnly());
    assertFalse(view.showMasses.isReadOnly());
  }

  @Test
  public void isReadOnly_True() {
    view.setReadOnly(true);

    assertTrue(view.isReadOnly());
  }

  @Test
  public void isReadOnly_False() {
    view.setReadOnly(false);

    assertFalse(view.isReadOnly());
  }

  @Test
  public void setReadOnly_True() {
    view.setReadOnly(true);

    assertTrue(view.master.isReadOnly());
    assertTrue(view.showHeader.isReadOnly());
    assertTrue(view.showMods.isReadOnly());
    assertTrue(view.showParams.isReadOnly());
    assertTrue(view.showFormat.isReadOnly());
    assertTrue(view.showMasses.isReadOnly());
  }

  @Test
  public void setReadOnly_False() {
    view.setReadOnly(false);

    assertFalse(view.master.isReadOnly());
    assertFalse(view.showHeader.isReadOnly());
    assertFalse(view.showMods.isReadOnly());
    assertFalse(view.showParams.isReadOnly());
    assertFalse(view.showFormat.isReadOnly());
    assertFalse(view.showMasses.isReadOnly());
  }

  @Test
  public void getBean() {
    view.setBean(parameters);

    assertSame(parameters, view.getBean());
  }

  @Test
  public void setBean() {
    parameters.setMaster(true);
    parameters.setShowHeader(true);
    parameters.setShowMods(true);
    parameters.setShowParams(true);
    parameters.setShowFormat(true);
    parameters.setShowMasses(true);

    view.setBean(parameters);

    assertEquals(true, view.master.getValue());
    assertEquals(true, view.showHeader.getValue());
    assertEquals(true, view.showMods.getValue());
    assertEquals(true, view.showParams.getValue());
    assertEquals(true, view.showFormat.getValue());
    assertEquals(true, view.showMasses.getValue());
  }

  @Test
  public void setBean_BeforeChangeLocale() {
    parameters.setMaster(true);
    parameters.setShowHeader(true);
    parameters.setShowMods(true);
    parameters.setShowParams(true);
    parameters.setShowFormat(true);
    parameters.setShowMasses(true);

    view.setBean(parameters);
    view.localeChange(mock(LocaleChangeEvent.class));

    assertEquals(true, view.master.getValue());
    assertEquals(true, view.showHeader.getValue());
    assertEquals(true, view.showMods.getValue());
    assertEquals(true, view.showParams.getValue());
    assertEquals(true, view.showFormat.getValue());
    assertEquals(true, view.showMasses.getValue());
  }

  @Test
  public void validate() {
    BinderValidationStatus<SearchParameters> statuses = view.validate();

    assertTrue(statuses.isOk());
  }

  @Test
  public void writeBean() throws Throwable {
    view.master.setValue(true);
    view.showHeader.setValue(true);
    view.showMods.setValue(true);
    view.showParams.setValue(true);
    view.showFormat.setValue(true);
    view.showMasses.setValue(true);

    view.writeBean(parameters);

    assertEquals(true, parameters.isMaster());
    assertEquals(true, parameters.isShowHeader());
    assertEquals(true, parameters.isShowMods());
    assertEquals(true, parameters.isShowParams());
    assertEquals(true, parameters.isShowFormat());
    assertEquals(true, parameters.isShowMasses());
  }
}
