/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.CALC_MR;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.DELTA;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.END;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.EXPECT;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.EXP_MR;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.EXP_Z;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.FRAME;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.HOMOL;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.IDENT;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.MASTER;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.MISS;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.NUM_MATCH;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.SCAN_TITLE;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.SCORE;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.SEQ;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.SHOW_PEP_DUPES;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.SHOW_UNASSIGNED;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.START;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.VAR_MOD;
import static ca.qc.ircm.mascotexport.job.web.PeptideParametersForm.STYLE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.job.PeptideParameters;
import ca.qc.ircm.mascotexport.test.config.AbstractViewTestCase;
import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import ca.qc.ircm.text.MessageResource;
import com.vaadin.flow.data.binder.BinderValidationStatus;
import com.vaadin.flow.i18n.LocaleChangeEvent;
import java.util.Locale;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class PeptideParametersFormTest extends AbstractViewTestCase {
  private PeptideParametersForm view = new PeptideParametersForm();
  private Locale locale = Locale.ENGLISH;
  private MessageResource resources = new MessageResource(PeptideParameters.class, locale);
  private PeptideParameters parameters = new PeptideParameters();

  @Before
  public void beforeTest() {
    when(ui.getLocale()).thenReturn(locale);
    view.localeChange(mock(LocaleChangeEvent.class));
  }

  @Test
  public void styles() {
    assertTrue(view.getContent().hasClassName(STYLE));
    assertTrue(view.master.hasClassName(MASTER));
    assertTrue(view.master.hasClassName("large"));
    assertTrue(view.expMr.hasClassName(EXP_MR));
    assertTrue(view.expZ.hasClassName(EXP_Z));
    assertTrue(view.calcMr.hasClassName(CALC_MR));
    assertTrue(view.delta.hasClassName(DELTA));
    assertTrue(view.start.hasClassName(START));
    assertTrue(view.end.hasClassName(END));
    assertTrue(view.miss.hasClassName(MISS));
    assertTrue(view.score.hasClassName(SCORE));
    assertTrue(view.homol.hasClassName(HOMOL));
    assertTrue(view.ident.hasClassName(IDENT));
    assertTrue(view.expect.hasClassName(EXPECT));
    assertTrue(view.seq.hasClassName(SEQ));
    assertTrue(view.frame.hasClassName(FRAME));
    assertTrue(view.varMod.hasClassName(VAR_MOD));
    assertTrue(view.numMatch.hasClassName(NUM_MATCH));
    assertTrue(view.scanTitle.hasClassName(SCAN_TITLE));
    assertTrue(view.showUnassigned.hasClassName(SHOW_UNASSIGNED));
    assertTrue(view.showPepDupes.hasClassName(SHOW_PEP_DUPES));
  }

  @Test
  public void labels() {
    assertEquals(resources.message(MASTER), view.master.getLabel());
    assertEquals(resources.message(EXP_MR), view.expMr.getLabel());
    assertEquals(resources.message(EXP_Z), view.expZ.getLabel());
    assertEquals(resources.message(CALC_MR), view.calcMr.getLabel());
    assertEquals(resources.message(DELTA), view.delta.getLabel());
    assertEquals(resources.message(START), view.start.getLabel());
    assertEquals(resources.message(END), view.end.getLabel());
    assertEquals(resources.message(MISS), view.miss.getLabel());
    assertEquals(resources.message(SCORE), view.score.getLabel());
    assertEquals(resources.message(HOMOL), view.homol.getLabel());
    assertEquals(resources.message(IDENT), view.ident.getLabel());
    assertEquals(resources.message(EXPECT), view.expect.getLabel());
    assertEquals(resources.message(SEQ), view.seq.getLabel());
    assertEquals(resources.message(FRAME), view.frame.getLabel());
    assertEquals(resources.message(VAR_MOD), view.varMod.getLabel());
    assertEquals(resources.message(NUM_MATCH), view.numMatch.getLabel());
    assertEquals(resources.message(SCAN_TITLE), view.scanTitle.getLabel());
    assertEquals(resources.message(SHOW_UNASSIGNED), view.showUnassigned.getLabel());
    assertEquals(resources.message(SHOW_PEP_DUPES), view.showPepDupes.getLabel());
  }

  @Test
  public void values() {
    assertEquals(false, view.master.getValue());
    assertEquals(false, view.expMr.getValue());
    assertEquals(false, view.expZ.getValue());
    assertEquals(false, view.calcMr.getValue());
    assertEquals(false, view.delta.getValue());
    assertEquals(false, view.start.getValue());
    assertEquals(false, view.end.getValue());
    assertEquals(false, view.miss.getValue());
    assertEquals(false, view.score.getValue());
    assertEquals(false, view.homol.getValue());
    assertEquals(false, view.ident.getValue());
    assertEquals(false, view.expect.getValue());
    assertEquals(false, view.seq.getValue());
    assertEquals(false, view.frame.getValue());
    assertEquals(false, view.varMod.getValue());
    assertEquals(false, view.numMatch.getValue());
    assertEquals(false, view.scanTitle.getValue());
    assertEquals(false, view.showUnassigned.getValue());
    assertEquals(false, view.showPepDupes.getValue());
  }

  @Test
  public void readOnly_Default() {
    assertFalse(view.isReadOnly());

    assertFalse(view.master.isReadOnly());
    assertFalse(view.expMr.isReadOnly());
    assertFalse(view.expZ.isReadOnly());
    assertFalse(view.calcMr.isReadOnly());
    assertFalse(view.delta.isReadOnly());
    assertFalse(view.start.isReadOnly());
    assertFalse(view.end.isReadOnly());
    assertFalse(view.miss.isReadOnly());
    assertFalse(view.score.isReadOnly());
    assertFalse(view.homol.isReadOnly());
    assertFalse(view.ident.isReadOnly());
    assertFalse(view.expect.isReadOnly());
    assertFalse(view.seq.isReadOnly());
    assertFalse(view.frame.isReadOnly());
    assertFalse(view.varMod.isReadOnly());
    assertFalse(view.numMatch.isReadOnly());
    assertFalse(view.scanTitle.isReadOnly());
    assertFalse(view.showUnassigned.isReadOnly());
    assertFalse(view.showPepDupes.isReadOnly());
  }

  @Test
  public void isReadOnly_True() {
    view.setReadOnly(true);

    assertTrue(view.isReadOnly());
  }

  @Test
  public void isReadOnly_False() {
    view.setReadOnly(false);

    assertFalse(view.isReadOnly());
  }

  @Test
  public void setReadOnly_True() {
    view.setReadOnly(true);

    assertTrue(view.master.isReadOnly());
    assertTrue(view.expMr.isReadOnly());
    assertTrue(view.expZ.isReadOnly());
    assertTrue(view.calcMr.isReadOnly());
    assertTrue(view.delta.isReadOnly());
    assertTrue(view.start.isReadOnly());
    assertTrue(view.end.isReadOnly());
    assertTrue(view.miss.isReadOnly());
    assertTrue(view.score.isReadOnly());
    assertTrue(view.homol.isReadOnly());
    assertTrue(view.ident.isReadOnly());
    assertTrue(view.expect.isReadOnly());
    assertTrue(view.seq.isReadOnly());
    assertTrue(view.frame.isReadOnly());
    assertTrue(view.varMod.isReadOnly());
    assertTrue(view.numMatch.isReadOnly());
    assertTrue(view.scanTitle.isReadOnly());
    assertTrue(view.showUnassigned.isReadOnly());
    assertTrue(view.showPepDupes.isReadOnly());
  }

  @Test
  public void setReadOnly_False() {
    view.setReadOnly(false);

    assertFalse(view.master.isReadOnly());
    assertFalse(view.expMr.isReadOnly());
    assertFalse(view.expZ.isReadOnly());
    assertFalse(view.calcMr.isReadOnly());
    assertFalse(view.delta.isReadOnly());
    assertFalse(view.start.isReadOnly());
    assertFalse(view.end.isReadOnly());
    assertFalse(view.miss.isReadOnly());
    assertFalse(view.score.isReadOnly());
    assertFalse(view.homol.isReadOnly());
    assertFalse(view.ident.isReadOnly());
    assertFalse(view.expect.isReadOnly());
    assertFalse(view.seq.isReadOnly());
    assertFalse(view.frame.isReadOnly());
    assertFalse(view.varMod.isReadOnly());
    assertFalse(view.numMatch.isReadOnly());
    assertFalse(view.scanTitle.isReadOnly());
    assertFalse(view.showUnassigned.isReadOnly());
    assertFalse(view.showPepDupes.isReadOnly());
  }

  @Test
  public void getBean() {
    view.setBean(parameters);

    assertSame(parameters, view.getBean());
  }

  @Test
  public void setBean() {
    parameters.setMaster(true);
    parameters.setExpMr(true);
    parameters.setExpZ(true);
    parameters.setCalcMr(true);
    parameters.setDelta(true);
    parameters.setStart(true);
    parameters.setEnd(true);
    parameters.setMiss(true);
    parameters.setScore(true);
    parameters.setHomol(true);
    parameters.setIdent(true);
    parameters.setExpect(true);
    parameters.setSeq(true);
    parameters.setFrame(true);
    parameters.setVarMod(true);
    parameters.setNumMatch(true);
    parameters.setScanTitle(true);
    parameters.setShowUnassigned(true);
    parameters.setShowPepDupes(true);

    view.setBean(parameters);

    assertEquals(true, view.master.getValue());
    assertEquals(true, view.expMr.getValue());
    assertEquals(true, view.expZ.getValue());
    assertEquals(true, view.calcMr.getValue());
    assertEquals(true, view.delta.getValue());
    assertEquals(true, view.start.getValue());
    assertEquals(true, view.end.getValue());
    assertEquals(true, view.miss.getValue());
    assertEquals(true, view.score.getValue());
    assertEquals(true, view.homol.getValue());
    assertEquals(true, view.ident.getValue());
    assertEquals(true, view.expect.getValue());
    assertEquals(true, view.seq.getValue());
    assertEquals(true, view.frame.getValue());
    assertEquals(true, view.varMod.getValue());
    assertEquals(true, view.numMatch.getValue());
    assertEquals(true, view.scanTitle.getValue());
    assertEquals(true, view.showUnassigned.getValue());
    assertEquals(true, view.showPepDupes.getValue());
  }

  @Test
  public void setBean_BeforeChangeLocale() {
    parameters.setMaster(true);
    parameters.setExpMr(true);
    parameters.setExpZ(true);
    parameters.setCalcMr(true);
    parameters.setDelta(true);
    parameters.setStart(true);
    parameters.setEnd(true);
    parameters.setMiss(true);
    parameters.setScore(true);
    parameters.setHomol(true);
    parameters.setIdent(true);
    parameters.setExpect(true);
    parameters.setSeq(true);
    parameters.setFrame(true);
    parameters.setVarMod(true);
    parameters.setNumMatch(true);
    parameters.setScanTitle(true);
    parameters.setShowUnassigned(true);
    parameters.setShowPepDupes(true);

    view.setBean(parameters);
    view.localeChange(mock(LocaleChangeEvent.class));

    assertEquals(true, view.master.getValue());
    assertEquals(true, view.expMr.getValue());
    assertEquals(true, view.expZ.getValue());
    assertEquals(true, view.calcMr.getValue());
    assertEquals(true, view.delta.getValue());
    assertEquals(true, view.start.getValue());
    assertEquals(true, view.end.getValue());
    assertEquals(true, view.miss.getValue());
    assertEquals(true, view.score.getValue());
    assertEquals(true, view.homol.getValue());
    assertEquals(true, view.ident.getValue());
    assertEquals(true, view.expect.getValue());
    assertEquals(true, view.seq.getValue());
    assertEquals(true, view.frame.getValue());
    assertEquals(true, view.varMod.getValue());
    assertEquals(true, view.numMatch.getValue());
    assertEquals(true, view.scanTitle.getValue());
    assertEquals(true, view.showUnassigned.getValue());
    assertEquals(true, view.showPepDupes.getValue());
  }

  @Test
  public void validate() {
    BinderValidationStatus<PeptideParameters> statuses = view.validate();

    assertTrue(statuses.isOk());
  }

  @Test
  public void writeBean() throws Throwable {
    view.master.setValue(true);
    view.expMr.setValue(true);
    view.expZ.setValue(true);
    view.calcMr.setValue(true);
    view.delta.setValue(true);
    view.start.setValue(true);
    view.end.setValue(true);
    view.miss.setValue(true);
    view.score.setValue(true);
    view.homol.setValue(true);
    view.ident.setValue(true);
    view.expect.setValue(true);
    view.seq.setValue(true);
    view.frame.setValue(true);
    view.varMod.setValue(true);
    view.numMatch.setValue(true);
    view.scanTitle.setValue(true);
    view.showUnassigned.setValue(true);
    view.showPepDupes.setValue(true);

    view.writeBean(parameters);

    assertEquals(true, parameters.isMaster());
    assertEquals(true, parameters.isExpMr());
    assertEquals(true, parameters.isExpZ());
    assertEquals(true, parameters.isCalcMr());
    assertEquals(true, parameters.isDelta());
    assertEquals(true, parameters.isStart());
    assertEquals(true, parameters.isEnd());
    assertEquals(true, parameters.isMiss());
    assertEquals(true, parameters.isScore());
    assertEquals(true, parameters.isHomol());
    assertEquals(true, parameters.isIdent());
    assertEquals(true, parameters.isExpect());
    assertEquals(true, parameters.isSeq());
    assertEquals(true, parameters.isFrame());
    assertEquals(true, parameters.isVarMod());
    assertEquals(true, parameters.isNumMatch());
    assertEquals(true, parameters.isScanTitle());
    assertEquals(true, parameters.isShowUnassigned());
    assertEquals(true, parameters.isShowPepDupes());
  }
}
