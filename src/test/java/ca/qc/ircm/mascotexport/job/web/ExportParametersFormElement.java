/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.EXPORT_FORMAT;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.GROUP_FAMILY;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.IGNORE_IONS_SCORE_BELOW;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.PREFER_TAXONOMY;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.REPORT;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.REQUIRE_BOLD_RED;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.SERVER_MUDPIT_SWITCH;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.SHOW_SAME_SETS;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.SHOW_SUBSETS;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.SIG_THRESHOLD;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.USE_HOMOLOGY;
import static ca.qc.ircm.mascotexport.job.web.ExportParametersForm.SERVER_MUDPIT_SWITCH_LABEL;
import static ca.qc.ircm.mascotexport.job.web.ExportParametersForm.USE_HOMOLOGY_LABEL;

import com.vaadin.flow.component.checkbox.testbench.CheckboxElement;
import com.vaadin.flow.component.combobox.testbench.ComboBoxElement;
import com.vaadin.flow.component.html.testbench.DivElement;
import com.vaadin.flow.component.html.testbench.LabelElement;
import com.vaadin.flow.component.radiobutton.testbench.RadioButtonGroupElement;
import com.vaadin.flow.component.textfield.testbench.TextFieldElement;
import com.vaadin.testbench.TestBenchElement;
import com.vaadin.testbench.elementsbase.Element;

@Element("vaadin-vertical-layout")
public class ExportParametersFormElement extends TestBenchElement {
  public ComboBoxElement exportFormat() {
    return $(ComboBoxElement.class).attributeContains("class", EXPORT_FORMAT).first();
  }

  public TextFieldElement sigThreshold() {
    return $(TextFieldElement.class).attributeContains("class", SIG_THRESHOLD).first();
  }

  public TextFieldElement ignoreIonsScoreBelow() {
    return $(TextFieldElement.class).attributeContains("class", IGNORE_IONS_SCORE_BELOW).first();
  }

  public RadioButtonGroupElement useHomology() {
    return $(RadioButtonGroupElement.class).attributeContains("class", USE_HOMOLOGY).first();
  }

  public LabelElement useHomologyLabel() {
    return $(LabelElement.class).attributeContains("class", USE_HOMOLOGY_LABEL).first();
  }

  public TextFieldElement report() {
    return $(TextFieldElement.class).attributeContains("class", REPORT).first();
  }

  public RadioButtonGroupElement serverMudpitSwitch() {
    return $(RadioButtonGroupElement.class).attributeContains("class", SERVER_MUDPIT_SWITCH)
        .first();
  }

  public LabelElement serverMudpitSwitchLabel() {
    return $(LabelElement.class).attributeContains("class", SERVER_MUDPIT_SWITCH_LABEL).first();
  }

  public CheckboxElement showSameSets() {
    return $(CheckboxElement.class).attributeContains("class", SHOW_SAME_SETS).first();
  }

  public TextFieldElement showSubsets() {
    return $(TextFieldElement.class).attributeContains("class", SHOW_SUBSETS).first();
  }

  public CheckboxElement groupFamily() {
    return $(CheckboxElement.class).attributeContains("class", GROUP_FAMILY).first();
  }

  public CheckboxElement requireBoldRed() {
    return $(CheckboxElement.class).attributeContains("class", REQUIRE_BOLD_RED).first();
  }

  public DivElement preferTaxonomy() {
    return $(DivElement.class).attributeContains("class", PREFER_TAXONOMY).first();
  }
}
