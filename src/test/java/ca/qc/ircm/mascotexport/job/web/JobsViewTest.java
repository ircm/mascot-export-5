/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.ExportJobProperties.DATE;
import static ca.qc.ircm.mascotexport.job.ExportJobProperties.DONE_DATE;
import static ca.qc.ircm.mascotexport.job.ExportJobProperties.FILE_PATH;
import static ca.qc.ircm.mascotexport.job.ExportJobProperties.INPUT;
import static ca.qc.ircm.mascotexport.job.web.JobsView.ADD;
import static ca.qc.ircm.mascotexport.job.web.JobsView.EXPORT;
import static ca.qc.ircm.mascotexport.job.web.JobsView.HEADER;
import static ca.qc.ircm.mascotexport.job.web.JobsView.JOBS;
import static ca.qc.ircm.mascotexport.job.web.JobsView.PROGRESS;
import static ca.qc.ircm.mascotexport.job.web.JobsView.REFRESH;
import static ca.qc.ircm.mascotexport.job.web.JobsView.REMOVE;
import static ca.qc.ircm.mascotexport.job.web.JobsView.REMOVE_DONE;
import static ca.qc.ircm.mascotexport.job.web.JobsView.REMOVE_MANY;
import static ca.qc.ircm.mascotexport.job.web.JobsView.VIEW;
import static ca.qc.ircm.mascotexport.job.web.JobsView.VIEW_NAME;
import static ca.qc.ircm.mascotexport.web.WebConstants.APPLICATION_NAME;
import static ca.qc.ircm.mascotexport.web.WebConstants.CANCEL;
import static ca.qc.ircm.mascotexport.web.WebConstants.TITLE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.job.ExportJob;
import ca.qc.ircm.mascotexport.test.config.AbstractViewTestCase;
import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import ca.qc.ircm.mascotexport.web.WebConstants;
import ca.qc.ircm.text.MessageResource;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.Column;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.grid.HeaderRow.HeaderCell;
import com.vaadin.flow.function.ValueProvider;
import com.vaadin.flow.i18n.LocaleChangeEvent;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class JobsViewTest extends AbstractViewTestCase {
  private JobsView view;
  @Mock
  private JobsViewPresenter presenter;
  @Captor
  private ArgumentCaptor<ValueProvider<ExportJob, String>> valueProviderCaptor;
  @Captor
  private ArgumentCaptor<ValueProvider<ExportJob, Button>> buttonProviderCaptor;
  private Locale locale = Locale.ENGLISH;
  private MessageResource resources = new MessageResource(JobsView.class, locale);
  private MessageResource jobResources = new MessageResource(ExportJob.class, locale);
  private MessageResource generalResources = new MessageResource(WebConstants.class, locale);
  private List<ExportJob> jobs = new ArrayList<>();

  /**
   * Before test.
   */
  @Before
  public void beforeTest() throws Throwable {
    when(ui.getLocale()).thenReturn(locale);
    view = new JobsView(presenter);
    ExportJob job = new ExportJob();
    job.setId(UUID.randomUUID().toString());
    job.setFilePath("20181022/F001234.dat");
    job.setDate(LocalDateTime.now().minus(2, ChronoUnit.DAYS));
    jobs.add(job);
    job = new ExportJob();
    job.setId(UUID.randomUUID().toString());
    job.setFilePath("20181022/F001235.dat");
    job.setDate(LocalDateTime.now().minus(2, ChronoUnit.DAYS));
    job.setDoneDate(LocalDateTime.now().minus(2, ChronoUnit.DAYS));
    jobs.add(job);
    job = new ExportJob();
    job.setId(UUID.randomUUID().toString());
    job.setFilePath("20181023/F001245.dat");
    job.setDate(LocalDateTime.now().minus(1, ChronoUnit.DAYS));
    jobs.add(job);
  }

  @SuppressWarnings("unchecked")
  private void mockColumns() {
    view.jobs = mock(Grid.class);
    HeaderRow headerRow = mock(HeaderRow.class);
    when(view.jobs.appendHeaderRow()).thenReturn(headerRow);
    when(headerRow.getCell(any())).thenReturn(mock(HeaderCell.class));
    view.fileColumn = mock(Column.class);
    when(view.jobs.addColumn(any(ValueProvider.class), eq(FILE_PATH))).thenReturn(view.fileColumn);
    when(view.fileColumn.setKey(any())).thenReturn(view.fileColumn);
    when(view.fileColumn.setHeader(any(String.class))).thenReturn(view.fileColumn);
    view.inputColumn = mock(Column.class);
    when(view.jobs.addColumn(any(ValueProvider.class), eq(INPUT))).thenReturn(view.inputColumn);
    when(view.inputColumn.setKey(any())).thenReturn(view.inputColumn);
    when(view.inputColumn.setHeader(any(String.class))).thenReturn(view.inputColumn);
    view.dateColumn = mock(Column.class);
    when(view.jobs.addColumn(any(ValueProvider.class), eq(DATE))).thenReturn(view.dateColumn);
    when(view.dateColumn.setKey(any())).thenReturn(view.dateColumn);
    when(view.dateColumn.setHeader(any(String.class))).thenReturn(view.dateColumn);
    view.doneColumn = mock(Column.class);
    when(view.jobs.addColumn(any(ValueProvider.class), eq(DONE_DATE))).thenReturn(view.doneColumn);
    when(view.doneColumn.setKey(any())).thenReturn(view.doneColumn);
    when(view.doneColumn.setHeader(any(String.class))).thenReturn(view.doneColumn);
    view.viewColumn = mock(Column.class);
    view.removeColumn = mock(Column.class);
    when(view.jobs.addComponentColumn(any(ValueProvider.class))).thenReturn(view.viewColumn,
        view.removeColumn);
    when(view.viewColumn.setKey(any())).thenReturn(view.viewColumn);
    when(view.viewColumn.setHeader(any(String.class))).thenReturn(view.viewColumn);
    when(view.removeColumn.setKey(any())).thenReturn(view.removeColumn);
    when(view.removeColumn.setHeader(any(String.class))).thenReturn(view.removeColumn);
    view.progressColumn = mock(Column.class);
    when(view.jobs.addColumn(any(ValueProvider.class))).thenReturn(view.progressColumn);
    when(view.progressColumn.setKey(any())).thenReturn(view.progressColumn);
    when(view.progressColumn.setHeader(any(String.class))).thenReturn(view.progressColumn);
  }

  @Test
  public void presenter_Init() {
    view.onAttach(mock(AttachEvent.class));
    verify(presenter).init(view);
  }

  @Test
  public void styles() {
    assertTrue(view.getContent().getId().orElse("").equals(VIEW_NAME));
    assertTrue(view.jobs.hasClassName(JOBS));
    assertTrue(view.export.hasClassName(EXPORT));
    assertTrue(view.export.hasThemeName(ButtonVariant.LUMO_PRIMARY.getVariantName()));
    assertTrue(view.cancel.hasClassName(CANCEL));
    assertTrue(view.add.hasClassName(ADD));
    assertTrue(view.add.hasThemeName(ButtonVariant.LUMO_SUCCESS.getVariantName()));
    assertTrue(view.removeMany.hasClassName(REMOVE_MANY));
    assertTrue(view.removeMany.hasThemeName(ButtonVariant.LUMO_ERROR.getVariantName()));
    assertTrue(view.removeDone.hasClassName(REMOVE_DONE));
    assertTrue(view.removeDone.hasThemeName(ButtonVariant.LUMO_ERROR.getVariantName()));
    assertTrue(view.refresh.hasClassName(REFRESH));
    assertTrue(view.refresh.hasThemeName(ButtonVariant.LUMO_TERTIARY.getVariantName()));
  }

  @Test
  public void labels() {
    mockColumns();
    view.initJobs();
    view.localeChange(mock(LocaleChangeEvent.class));
    assertEquals(resources.message(HEADER), view.header.getText());
    assertEquals(resources.message(EXPORT), view.export.getText());
    assertEquals(generalResources.message(CANCEL), view.cancel.getText());
    assertEquals(resources.message(ADD), view.add.getText());
    assertEquals(resources.message(REMOVE_MANY), view.removeMany.getText());
    assertEquals(resources.message(REMOVE_DONE), view.removeDone.getText());
    assertEquals(resources.message(REFRESH), view.refresh.getText());
    verify(view.fileColumn).setHeader(jobResources.message(FILE_PATH));
    verify(view.fileColumn).setFooter(jobResources.message(FILE_PATH));
    verify(view.inputColumn).setHeader(jobResources.message(INPUT));
    verify(view.inputColumn).setFooter(jobResources.message(INPUT));
    verify(view.dateColumn).setHeader(jobResources.message(DATE));
    verify(view.dateColumn).setFooter(jobResources.message(DATE));
    verify(view.doneColumn).setHeader(jobResources.message(DONE_DATE));
    verify(view.doneColumn).setFooter(jobResources.message(DONE_DATE));
    verify(view.viewColumn).setHeader(resources.message(VIEW));
    verify(view.viewColumn).setFooter(resources.message(VIEW));
    verify(view.removeColumn).setHeader(resources.message(REMOVE));
    verify(view.removeColumn).setFooter(resources.message(REMOVE));
    verify(view.progressColumn).setHeader(resources.message(PROGRESS));
    verify(view.progressColumn).setFooter(resources.message(PROGRESS));
  }

  @Test
  public void localeChange() {
    mockColumns();
    view.initJobs();
    view.localeChange(mock(LocaleChangeEvent.class));
    Locale locale = Locale.FRENCH;
    when(ui.getLocale()).thenReturn(locale);
    final MessageResource resources = new MessageResource(JobsView.class, locale);
    final MessageResource jobResources = new MessageResource(ExportJob.class, locale);
    final MessageResource generalResources = new MessageResource(WebConstants.class, locale);
    view.localeChange(mock(LocaleChangeEvent.class));
    assertEquals(resources.message(HEADER), view.header.getText());
    assertEquals(resources.message(EXPORT), view.export.getText());
    assertEquals(generalResources.message(CANCEL), view.cancel.getText());
    assertEquals(resources.message(ADD), view.add.getText());
    assertEquals(resources.message(REMOVE_MANY), view.removeMany.getText());
    assertEquals(resources.message(REMOVE_DONE), view.removeDone.getText());
    assertEquals(resources.message(REFRESH), view.refresh.getText());
    verify(view.fileColumn, atLeastOnce()).setHeader(jobResources.message(FILE_PATH));
    verify(view.fileColumn, atLeastOnce()).setFooter(jobResources.message(FILE_PATH));
    verify(view.inputColumn, atLeastOnce()).setHeader(jobResources.message(INPUT));
    verify(view.inputColumn, atLeastOnce()).setFooter(jobResources.message(INPUT));
    verify(view.dateColumn, atLeastOnce()).setHeader(jobResources.message(DATE));
    verify(view.dateColumn, atLeastOnce()).setFooter(jobResources.message(DATE));
    verify(view.doneColumn, atLeastOnce()).setHeader(jobResources.message(DONE_DATE));
    verify(view.doneColumn, atLeastOnce()).setFooter(jobResources.message(DONE_DATE));
    verify(view.viewColumn, atLeastOnce()).setHeader(resources.message(VIEW));
    verify(view.viewColumn, atLeastOnce()).setFooter(resources.message(VIEW));
    verify(view.removeColumn, atLeastOnce()).setHeader(resources.message(REMOVE));
    verify(view.removeColumn, atLeastOnce()).setFooter(resources.message(REMOVE));
  }

  @Test
  public void getPageTitle() {
    assertEquals(resources.message(TITLE, generalResources.message(APPLICATION_NAME)),
        view.getPageTitle());
  }

  @Test
  public void files_SelectionMode() {
    mockColumns();
    view.initJobs();
    verify(view.jobs).setSelectionMode(SelectionMode.MULTI);
  }

  @Test
  public void files_Columns() {
    view.initJobs();
    assertEquals(7, view.jobs.getColumns().size());
    assertNotNull(view.jobs.getColumnByKey(FILE_PATH));
    assertNotNull(view.jobs.getColumnByKey(INPUT));
    assertNotNull(view.jobs.getColumnByKey(DATE));
    assertNotNull(view.jobs.getColumnByKey(DONE_DATE));
    assertNotNull(view.jobs.getColumnByKey(VIEW));
    assertNotNull(view.jobs.getColumnByKey(REMOVE));
    assertNotNull(view.jobs.getColumnByKey(PROGRESS));
  }

  @Test
  public void files_ColumnsValueProvider() {
    mockColumns();
    view.initJobs();
    final DateTimeFormatter dateFormatter = DateTimeFormatter.ISO_LOCAL_DATE;
    verify(view.jobs).addColumn(valueProviderCaptor.capture(), eq(FILE_PATH));
    ValueProvider<ExportJob, String> valueProvider = valueProviderCaptor.getValue();
    for (ExportJob job : jobs) {
      assertEquals(job.getFilePath(), valueProvider.apply(job));
    }
    verify(view.jobs).addColumn(valueProviderCaptor.capture(), eq(INPUT));
    valueProvider = valueProviderCaptor.getValue();
    for (ExportJob job : jobs) {
      assertEquals(job.getInput(), valueProvider.apply(job));
    }
    verify(view.jobs).addColumn(valueProviderCaptor.capture(), eq(DATE));
    valueProvider = valueProviderCaptor.getValue();
    for (ExportJob job : jobs) {
      assertEquals(dateFormatter.format(job.getDate().toLocalDate()), valueProvider.apply(job));
    }
    verify(view.jobs).addColumn(valueProviderCaptor.capture(), eq(DONE_DATE));
    valueProvider = valueProviderCaptor.getValue();
    for (ExportJob job : jobs) {
      assertEquals(
          job.getDoneDate() != null ? dateFormatter.format(job.getDoneDate().toLocalDate()) : "",
          valueProvider.apply(job));
    }
    verify(view.jobs, times(2)).addComponentColumn(buttonProviderCaptor.capture());
    ValueProvider<ExportJob, Button> buttonProvider = buttonProviderCaptor.getAllValues().get(0);
    verify(presenter, never()).viewButtonListener(any());
    for (ExportJob job : jobs) {
      Button button = buttonProvider.apply(job);
      assertTrue(button.hasClassName(VIEW));
      verify(presenter).viewButtonListener(job);
    }
    buttonProvider = buttonProviderCaptor.getAllValues().get(1);
    verify(presenter, never()).removeButtonListener(any());
    for (ExportJob job : jobs) {
      Button button = buttonProvider.apply(job);
      assertTrue(button.hasClassName(REMOVE));
      verify(presenter).removeButtonListener(job);
    }
    verify(view.jobs).addColumn(valueProviderCaptor.capture());
    when(presenter.progress(any())).thenAnswer(invocation -> {
      ExportJob job = (ExportJob) invocation.getArgument(0);
      return "progress:" + job.getId();
    });
    valueProvider = valueProviderCaptor.getValue();
    for (ExportJob job : jobs) {
      assertEquals("progress:" + job.getId(), valueProvider.apply(job));
      verify(presenter).progress(job);
    }
  }
}
