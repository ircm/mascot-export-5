/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.ProteinParameters.SLOW;
import static ca.qc.ircm.mascotexport.job.ProteinParameters.VERY_SLOW;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.COVER;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.DESC;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.EMPAI;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.LEN;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.MASS;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.MASTER;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.MATCHES;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.PI;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.SCORE;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.SEQ;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.TAX_ID;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.TAX_STR;
import static ca.qc.ircm.mascotexport.job.web.ProteinParametersForm.STYLE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.job.ProteinParameters;
import ca.qc.ircm.mascotexport.test.config.AbstractViewTestCase;
import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import ca.qc.ircm.text.MessageResource;
import com.vaadin.flow.data.binder.BinderValidationStatus;
import com.vaadin.flow.i18n.LocaleChangeEvent;
import java.util.Locale;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class ProteinParametersFormTest extends AbstractViewTestCase {
  private ProteinParametersForm view = new ProteinParametersForm();
  private Locale locale = Locale.ENGLISH;
  private MessageResource resources = new MessageResource(ProteinParameters.class, locale);
  private ProteinParameters parameters = new ProteinParameters();

  @Before
  public void beforeTest() {
    when(ui.getLocale()).thenReturn(locale);
    view.localeChange(mock(LocaleChangeEvent.class));
  }

  @Test
  public void styles() {
    assertTrue(view.getContent().hasClassName(STYLE));
    assertTrue(view.master.hasClassName(MASTER));
    assertTrue(view.master.hasClassName("large"));
    assertTrue(view.score.hasClassName(SCORE));
    assertTrue(view.desc.hasClassName(DESC));
    assertTrue(view.mass.hasClassName(MASS));
    assertTrue(view.matches.hasClassName(MATCHES));
    assertTrue(view.cover.hasClassName(COVER));
    assertTrue(view.len.hasClassName(LEN));
    assertTrue(view.pi.hasClassName(PI));
    assertTrue(view.taxStr.hasClassName(TAX_STR));
    assertTrue(view.taxId.hasClassName(TAX_ID));
    assertTrue(view.seq.hasClassName(SEQ));
    assertTrue(view.empai.hasClassName(EMPAI));
    assertTrue(view.slow.hasClassName(SLOW));
    assertTrue(view.verySlow.hasClassName(VERY_SLOW));
  }

  @Test
  public void labels() {
    assertEquals(resources.message(MASTER), view.master.getLabel());
    assertEquals(resources.message(SCORE), view.score.getLabel());
    assertEquals(resources.message(DESC), view.desc.getLabel());
    assertEquals(resources.message(MASS), view.mass.getLabel());
    assertEquals(resources.message(MATCHES), view.matches.getLabel());
    assertEquals(resources.message(COVER), view.cover.getLabel());
    assertEquals(resources.message(LEN), view.len.getLabel());
    assertEquals(resources.message(PI), view.pi.getLabel());
    assertEquals(resources.message(TAX_STR), view.taxStr.getLabel());
    assertEquals(resources.message(TAX_ID), view.taxId.getLabel());
    assertEquals(resources.message(SEQ), view.seq.getLabel());
    assertEquals(resources.message(EMPAI), view.empai.getLabel());
    assertEquals(resources.message(SLOW), view.slow.getText());
    assertEquals(resources.message(VERY_SLOW), view.verySlow.getText());
  }

  @Test
  public void values() {
    assertEquals(false, view.master.getValue());
    assertEquals(false, view.score.getValue());
    assertEquals(false, view.desc.getValue());
    assertEquals(false, view.mass.getValue());
    assertEquals(false, view.matches.getValue());
    assertEquals(false, view.cover.getValue());
    assertEquals(false, view.len.getValue());
    assertEquals(false, view.pi.getValue());
    assertEquals(false, view.taxStr.getValue());
    assertEquals(false, view.taxId.getValue());
    assertEquals(false, view.seq.getValue());
    assertEquals(false, view.empai.getValue());
  }

  @Test
  public void readOnly_Default() {
    assertFalse(view.isReadOnly());

    assertFalse(view.master.isReadOnly());
    assertFalse(view.score.isReadOnly());
    assertFalse(view.desc.isReadOnly());
    assertFalse(view.mass.isReadOnly());
    assertFalse(view.matches.isReadOnly());
    assertFalse(view.cover.isReadOnly());
    assertFalse(view.len.isReadOnly());
    assertFalse(view.pi.isReadOnly());
    assertFalse(view.taxStr.isReadOnly());
    assertFalse(view.taxId.isReadOnly());
    assertFalse(view.seq.isReadOnly());
    assertFalse(view.empai.isReadOnly());
  }

  @Test
  public void isReadOnly_True() {
    view.setReadOnly(true);

    assertTrue(view.isReadOnly());
  }

  @Test
  public void isReadOnly_False() {
    view.setReadOnly(false);

    assertFalse(view.isReadOnly());
  }

  @Test
  public void setReadOnly_True() {
    view.setReadOnly(true);

    assertTrue(view.master.isReadOnly());
    assertTrue(view.score.isReadOnly());
    assertTrue(view.desc.isReadOnly());
    assertTrue(view.mass.isReadOnly());
    assertTrue(view.matches.isReadOnly());
    assertTrue(view.cover.isReadOnly());
    assertTrue(view.len.isReadOnly());
    assertTrue(view.pi.isReadOnly());
    assertTrue(view.taxStr.isReadOnly());
    assertTrue(view.taxId.isReadOnly());
    assertTrue(view.seq.isReadOnly());
    assertTrue(view.empai.isReadOnly());
  }

  @Test
  public void setReadOnly_False() {
    view.setReadOnly(false);

    assertFalse(view.master.isReadOnly());
    assertFalse(view.score.isReadOnly());
    assertFalse(view.desc.isReadOnly());
    assertFalse(view.mass.isReadOnly());
    assertFalse(view.matches.isReadOnly());
    assertFalse(view.cover.isReadOnly());
    assertFalse(view.len.isReadOnly());
    assertFalse(view.pi.isReadOnly());
    assertFalse(view.taxStr.isReadOnly());
    assertFalse(view.taxId.isReadOnly());
    assertFalse(view.seq.isReadOnly());
    assertFalse(view.empai.isReadOnly());
  }

  @Test
  public void getBean() {
    view.setBean(parameters);

    assertSame(parameters, view.getBean());
  }

  @Test
  public void setBean() {
    parameters.setMaster(true);
    parameters.setScore(true);
    parameters.setDesc(true);
    parameters.setMass(true);
    parameters.setMatches(true);
    parameters.setCover(true);
    parameters.setLen(true);
    parameters.setPi(true);
    parameters.setTaxStr(true);
    parameters.setTaxId(true);
    parameters.setSeq(true);
    parameters.setEmpai(true);

    view.setBean(parameters);

    assertEquals(true, view.master.getValue());
    assertEquals(true, view.score.getValue());
    assertEquals(true, view.desc.getValue());
    assertEquals(true, view.mass.getValue());
    assertEquals(true, view.matches.getValue());
    assertEquals(true, view.cover.getValue());
    assertEquals(true, view.len.getValue());
    assertEquals(true, view.pi.getValue());
    assertEquals(true, view.taxStr.getValue());
    assertEquals(true, view.taxId.getValue());
    assertEquals(true, view.seq.getValue());
    assertEquals(true, view.empai.getValue());
  }

  @Test
  public void setBean_BeforeChangeLocale() {
    parameters.setMaster(true);
    parameters.setScore(true);
    parameters.setDesc(true);
    parameters.setMass(true);
    parameters.setMatches(true);
    parameters.setCover(true);
    parameters.setLen(true);
    parameters.setPi(true);
    parameters.setTaxStr(true);
    parameters.setTaxId(true);
    parameters.setSeq(true);
    parameters.setEmpai(true);

    view.setBean(parameters);
    view.localeChange(mock(LocaleChangeEvent.class));

    assertEquals(true, view.master.getValue());
    assertEquals(true, view.score.getValue());
    assertEquals(true, view.desc.getValue());
    assertEquals(true, view.mass.getValue());
    assertEquals(true, view.matches.getValue());
    assertEquals(true, view.cover.getValue());
    assertEquals(true, view.len.getValue());
    assertEquals(true, view.pi.getValue());
    assertEquals(true, view.taxStr.getValue());
    assertEquals(true, view.taxId.getValue());
    assertEquals(true, view.seq.getValue());
    assertEquals(true, view.empai.getValue());
  }

  @Test
  public void validate() {
    BinderValidationStatus<ProteinParameters> statuses = view.validate();

    assertTrue(statuses.isOk());
  }

  @Test
  public void writeBean() throws Throwable {
    view.master.setValue(true);
    view.score.setValue(true);
    view.desc.setValue(true);
    view.mass.setValue(true);
    view.matches.setValue(true);
    view.cover.setValue(true);
    view.len.setValue(true);
    view.pi.setValue(true);
    view.taxStr.setValue(true);
    view.taxId.setValue(true);
    view.seq.setValue(true);
    view.empai.setValue(true);

    view.writeBean(parameters);

    assertEquals(true, parameters.isMaster());
    assertEquals(true, parameters.isScore());
    assertEquals(true, parameters.isDesc());
    assertEquals(true, parameters.isMass());
    assertEquals(true, parameters.isMatches());
    assertEquals(true, parameters.isCover());
    assertEquals(true, parameters.isLen());
    assertEquals(true, parameters.isPi());
    assertEquals(true, parameters.isTaxStr());
    assertEquals(true, parameters.isTaxId());
    assertEquals(true, parameters.isSeq());
    assertEquals(true, parameters.isEmpai());
  }
}
