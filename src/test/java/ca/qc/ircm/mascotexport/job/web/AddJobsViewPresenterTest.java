/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.ExportFormat.CSV;
import static ca.qc.ircm.mascotexport.job.web.AddJobsView.FILES_EMPTY;
import static ca.qc.ircm.mascotexport.job.web.AddJobsView.LOAD_IO_EXCEPTION;
import static ca.qc.ircm.mascotexport.test.utils.VaadinTestUtils.items;
import static ca.qc.ircm.mascotexport.web.WebConstants.FIELD_NOTIFICATION;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.ProteinScoring;
import ca.qc.ircm.mascotexport.job.ExportJob;
import ca.qc.ircm.mascotexport.job.ExportJobService;
import ca.qc.ircm.mascotexport.job.ExportParameters;
import ca.qc.ircm.mascotexport.job.PeptideParameters;
import ca.qc.ircm.mascotexport.job.ProteinParameters;
import ca.qc.ircm.mascotexport.job.QueryParameters;
import ca.qc.ircm.mascotexport.job.SearchParameters;
import ca.qc.ircm.mascotexport.mascot.MascotFile;
import ca.qc.ircm.mascotexport.mascot.MascotFileFilter;
import ca.qc.ircm.mascotexport.mascot.MascotService;
import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import ca.qc.ircm.mascotexport.web.WebConstants;
import ca.qc.ircm.text.MessageResource;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BinderValidationStatus;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.value.ValueChangeMode;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class AddJobsViewPresenterTest {
  private static final Double DELTA = 0.000000001;
  private AddJobsViewPresenter presenter;
  @Mock
  private AddJobsView view;
  @Mock
  private ExportJobService jobService;
  @Mock
  private MascotService mascotService;
  @Mock
  private ListDataProvider<MascotFile> filesProvider;
  @Mock
  private BinderValidationStatus<ExportParameters> exportStatuses;
  @Mock
  private BinderValidationStatus<SearchParameters> searchStatuses;
  @Mock
  private BinderValidationStatus<ProteinParameters> proteinStatuses;
  @Mock
  private BinderValidationStatus<PeptideParameters> peptideStatuses;
  @Mock
  private BinderValidationStatus<QueryParameters> queryStatuses;
  @Captor
  private ArgumentCaptor<ExportParameters> exportParametersCaptor;
  @Captor
  private ArgumentCaptor<SearchParameters> searchParametersCaptor;
  @Captor
  private ArgumentCaptor<ProteinParameters> proteinParametersCaptor;
  @Captor
  private ArgumentCaptor<PeptideParameters> peptideParametersCaptor;
  @Captor
  private ArgumentCaptor<QueryParameters> queryParametersCaptor;
  @Captor
  private ArgumentCaptor<Collection<ExportJob>> jobsCaptor;
  @Captor
  private ArgumentCaptor<ComponentEventListener<ClickEvent<Button>>> clickListenerCaptor;
  @Captor
  private ArgumentCaptor<ComponentEventListener<AttachEvent>> attachListenerCaptor;
  private Locale locale = Locale.ENGLISH;
  private MessageResource resources = new MessageResource(AddJobsView.class, locale);
  private MessageResource generalResources = new MessageResource(WebConstants.class, locale);
  private List<MascotFile> mascotFiles = new ArrayList<>();

  /**
   * Before test.
   */
  @Before
  public void beforeTest() throws Throwable {
    presenter = new AddJobsViewPresenter(jobService, mascotService);
    mascotFiles.add(new MascotFile(Paths.get("20181011", "F20013.dat"), "VL_20181011_COU_01.RAW"));
    mascotFiles.add(new MascotFile(Paths.get("20181011", "F20014.dat"), "VL_20181011_COU_02.RAW"));
    mascotFiles.add(new MascotFile(Paths.get("20181012", "F20016.dat"), "VL_20181012_COU_01.RAW"));
    mascotFiles.add(new MascotFile(Paths.get("F00116.dat"), "VL_20181012_COU_01.RAW"));
    when(mascotService.allFilesWithInput()).thenReturn(mascotFiles);
    when(view.getLocale()).thenReturn(locale);
    view.header = new H2();
    view.files = new Grid<>();
    view.files.setSelectionMode(SelectionMode.MULTI);
    view.filesError = new Div();
    view.nameFilter = new TextField();
    view.parentFilter = new TextField();
    view.inputFilter = new TextField();
    view.exportParametersForm = mock(ExportParametersForm.class);
    when(view.exportParametersForm.validate()).thenReturn(exportStatuses);
    when(exportStatuses.isOk()).thenReturn(true);
    view.searchParametersForm = mock(SearchParametersForm.class);
    when(view.searchParametersForm.validate()).thenReturn(searchStatuses);
    when(searchStatuses.isOk()).thenReturn(true);
    view.proteinParametersForm = mock(ProteinParametersForm.class);
    when(view.proteinParametersForm.validate()).thenReturn(proteinStatuses);
    when(proteinStatuses.isOk()).thenReturn(true);
    view.peptideParametersForm = mock(PeptideParametersForm.class);
    when(view.peptideParametersForm.validate()).thenReturn(peptideStatuses);
    when(peptideStatuses.isOk()).thenReturn(true);
    view.queryParametersForm = mock(QueryParametersForm.class);
    when(view.queryParametersForm.validate()).thenReturn(queryStatuses);
    when(queryStatuses.isOk()).thenReturn(true);
    view.save = new Button();
    view.cancel = new Button();
  }

  @Test
  public void files() {
    presenter.init(view);
    List<MascotFile> files = items(view.files);
    assertEquals(mascotFiles.size(), files.size());
    for (MascotFile file : mascotFiles) {
      assertTrue(file.toString(), files.contains(file));
    }
    assertEquals(0, view.files.getSelectedItems().size());
    mascotFiles.forEach(file -> view.files.select(file));
    assertEquals(mascotFiles.size(), view.files.getSelectedItems().size());
  }

  @Test
  public void files_IoException() throws Throwable {
    when(mascotService.allFilesWithInput()).thenThrow(new IOException("test"));
    presenter.init(view);

    verify(view).showError(resources.message(LOAD_IO_EXCEPTION));
    List<MascotFile> files = items(view.files);
    assertEquals(0, files.size());
  }

  @Test
  public void files_Empty() {
    presenter.init(view);
    view.files.select(mascotFiles.get(0));
    view.files.deselectAll();

    assertTrue(view.filesError.isVisible());
    assertEquals(resources.message(FILES_EMPTY), view.filesError.getText());
  }

  @Test
  public void files_EmptyOnInit() {
    presenter.init(view);

    assertTrue(view.filesError.isVisible());
    assertEquals(resources.message(FILES_EMPTY), view.filesError.getText());
  }

  @Test
  public void files_NotEmpty() {
    presenter.init(view);
    view.files.select(mascotFiles.get(0));

    assertFalse(view.filesError.isVisible());
  }

  @Test
  public void files_NameFilter() {
    presenter.init(view);
    view.files.setDataProvider(filesProvider);
    String filterValue = "test";
    view.nameFilter.setValue(filterValue);

    verify(filesProvider).refreshAll();
    MascotFileFilter filter = presenter.getFilter();
    assertEquals(filterValue, filter.nameContains);
    assertEquals(ValueChangeMode.EAGER, view.nameFilter.getValueChangeMode());
  }

  @Test
  public void files_ParentFilter() {
    presenter.init(view);
    view.files.setDataProvider(filesProvider);
    String filterValue = "test";
    view.parentFilter.setValue(filterValue);

    verify(filesProvider).refreshAll();
    MascotFileFilter filter = presenter.getFilter();
    assertEquals(filterValue, filter.parentContains);
    assertEquals(ValueChangeMode.EAGER, view.parentFilter.getValueChangeMode());
  }

  @Test
  public void files_InputFilter() {
    presenter.init(view);
    view.files.setDataProvider(filesProvider);
    String filterValue = "test";
    view.inputFilter.setValue(filterValue);

    verify(filesProvider).refreshAll();
    MascotFileFilter filter = presenter.getFilter();
    assertEquals(filterValue, filter.inputFileContains);
    assertEquals(ValueChangeMode.EAGER, view.inputFilter.getValueChangeMode());
  }

  @Test
  public void values() {
    presenter.init(view);
    verify(view.exportParametersForm).setBean(exportParametersCaptor.capture());
    ExportParameters exportParameters = exportParametersCaptor.getValue();
    assertEquals(CSV, exportParameters.getExportFormat());
    assertEquals(0.05, exportParameters.getSigThreshold(), DELTA);
    assertEquals((Integer) 15, exportParameters.getIgnoreIonsScoreBelow());
    assertEquals(false, exportParameters.isUseHomology());
    assertEquals((Integer) 200, exportParameters.getReport());
    assertEquals(ProteinScoring.STANDARD, exportParameters.getServerMudpitSwitch());
    assertEquals(true, exportParameters.isShowSameSets());
    assertEquals((Integer) 1, exportParameters.getShowSubsets());
    assertEquals(false, exportParameters.isGroupFamily());
    assertEquals(true, exportParameters.isRequireBoldRed());
    verify(view.searchParametersForm).setBean(searchParametersCaptor.capture());
    SearchParameters searchParameters = searchParametersCaptor.getValue();
    assertEquals(true, searchParameters.isMaster());
    assertEquals(true, searchParameters.isShowHeader());
    assertEquals(true, searchParameters.isShowMods());
    assertEquals(true, searchParameters.isShowParams());
    assertEquals(true, searchParameters.isShowFormat());
    assertEquals(false, searchParameters.isShowMasses());
    verify(view.proteinParametersForm).setBean(proteinParametersCaptor.capture());
    ProteinParameters proteinParameters = proteinParametersCaptor.getValue();
    assertEquals(true, proteinParameters.isMaster());
    assertEquals(true, proteinParameters.isScore());
    assertEquals(true, proteinParameters.isDesc());
    assertEquals(true, proteinParameters.isMass());
    assertEquals(true, proteinParameters.isMatches());
    assertEquals(true, proteinParameters.isCover());
    assertEquals(true, proteinParameters.isLen());
    assertEquals(true, proteinParameters.isPi());
    assertEquals(true, proteinParameters.isTaxStr());
    assertEquals(true, proteinParameters.isTaxId());
    assertEquals(false, proteinParameters.isSeq());
    assertEquals(true, proteinParameters.isEmpai());
    verify(view.peptideParametersForm).setBean(peptideParametersCaptor.capture());
    PeptideParameters peptideParameters = peptideParametersCaptor.getValue();
    assertEquals(true, peptideParameters.isMaster());
    assertEquals(true, peptideParameters.isExpMr());
    assertEquals(true, peptideParameters.isExpZ());
    assertEquals(true, peptideParameters.isCalcMr());
    assertEquals(true, peptideParameters.isDelta());
    assertEquals(true, peptideParameters.isStart());
    assertEquals(true, peptideParameters.isEnd());
    assertEquals(true, peptideParameters.isMiss());
    assertEquals(true, peptideParameters.isScore());
    assertEquals(false, peptideParameters.isHomol());
    assertEquals(false, peptideParameters.isIdent());
    assertEquals(true, peptideParameters.isExpect());
    assertEquals(true, peptideParameters.isSeq());
    assertEquals(false, peptideParameters.isFrame());
    assertEquals(true, peptideParameters.isVarMod());
    assertEquals(true, peptideParameters.isNumMatch());
    assertEquals(true, peptideParameters.isScanTitle());
    assertEquals(false, peptideParameters.isShowUnassigned());
    assertEquals(false, peptideParameters.isShowPepDupes());
    verify(view.queryParametersForm).setBean(queryParametersCaptor.capture());
    QueryParameters queryParameters = queryParametersCaptor.getValue();
    assertEquals(false, queryParameters.isMaster());
    assertEquals(false, queryParameters.isTitle());
    assertEquals(false, queryParameters.isQualifiers());
    assertEquals(false, queryParameters.isParams());
    assertEquals(false, queryParameters.isPeaks());
    assertEquals(false, queryParameters.isRaw());
  }

  @SuppressWarnings("unchecked")
  private void clickSave() {
    verify(view.save).addClickListener(clickListenerCaptor.capture());
    ComponentEventListener<ClickEvent<Button>> listener = clickListenerCaptor.getValue();
    listener.onComponentEvent(mock(ClickEvent.class));
  }

  @Test
  public void save() throws Throwable {
    view.save = mock(Button.class);
    presenter.init(view);
    mascotFiles.forEach(file -> view.files.select(file));
    clickSave();

    verify(jobService).insert(jobsCaptor.capture());
    List<ExportJob> jobs = new ArrayList<>(jobsCaptor.getValue());
    assertEquals(jobs.size(), mascotFiles.size());
    for (ExportJob job : jobs) {
      verify(view.exportParametersForm).writeBean(job.getExportParameters());
      verify(view.searchParametersForm).writeBean(job.getSearchParameters());
      verify(view.proteinParametersForm).writeBean(job.getProteinParameters());
      verify(view.peptideParametersForm).writeBean(job.getPeptideParameters());
      verify(view.queryParametersForm).writeBean(job.getQueryParameters());
    }
    verify(view).navigate(JobsView.class);
  }

  @Test
  public void save_NoFiles() throws Throwable {
    view.save = mock(Button.class);
    presenter.init(view);
    clickSave();

    verify(view).showError(generalResources.message(FIELD_NOTIFICATION));
  }

  @Test
  public void save_ExportParametersError() throws Throwable {
    when(exportStatuses.isOk()).thenReturn(false);
    when(view.exportParametersForm.validate()).thenReturn(exportStatuses);
    view.save = mock(Button.class);
    presenter.init(view);
    clickSave();

    verify(view).showError(generalResources.message(FIELD_NOTIFICATION));
  }

  @Test
  public void save_SearchParametersError() throws Throwable {
    when(searchStatuses.isOk()).thenReturn(false);
    when(view.searchParametersForm.validate()).thenReturn(searchStatuses);
    view.save = mock(Button.class);
    presenter.init(view);
    clickSave();

    verify(view).showError(generalResources.message(FIELD_NOTIFICATION));
  }

  @Test
  public void save_ProteinParametersError() throws Throwable {
    when(proteinStatuses.isOk()).thenReturn(false);
    when(view.proteinParametersForm.validate()).thenReturn(proteinStatuses);
    view.save = mock(Button.class);
    presenter.init(view);
    clickSave();

    verify(view).showError(generalResources.message(FIELD_NOTIFICATION));
  }

  @Test
  public void save_PeptideParametersError() throws Throwable {
    when(peptideStatuses.isOk()).thenReturn(false);
    when(view.peptideParametersForm.validate()).thenReturn(peptideStatuses);
    view.save = mock(Button.class);
    presenter.init(view);
    clickSave();

    verify(view).showError(generalResources.message(FIELD_NOTIFICATION));
  }

  @Test
  public void save_QueryParametersError() throws Throwable {
    when(queryStatuses.isOk()).thenReturn(false);
    when(view.queryParametersForm.validate()).thenReturn(queryStatuses);
    view.save = mock(Button.class);
    presenter.init(view);
    clickSave();

    verify(view).showError(generalResources.message(FIELD_NOTIFICATION));
  }

  @Test
  @SuppressWarnings("unchecked")
  public void cancel() {
    view.cancel = mock(Button.class);
    presenter.init(view);

    verify(view.cancel).addClickListener(clickListenerCaptor.capture());
    ComponentEventListener<ClickEvent<Button>> listener = clickListenerCaptor.getValue();
    listener.onComponentEvent(mock(ClickEvent.class));
    verify(view).navigate(JobsView.class);
  }
}
