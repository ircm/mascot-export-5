/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job;

import static ca.qc.ircm.mascotexport.job.ExportFormat.CSV;
import static ca.qc.ircm.mascotexport.job.ExportFormat.XML;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.MascotConfiguration;
import ca.qc.ircm.mascotexport.MascotExportConfiguration;
import ca.qc.ircm.mascotexport.mascot.MascotParser;
import ca.qc.ircm.mascotexport.mascot.MascotService;
import ca.qc.ircm.mascotexport.task.TaskFactory;
import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import ca.qc.ircm.progressbar.ProgressBar;
import ca.qc.ircm.progressbar.ProgressBarWithListeners;
import ca.qc.ircm.task.Command;
import ca.qc.ircm.task.Task;
import ca.qc.ircm.task.TaskContext;
import ca.qc.ircm.text.MessageResource;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class ProcessExportJobServiceTest {
  private ProcessExportJobService processExportJobService;
  @Rule
  public TemporaryFolder temporaryFolder = new TemporaryFolder();
  @Mock
  private MascotConfiguration mascotConfiguration;
  @Mock
  private MascotExportConfiguration mascotExportConfiguration;
  @Mock
  private ExportJobService jobService;
  @Mock
  private MascotService mascotService;
  @Mock
  private ExecutorService executorService;
  @Mock
  private TaskFactory taskFactory;
  @Mock
  private RunningExportJobs runningJobs;
  @Mock
  private MascotParser mascotParser;
  @Mock
  private Task task;
  @Mock
  private TaskContext taskContext;
  @Mock
  private ProgressBarWithListeners processJobsProgressBar;
  @Mock
  private ProgressBarWithListeners progressBar;
  @Mock
  private ExportJob job;
  @Mock
  private ExportParameters exportParameters;
  @Mock
  private Map<String, List<String>> parameters;
  @Captor
  private ArgumentCaptor<Command> commandCaptor;
  @Captor
  private ArgumentCaptor<TaskContext> taskContextCaptor;
  private Path home;
  private Path datRoot;
  private Path output;
  private String generatedFileContent = RandomStringUtils.randomAlphabetic(4096);
  private int minimumPeptideLenghtSummary = 8;

  /**
   * Before tests.
   */
  @Before
  @SuppressWarnings("unchecked")
  public void beforeTest() throws Throwable {
    processExportJobService =
        new ProcessExportJobService(jobService, mascotService, executorService, taskFactory,
            runningJobs, mascotConfiguration, mascotExportConfiguration, mascotParser);
    home = temporaryFolder.getRoot().toPath();
    datRoot = Files.createDirectory(home.resolve("dat"));
    output = Files.createDirectory(home.resolve("output"));
    when(job.getExportParameters()).thenReturn(exportParameters);
    when(job.commandLineParameters(any())).thenReturn(parameters);
    when(mascotConfiguration.getData()).thenReturn(datRoot);
    when(mascotConfiguration.getMinimumPeptideLenghtSummary())
        .thenReturn(minimumPeptideLenghtSummary);
    when(mascotExportConfiguration.getOutput()).thenReturn(output);
    doAnswer(new Answer<Void>() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Thread.sleep(500);
        OutputStream output = (OutputStream) invocation.getArguments()[2];
        output.write(generatedFileContent.getBytes("UTF-8"));
        return null;
      }
    }).when(mascotService).export(any(Path.class), any(Map.class), any(OutputStream.class));
    String inputFile = "unit_raw.raw";
    when(mascotParser.parseFilename(any(Path.class))).thenReturn(inputFile);
    when(taskFactory.create(any(Command.class), any(TaskContext.class), any(ExecutorService.class)))
        .then(new Answer<Task>() {
          @Override
          public Task answer(InvocationOnMock invocation) throws Throwable {
            TaskContext taskContext = (TaskContext) invocation.getArguments()[1];
            taskContext.setTask(task);
            return task;
          }
        });
    when(processJobsProgressBar.step(anyDouble())).thenReturn(processJobsProgressBar);
    when(progressBar.step(anyDouble())).thenReturn(progressBar);
  }

  private String getFileContent(Path file) throws IOException {
    return Files.readAllLines(file).stream().collect(Collectors.joining("\n"));
  }

  @Test
  public void process_Jobs_Csv() throws Throwable {
    when(exportParameters.getExportFormat()).thenReturn(CSV);
    when(job.getFilePath()).thenReturn("20110330/F017513.dat");
    Path result = mascotExportConfiguration.getOutput().resolve("20110330/unit_raw__F017513.csv");
    Files.deleteIfExists(result);
    assertFalse(Files.exists(result));

    processExportJobService.process(Arrays.asList(job), Locale.getDefault());

    verify(taskFactory).create(commandCaptor.capture(), taskContextCaptor.capture(),
        eq(executorService));
    verify(runningJobs).add(job, task);
    verify(task).execute();
    commandCaptor.getValue().execute();
    assertTrue(result + " does not exists", Files.exists(result));
    Path jobFile = mascotConfiguration.getData().resolve(job.getFilePath());
    verify(job).commandLineParameters(minimumPeptideLenghtSummary);
    verify(mascotService).export(eq(jobFile), eq(parameters), any(OutputStream.class));
    verify(mascotParser).parseFilename(eq(jobFile));
    verify(jobService).jobDone(job);
    assertEquals(generatedFileContent, getFileContent(result));
    ProgressBar progressBar = taskContextCaptor.getValue().getProgressBar();
    assertNotNull(progressBar.getTitle());
    assertNotNull(progressBar.getMessage());
  }

  @Test
  public void process_Jobs_Xml() throws Throwable {
    when(exportParameters.getExportFormat()).thenReturn(XML);
    when(job.getFilePath()).thenReturn("20110330/F017514.dat");
    Path result = mascotExportConfiguration.getOutput().resolve("20110330/unit_raw__F017514.xml");
    Files.deleteIfExists(result);
    assertFalse(Files.exists(result));

    processExportJobService.process(Arrays.asList(job), Locale.getDefault());

    verify(taskFactory).create(commandCaptor.capture(), taskContextCaptor.capture(),
        eq(executorService));
    verify(runningJobs).add(job, task);
    verify(task).execute();
    commandCaptor.getValue().execute();
    assertTrue(result + " does not exists", Files.exists(result));
    Path jobFile = mascotConfiguration.getData().resolve(job.getFilePath());
    verify(job).commandLineParameters(minimumPeptideLenghtSummary);
    verify(mascotService).export(eq(jobFile), eq(parameters), any(OutputStream.class));
    verify(mascotParser).parseFilename(eq(jobFile));
    verify(jobService).jobDone(job);
    assertEquals(generatedFileContent, getFileContent(result));
    ProgressBar progressBar = taskContextCaptor.getValue().getProgressBar();
    assertNotNull(progressBar.getTitle());
    assertNotNull(progressBar.getMessage());
  }

  @Test
  public void process_Jobs_FileLock() throws Throwable {
    when(exportParameters.getExportFormat()).thenReturn(CSV);
    when(job.getFilePath()).thenReturn("20110330/F017513.dat");
    Path result = mascotExportConfiguration.getOutput().resolve("20110330/unit_raw__F017513.csv");
    Files.deleteIfExists(result);
    assertFalse(Files.exists(result));
    Files.createDirectories(result.getParent());
    Files.createFile(result);

    processExportJobService.process(Arrays.asList(job), Locale.getDefault());

    verify(taskFactory).create(commandCaptor.capture(), any(TaskContext.class),
        eq(executorService));
    verify(runningJobs).add(job, task);
    verify(task).execute();
    try (FileChannel channel = FileChannel.open(result, StandardOpenOption.WRITE)) {
      try (FileLock lock = channel.lock()) {
        try {
          commandCaptor.getValue().execute();
          fail("Expected FileLockException");
        } catch (FileLockException exception) {
          // Ignore.
        }
      }
    }
  }

  @Test
  public void interrupt_Running() throws Throwable {
    when(runningJobs.get(any(ExportJob.class))).thenReturn(task);

    processExportJobService.interrupt(job);

    verify(runningJobs).get(job);
    verify(task).cancel();
  }

  @Test
  public void interrupt_NotRunning() throws Throwable {
    when(runningJobs.get(any(ExportJob.class))).thenReturn(null);

    processExportJobService.interrupt(job);

    verify(runningJobs).get(job);
    verify(task, never()).cancel();
  }

  @Test
  public void interrupt_Null() throws Throwable {
    processExportJobService.interrupt(null);
  }

  @Test
  public void outputLength_Running() throws Throwable {
    long outputLength = 34543L;
    when(runningJobs.get(any(ExportJob.class))).thenReturn(task);
    when(task.getContext()).thenReturn(taskContext);
    when(taskContext.getProgressBar()).thenReturn(processJobsProgressBar);
    MessageResource resources =
        new MessageResource(ProcessExportJobService.class, Locale.getDefault());
    String message = resources.message("data", outputLength / 1024);
    when(processJobsProgressBar.getMessage()).thenReturn(message);

    long actualOutputLength = processExportJobService.outputLength(job);

    assertEquals(outputLength / 1024 * 1024, actualOutputLength);
    verify(runningJobs).get(job);
    verify(task).getContext();
    verify(taskContext).getProgressBar();
    verify(processJobsProgressBar).getMessage();
  }

  @Test
  public void outputLength_InvalidMessage() throws Throwable {
    when(runningJobs.get(any(ExportJob.class))).thenReturn(task);
    when(task.getContext()).thenReturn(taskContext);
    when(taskContext.getProgressBar()).thenReturn(processJobsProgressBar);
    String message = String.valueOf("test");
    when(processJobsProgressBar.getMessage()).thenReturn(message);

    long outputLength = processExportJobService.outputLength(job);

    assertEquals(-1L, outputLength);
    verify(runningJobs).get(job);
    verify(task).getContext();
    verify(taskContext).getProgressBar();
    verify(processJobsProgressBar).getMessage();
  }

  @Test
  public void outputLength_NotRunning() throws Throwable {
    when(runningJobs.get(any(ExportJob.class))).thenReturn(null);

    long outputLength = processExportJobService.outputLength(job);

    verify(runningJobs).get(job);
    assertEquals(-1L, outputLength);
  }

  @Test
  public void outputLength_Null() throws Throwable {
    long outputLength = processExportJobService.outputLength(null);

    assertEquals(-1L, outputLength);
  }
}
