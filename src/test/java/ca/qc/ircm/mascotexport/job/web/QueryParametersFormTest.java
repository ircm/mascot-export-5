/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.QueryParametersProperties.MASTER;
import static ca.qc.ircm.mascotexport.job.QueryParametersProperties.PARAMS;
import static ca.qc.ircm.mascotexport.job.QueryParametersProperties.PEAKS;
import static ca.qc.ircm.mascotexport.job.QueryParametersProperties.QUALIFIERS;
import static ca.qc.ircm.mascotexport.job.QueryParametersProperties.RAW;
import static ca.qc.ircm.mascotexport.job.QueryParametersProperties.TITLE;
import static ca.qc.ircm.mascotexport.job.web.QueryParametersForm.STYLE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.job.QueryParameters;
import ca.qc.ircm.mascotexport.test.config.AbstractViewTestCase;
import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import ca.qc.ircm.text.MessageResource;
import com.vaadin.flow.data.binder.BinderValidationStatus;
import com.vaadin.flow.i18n.LocaleChangeEvent;
import java.util.Locale;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class QueryParametersFormTest extends AbstractViewTestCase {
  private QueryParametersForm view = new QueryParametersForm();
  private Locale locale = Locale.ENGLISH;
  private MessageResource resources = new MessageResource(QueryParameters.class, locale);
  private QueryParameters parameters = new QueryParameters();

  @Before
  public void beforeTest() {
    when(ui.getLocale()).thenReturn(locale);
    view.localeChange(mock(LocaleChangeEvent.class));
  }

  @Test
  public void styles() {
    assertTrue(view.getContent().hasClassName(STYLE));
    assertTrue(view.master.hasClassName(MASTER));
    assertTrue(view.master.hasClassName("large"));
    assertTrue(view.title.hasClassName(TITLE));
    assertTrue(view.qualifiers.hasClassName(QUALIFIERS));
    assertTrue(view.params.hasClassName(PARAMS));
    assertTrue(view.peaks.hasClassName(PEAKS));
    assertTrue(view.raw.hasClassName(RAW));
  }

  @Test
  public void labels() {
    assertEquals(resources.message(MASTER), view.master.getLabel());
    assertEquals(resources.message(TITLE), view.title.getLabel());
    assertEquals(resources.message(QUALIFIERS), view.qualifiers.getLabel());
    assertEquals(resources.message(PARAMS), view.params.getLabel());
    assertEquals(resources.message(PEAKS), view.peaks.getLabel());
    assertEquals(resources.message(RAW), view.raw.getLabel());
  }

  @Test
  public void values() {
    assertEquals(false, view.master.getValue());
    assertEquals(false, view.title.getValue());
    assertEquals(false, view.qualifiers.getValue());
    assertEquals(false, view.params.getValue());
    assertEquals(false, view.peaks.getValue());
    assertEquals(false, view.raw.getValue());
  }

  @Test
  public void readOnly_Default() {
    assertFalse(view.isReadOnly());

    assertFalse(view.master.isReadOnly());
    assertFalse(view.title.isReadOnly());
    assertFalse(view.qualifiers.isReadOnly());
    assertFalse(view.params.isReadOnly());
    assertFalse(view.peaks.isReadOnly());
    assertFalse(view.raw.isReadOnly());
  }

  @Test
  public void isReadOnly_True() {
    view.setReadOnly(true);

    assertTrue(view.isReadOnly());
  }

  @Test
  public void isReadOnly_False() {
    view.setReadOnly(false);

    assertFalse(view.isReadOnly());
  }

  @Test
  public void setReadOnly_True() {
    view.setReadOnly(true);

    assertTrue(view.master.isReadOnly());
    assertTrue(view.title.isReadOnly());
    assertTrue(view.qualifiers.isReadOnly());
    assertTrue(view.params.isReadOnly());
    assertTrue(view.peaks.isReadOnly());
    assertTrue(view.raw.isReadOnly());
  }

  @Test
  public void setReadOnly_False() {
    view.setReadOnly(false);

    assertFalse(view.master.isReadOnly());
    assertFalse(view.title.isReadOnly());
    assertFalse(view.qualifiers.isReadOnly());
    assertFalse(view.params.isReadOnly());
    assertFalse(view.peaks.isReadOnly());
    assertFalse(view.raw.isReadOnly());
  }

  @Test
  public void getBean() {
    view.setBean(parameters);

    assertSame(parameters, view.getBean());
  }

  @Test
  public void setBean() {
    parameters.setMaster(true);
    parameters.setTitle(true);
    parameters.setQualifiers(true);
    parameters.setParams(true);
    parameters.setPeaks(true);
    parameters.setRaw(true);

    view.setBean(parameters);

    assertEquals(true, view.master.getValue());
    assertEquals(true, view.title.getValue());
    assertEquals(true, view.qualifiers.getValue());
    assertEquals(true, view.params.getValue());
    assertEquals(true, view.peaks.getValue());
    assertEquals(true, view.raw.getValue());
  }

  @Test
  public void setBean_BeforeChangeLocale() {
    parameters.setMaster(true);
    parameters.setTitle(true);
    parameters.setQualifiers(true);
    parameters.setParams(true);
    parameters.setPeaks(true);
    parameters.setRaw(true);

    view.setBean(parameters);
    view.localeChange(mock(LocaleChangeEvent.class));

    assertEquals(true, view.master.getValue());
    assertEquals(true, view.title.getValue());
    assertEquals(true, view.qualifiers.getValue());
    assertEquals(true, view.params.getValue());
    assertEquals(true, view.peaks.getValue());
    assertEquals(true, view.raw.getValue());
  }

  @Test
  public void validate() {
    BinderValidationStatus<QueryParameters> statuses = view.validate();

    assertTrue(statuses.isOk());
  }

  @Test
  public void writeBean() throws Throwable {
    view.master.setValue(true);
    view.title.setValue(true);
    view.qualifiers.setValue(true);
    view.params.setValue(true);
    view.peaks.setValue(true);
    view.raw.setValue(true);

    view.writeBean(parameters);

    assertEquals(true, parameters.isMaster());
    assertEquals(true, parameters.isTitle());
    assertEquals(true, parameters.isQualifiers());
    assertEquals(true, parameters.isParams());
    assertEquals(true, parameters.isPeaks());
    assertEquals(true, parameters.isRaw());
  }
}
