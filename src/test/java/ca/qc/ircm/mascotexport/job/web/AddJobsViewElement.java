/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.web.WebConstants.CANCEL;
import static ca.qc.ircm.mascotexport.web.WebConstants.SAVE;

import ca.qc.ircm.mascotexport.test.web.MultiSelectGridElement;
import com.vaadin.flow.component.button.testbench.ButtonElement;
import com.vaadin.flow.component.grid.testbench.GridElement;
import com.vaadin.testbench.TestBenchElement;
import com.vaadin.testbench.elementsbase.Element;

@Element("vaadin-vertical-layout")
public class AddJobsViewElement extends TestBenchElement {
  private static final int NAME_COLUMN = 1;
  private static final int PARENT_COLUMN = 2;
  private static final int INPUT_COLUMN = 3;

  public GridElement jobs() {
    return $(MultiSelectGridElement.class).waitForFirst();
  }

  public String jobName(int row) {
    return jobs().getCell(row, NAME_COLUMN).getText();
  }

  public String jobParent(int row) {
    return jobs().getCell(row, PARENT_COLUMN).getText();
  }

  public String jobInput(int row) {
    return jobs().getCell(row, INPUT_COLUMN).getText();
  }

  public ExportParametersFormElement exportParameters() {
    return $(ExportParametersFormElement.class)
        .attributeContains("class", ExportParametersForm.STYLE).first();
  }

  public SearchParametersFormElement searchParameters() {
    return $(SearchParametersFormElement.class)
        .attributeContains("class", SearchParametersForm.STYLE).first();
  }

  public ProteinParametersFormElement proteinParameters() {
    return $(ProteinParametersFormElement.class)
        .attributeContains("class", ProteinParametersForm.STYLE).first();
  }

  public PeptideParametersFormElement peptideParameters() {
    return $(PeptideParametersFormElement.class)
        .attributeContains("class", PeptideParametersForm.STYLE).first();
  }

  public QueryParametersFormElement queryParameters() {
    return $(QueryParametersFormElement.class).attributeContains("class", QueryParametersForm.STYLE)
        .first();
  }

  public void clickSave() {
    $(ButtonElement.class).attributeContains("class", SAVE).first().click();
  }

  public void clickCancel() {
    $(ButtonElement.class).attributeContains("class", CANCEL).first().click();
  }
}
