/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.SearchParametersProperties.MASTER;
import static ca.qc.ircm.mascotexport.job.SearchParametersProperties.SHOW_FORMAT;
import static ca.qc.ircm.mascotexport.job.SearchParametersProperties.SHOW_HEADER;
import static ca.qc.ircm.mascotexport.job.SearchParametersProperties.SHOW_MASSES;
import static ca.qc.ircm.mascotexport.job.SearchParametersProperties.SHOW_MODS;
import static ca.qc.ircm.mascotexport.job.SearchParametersProperties.SHOW_PARAMS;

import com.vaadin.flow.component.checkbox.testbench.CheckboxElement;
import com.vaadin.testbench.TestBenchElement;
import com.vaadin.testbench.elementsbase.Element;

@Element("vaadin-vertical-layout")
public class SearchParametersFormElement extends TestBenchElement {
  public CheckboxElement master() {
    return $(CheckboxElement.class).attributeContains("class", MASTER).first();
  }

  public CheckboxElement showHeader() {
    return $(CheckboxElement.class).attributeContains("class", SHOW_HEADER).first();
  }

  public CheckboxElement showMods() {
    return $(CheckboxElement.class).attributeContains("class", SHOW_MODS).first();
  }

  public CheckboxElement showParams() {
    return $(CheckboxElement.class).attributeContains("class", SHOW_PARAMS).first();
  }

  public CheckboxElement showFormat() {
    return $(CheckboxElement.class).attributeContains("class", SHOW_FORMAT).first();
  }

  public CheckboxElement showMasses() {
    return $(CheckboxElement.class).attributeContains("class", SHOW_MASSES).first();
  }
}
