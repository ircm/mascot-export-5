/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.ProteinParameters.SLOW;
import static ca.qc.ircm.mascotexport.job.ProteinParameters.VERY_SLOW;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.COVER;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.DESC;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.EMPAI;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.LEN;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.MASS;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.MASTER;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.MATCHES;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.PI;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.SCORE;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.SEQ;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.TAX_ID;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.TAX_STR;

import com.vaadin.flow.component.checkbox.testbench.CheckboxElement;
import com.vaadin.flow.component.html.testbench.DivElement;
import com.vaadin.testbench.TestBenchElement;
import com.vaadin.testbench.elementsbase.Element;

@Element("vaadin-vertical-layout")
public class ProteinParametersFormElement extends TestBenchElement {
  public CheckboxElement master() {
    return $(CheckboxElement.class).attributeContains("class", MASTER).first();
  }

  public CheckboxElement score() {
    return $(CheckboxElement.class).attributeContains("class", SCORE).first();
  }

  public CheckboxElement desc() {
    return $(CheckboxElement.class).attributeContains("class", DESC).first();
  }

  public CheckboxElement mass() {
    return $(CheckboxElement.class).attributeContains("class", MASS).first();
  }

  public CheckboxElement matches() {
    return $(CheckboxElement.class).attributeContains("class", MATCHES).first();
  }

  public CheckboxElement cover() {
    return $(CheckboxElement.class).attributeContains("class", COVER).first();
  }

  public CheckboxElement len() {
    return $(CheckboxElement.class).attributeContains("class", LEN).first();
  }

  public CheckboxElement pi() {
    return $(CheckboxElement.class).attributeContains("class", PI).first();
  }

  public CheckboxElement taxStr() {
    return $(CheckboxElement.class).attributeContains("class", TAX_STR).first();
  }

  public CheckboxElement taxId() {
    return $(CheckboxElement.class).attributeContains("class", TAX_ID).first();
  }

  public CheckboxElement seq() {
    return $(CheckboxElement.class).attributeContains("class", SEQ).first();
  }

  public CheckboxElement empai() {
    return $(CheckboxElement.class).attributeContains("class", EMPAI).first();
  }

  public DivElement slow() {
    return $(DivElement.class).attributeContains("class", SLOW).first();
  }

  public DivElement verySlow() {
    return $(DivElement.class).attributeContains("class", VERY_SLOW).first();
  }
}
