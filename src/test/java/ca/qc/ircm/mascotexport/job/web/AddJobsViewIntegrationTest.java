/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.ExportFormat.CSV;
import static ca.qc.ircm.mascotexport.web.WebConstants.APPLICATION_NAME;
import static org.junit.Assert.assertEquals;

import ca.qc.ircm.mascotexport.MascotConfiguration;
import ca.qc.ircm.mascotexport.ProteinScoring;
import ca.qc.ircm.mascotexport.job.ExportJob;
import ca.qc.ircm.mascotexport.job.ExportJobService;
import ca.qc.ircm.mascotexport.job.ExportParameters;
import ca.qc.ircm.mascotexport.job.PeptideParameters;
import ca.qc.ircm.mascotexport.job.ProteinParameters;
import ca.qc.ircm.mascotexport.job.QueryParameters;
import ca.qc.ircm.mascotexport.job.SearchParameters;
import ca.qc.ircm.mascotexport.test.config.AbstractTestBenchTestCase;
import ca.qc.ircm.mascotexport.test.config.TestBenchTestAnnotations;
import ca.qc.ircm.mascotexport.web.WebConstants;
import ca.qc.ircm.text.MessageResource;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Locale;
import javax.inject.Inject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@TestBenchTestAnnotations
public class AddJobsViewIntegrationTest extends AbstractTestBenchTestCase {
  @Inject
  private ExportJobService exportJobService;
  @Inject
  private MascotConfiguration mascotConfiguration;
  private Path file1;
  private Path file2;
  private Path file3;
  private String input1 = "QE_20181011_ADH_10.raw";
  @SuppressWarnings("unused")
  private String input2 = "VL_20181004_ADH_06.raw";
  private String input3 = "LO_20100705_COU_01.RAW";

  /**
   * Copy Mascot files.
   *
   * @throws Throwable
   *           any exception thrown
   */
  @Before
  public void beforeTest() throws Throwable {
    Path destination = mascotConfiguration.getData();
    Files.createDirectory(destination);
    Path source = Paths.get(getClass().getResource("/").toURI());
    Path source1 = source.resolve("F027889.dat");
    file1 = destination.resolve("20181017").resolve(source1.getFileName());
    Files.createDirectory(file1.getParent());
    Files.copy(source1, file1);
    Path source2 = source.resolve("F027693.dat");
    file2 = destination.resolve("20181009").resolve(source2.getFileName());
    Files.createDirectory(file2.getParent());
    Files.copy(source2, file2);
    Path source3 = source.resolve("F011906.dat");
    file3 = destination.resolve("20181007").resolve(source3.getFileName());
    Files.createDirectory(file3.getParent());
    Files.copy(source3, file3);
  }

  /**
   * Delete Mascot files.
   *
   * @throws Throwable
   *           any exception thrown
   */
  @After
  public void afterTest() throws Throwable {
    Files.deleteIfExists(file1);
    Files.deleteIfExists(file1.getParent());
    Files.deleteIfExists(file2);
    Files.deleteIfExists(file2.getParent());
    Files.deleteIfExists(file3);
    Files.deleteIfExists(file3.getParent());
    Files.deleteIfExists(mascotConfiguration.getData());
  }

  @Test
  public void title() throws Throwable {
    openAddJobsView();
    MessageResource resources = new MessageResource(AddJobsView.class, Locale.getDefault());
    MessageResource generalResources = new MessageResource(WebConstants.class, Locale.getDefault());
    assertEquals(resources.message(WebConstants.TITLE, generalResources.message(APPLICATION_NAME)),
        getDriver().getTitle());
  }

  private void validateExportParameters(ExportParameters expected, ExportParameters actual) {
    assertEquals(expected.getExportFormat(), actual.getExportFormat());
    assertEquals(expected.getSigThreshold(), actual.getSigThreshold());
    assertEquals(expected.getIgnoreIonsScoreBelow(), actual.getIgnoreIonsScoreBelow());
    assertEquals(expected.isUseHomology(), actual.isUseHomology());
    assertEquals(expected.getReport(), actual.getReport());
    assertEquals(expected.getServerMudpitSwitch(), actual.getServerMudpitSwitch());
    assertEquals(expected.isShowSameSets(), actual.isShowSameSets());
    assertEquals(expected.getShowSubsets(), actual.getShowSubsets());
    assertEquals(expected.isGroupFamily(), actual.isGroupFamily());
    assertEquals(expected.isRequireBoldRed(), actual.isRequireBoldRed());
    assertEquals(expected.getPreferTaxonomy(), actual.getPreferTaxonomy());
  }

  private void validateSearchParameters(SearchParameters expected, SearchParameters actual) {
    assertEquals(expected.isMaster(), actual.isMaster());
    assertEquals(expected.isShowHeader(), actual.isShowHeader());
    assertEquals(expected.isShowMods(), actual.isShowMods());
    assertEquals(expected.isShowParams(), actual.isShowParams());
    assertEquals(expected.isShowFormat(), actual.isShowFormat());
    assertEquals(expected.isShowMasses(), actual.isShowMasses());
  }

  private void validateProteinParameters(ProteinParameters expected, ProteinParameters actual) {
    assertEquals(expected.isMaster(), actual.isMaster());
    assertEquals(expected.isScore(), actual.isScore());
    assertEquals(expected.isDesc(), actual.isDesc());
    assertEquals(expected.isMass(), actual.isMass());
    assertEquals(expected.isMatches(), actual.isMatches());
    assertEquals(expected.isCover(), actual.isCover());
    assertEquals(expected.isLen(), actual.isLen());
    assertEquals(expected.isPi(), actual.isPi());
    assertEquals(expected.isTaxStr(), actual.isTaxStr());
    assertEquals(expected.isTaxId(), actual.isTaxId());
    assertEquals(expected.isSeq(), actual.isSeq());
    assertEquals(expected.isEmpai(), actual.isEmpai());
  }

  private void validatePeptideParameters(PeptideParameters expected, PeptideParameters actual) {
    assertEquals(expected.isMaster(), actual.isMaster());
    assertEquals(expected.isExpMr(), actual.isExpMr());
    assertEquals(expected.isExpZ(), actual.isExpZ());
    assertEquals(expected.isCalcMr(), actual.isCalcMr());
    assertEquals(expected.isDelta(), actual.isDelta());
    assertEquals(expected.isStart(), actual.isStart());
    assertEquals(expected.isEnd(), actual.isEnd());
    assertEquals(expected.isMiss(), actual.isMiss());
    assertEquals(expected.isScore(), actual.isScore());
    assertEquals(expected.isHomol(), actual.isHomol());
    assertEquals(expected.isIdent(), actual.isIdent());
    assertEquals(expected.isExpect(), actual.isExpect());
    assertEquals(expected.isSeq(), actual.isSeq());
    assertEquals(expected.isFrame(), actual.isFrame());
    assertEquals(expected.isVarMod(), actual.isVarMod());
    assertEquals(expected.isNumMatch(), actual.isNumMatch());
    assertEquals(expected.isScanTitle(), actual.isScanTitle());
    assertEquals(expected.isShowUnassigned(), actual.isShowUnassigned());
    assertEquals(expected.isShowPepDupes(), actual.isShowPepDupes());
  }

  private void validateQueryParameters(QueryParameters expected, QueryParameters actual) {
    assertEquals(expected.isMaster(), actual.isMaster());
    assertEquals(expected.isTitle(), actual.isTitle());
    assertEquals(expected.isQualifiers(), actual.isQualifiers());
    assertEquals(expected.isParams(), actual.isParams());
    assertEquals(expected.isPeaks(), actual.isPeaks());
    assertEquals(expected.isRaw(), actual.isRaw());
  }

  @Test
  public void save() throws Throwable {
    final List<ExportJob> previousJobs = exportJobService.all();
    AddJobsViewElement jobsView = openAddJobsView();
    jobsView.jobs().select(0);
    jobsView.jobs().select(2);

    jobsView.clickSave();
    Thread.sleep(1000); // TODO Replace by waitForFirst in Vaadin 14.1.

    final ExportParameters exportParameters = new ExportParameters();
    exportParameters.setExportFormat(CSV);
    exportParameters.setSigThreshold(0.05);
    exportParameters.setIgnoreIonsScoreBelow(15);
    exportParameters.setUseHomology(false);
    exportParameters.setReport(200);
    exportParameters.setServerMudpitSwitch(ProteinScoring.STANDARD);
    exportParameters.setShowSameSets(true);
    exportParameters.setShowSubsets(1);
    exportParameters.setGroupFamily(false);
    exportParameters.setRequireBoldRed(true);
    final SearchParameters searchParameters = new SearchParameters();
    searchParameters.setMaster(true);
    searchParameters.setShowHeader(true);
    searchParameters.setShowMods(true);
    searchParameters.setShowParams(true);
    searchParameters.setShowFormat(true);
    searchParameters.setShowMasses(false);
    final ProteinParameters proteinParameters = new ProteinParameters();
    proteinParameters.setMaster(true);
    proteinParameters.setScore(true);
    proteinParameters.setDesc(true);
    proteinParameters.setMass(true);
    proteinParameters.setMatches(true);
    proteinParameters.setCover(true);
    proteinParameters.setLen(true);
    proteinParameters.setPi(true);
    proteinParameters.setTaxStr(true);
    proteinParameters.setTaxId(true);
    proteinParameters.setSeq(false);
    proteinParameters.setEmpai(true);
    final PeptideParameters peptideParameters = new PeptideParameters();
    peptideParameters.setMaster(true);
    peptideParameters.setExpMr(true);
    peptideParameters.setExpZ(true);
    peptideParameters.setCalcMr(true);
    peptideParameters.setDelta(true);
    peptideParameters.setStart(true);
    peptideParameters.setEnd(true);
    peptideParameters.setMiss(true);
    peptideParameters.setScore(true);
    peptideParameters.setHomol(false);
    peptideParameters.setIdent(false);
    peptideParameters.setExpect(true);
    peptideParameters.setSeq(true);
    peptideParameters.setFrame(false);
    peptideParameters.setVarMod(true);
    peptideParameters.setNumMatch(true);
    peptideParameters.setScanTitle(true);
    peptideParameters.setShowUnassigned(false);
    peptideParameters.setShowPepDupes(false);
    final QueryParameters queryParameters = new QueryParameters();
    queryParameters.setMaster(false);
    queryParameters.setTitle(false);
    queryParameters.setQualifiers(false);
    queryParameters.setParams(false);
    queryParameters.setPeaks(false);
    queryParameters.setRaw(false);
    List<ExportJob> jobs = exportJobService.all();
    assertEquals(previousJobs.size() + 2, jobs.size());
    ExportJob job = jobs.get(jobs.size() - 2);
    assertEquals(mascotConfiguration.getData().relativize(file1).toString(), job.getFilePath());
    assertEquals(input1, job.getInput());
    validateExportParameters(exportParameters, job.getExportParameters());
    validateSearchParameters(searchParameters, job.getSearchParameters());
    validateProteinParameters(proteinParameters, job.getProteinParameters());
    validatePeptideParameters(peptideParameters, job.getPeptideParameters());
    validateQueryParameters(queryParameters, job.getQueryParameters());
    job = jobs.get(jobs.size() - 1);
    assertEquals(mascotConfiguration.getData().relativize(file3).toString(), job.getFilePath());
    assertEquals(input3, job.getInput());
  }

  @Test
  public void cancel() throws Throwable {
    AddJobsViewElement jobsView = openAddJobsView();
    jobsView.clickCancel();

    assertEquals(viewUrl(JobsView.VIEW_NAME), getDriver().getCurrentUrl());
  }
}
