/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.web.JobsView.EXPORT_DONE;
import static ca.qc.ircm.mascotexport.job.web.JobsView.JOBS_EMPTY;
import static ca.qc.ircm.mascotexport.test.utils.VaadinTestUtils.items;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.job.ExportJob;
import ca.qc.ircm.mascotexport.job.ExportJobService;
import ca.qc.ircm.mascotexport.job.ProcessExportJobService;
import ca.qc.ircm.mascotexport.job.RunningExportJobs;
import ca.qc.ircm.mascotexport.test.config.AbstractViewTestCase;
import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import ca.qc.ircm.progressbar.ProgressBar;
import ca.qc.ircm.task.Task;
import ca.qc.ircm.task.TaskContext;
import ca.qc.ircm.text.MessageResource;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.Column;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.html.H1;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class JobsViewPresenterTest extends AbstractViewTestCase {
  private JobsViewPresenter presenter;
  @Mock
  private JobsView view;
  @Mock
  private ExportJobService jobService;
  @Mock
  private ProcessExportJobService processJobService;
  @Mock
  private RunningExportJobs runningJobs;
  @Mock
  private Task task;
  @Mock
  private TaskContext taskContext;
  @Mock
  private ProgressBar progressBar;
  @Captor
  private ArgumentCaptor<Collection<ExportJob>> jobsCaptor;
  @Captor
  private ArgumentCaptor<ComponentEventListener<ClickEvent<Button>>> clickListenerCaptor;
  private List<ExportJob> jobs = new ArrayList<>();
  private Locale locale = Locale.ENGLISH;
  private MessageResource resources = new MessageResource(JobsView.class, locale);

  /**
   * Before test.
   */
  @Before
  @SuppressWarnings("unchecked")
  public void beforeTest() {
    presenter = new JobsViewPresenter(jobService, processJobService, runningJobs);
    ExportJob job = new ExportJob();
    job.setFilePath("20181022/F001234.dat");
    job.setDate(LocalDateTime.now().minus(2, ChronoUnit.DAYS));
    jobs.add(job);
    job = new ExportJob();
    job.setFilePath("20181022/F001235.dat");
    job.setDate(LocalDateTime.now().minus(2, ChronoUnit.DAYS));
    job.setDoneDate(LocalDateTime.now().minus(2, ChronoUnit.DAYS));
    jobs.add(job);
    job = new ExportJob();
    job.setFilePath("20181023/F001245.dat");
    job.setDate(LocalDateTime.now().minus(1, ChronoUnit.DAYS));
    jobs.add(job);
    when(jobService.all()).thenReturn(jobs);
    view.header = new H1();
    view.jobs = new Grid<>();
    view.jobs.setSelectionMode(SelectionMode.MULTI);
    view.fileColumn = mock(Column.class);
    when(view.fileColumn.setHeader(any(String.class))).thenReturn(view.fileColumn);
    view.inputColumn = mock(Column.class);
    when(view.inputColumn.setHeader(any(String.class))).thenReturn(view.inputColumn);
    view.dateColumn = mock(Column.class);
    when(view.dateColumn.setHeader(any(String.class))).thenReturn(view.dateColumn);
    view.doneColumn = mock(Column.class);
    when(view.doneColumn.setHeader(any(String.class))).thenReturn(view.doneColumn);
    view.removeColumn = mock(Column.class);
    when(view.removeColumn.setHeader(any(String.class))).thenReturn(view.removeColumn);
    view.viewColumn = mock(Column.class);
    when(view.viewColumn.setHeader(any(String.class))).thenReturn(view.viewColumn);
    view.export = new Button();
    view.cancel = new Button();
    view.add = new Button();
    view.removeMany = new Button();
    view.removeDone = new Button();
    view.refresh = new Button();
    when(view.getLocale()).thenReturn(locale);
    when(runningJobs.get(any())).thenReturn(task);
    when(task.getContext()).thenReturn(taskContext);
    when(taskContext.getProgressBar()).thenReturn(progressBar);
  }

  @Test
  public void jobs() {
    presenter.init(view);
    List<ExportJob> jobs = items(view.jobs);
    assertEquals(this.jobs.size(), jobs.size());
    for (ExportJob job : this.jobs) {
      assertTrue(job.toString(), jobs.contains(job));
    }
    assertEquals(0, view.jobs.getSelectedItems().size());
    jobs.forEach(job -> view.jobs.select(job));
    assertEquals(jobs.size(), view.jobs.getSelectedItems().size());
  }

  @Test
  public void removeOldJobs() {
    presenter.init(view);
    verify(runningJobs).removeOldJobs();
  }

  @SuppressWarnings("unchecked")
  private void click(Button mockedButton) {
    verify(mockedButton).addClickListener(clickListenerCaptor.capture());
    ComponentEventListener<ClickEvent<Button>> listener = clickListenerCaptor.getValue();
    listener.onComponentEvent(mock(ClickEvent.class));
  }

  @Test
  public void export() {
    view.export = mock(Button.class);
    presenter.init(view);
    jobs.forEach(job -> view.jobs.select(job));
    click(view.export);

    verify(processJobService).process(jobsCaptor.capture(), eq(locale));
    Collection<ExportJob> jobs = jobsCaptor.getValue();
    assertEquals(this.jobs.size(), jobs.size());
    for (ExportJob job : this.jobs) {
      assertTrue(jobs.contains(job));
    }
    verify(view).showNotification(resources.message(EXPORT_DONE, jobs.size()));
  }

  @Test
  public void export_NoJob() {
    view.export = mock(Button.class);
    presenter.init(view);
    click(view.export);

    verify(view).showNotification(resources.message(JOBS_EMPTY));
  }

  @Test
  public void cancel() {
    when(runningJobs.get(any())).thenReturn(null, task, null);
    view.cancel = mock(Button.class);
    presenter.init(view);
    jobs.forEach(job -> view.jobs.select(job));
    click(view.cancel);

    for (ExportJob job : this.jobs) {
      verify(runningJobs).get(job);
    }
    verify(task).cancel();
  }

  @Test
  public void add() {
    view.add = mock(Button.class);
    presenter.init(view);
    click(view.add);

    verify(view).navigate(AddJobsView.class);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void viewButtonListener() {
    presenter.init(view);
    ExportJob job = jobs.get(0);
    when(jobService.all()).thenReturn(jobs, Collections.emptyList());
    ComponentEventListener<ClickEvent<Button>> listener = presenter.viewButtonListener(job);
    listener.onComponentEvent(mock(ClickEvent.class));

    Dialog dialog = testOpenDialog();
    assertTrue(dialog instanceof JobParametersDialog);
    JobParametersDialog jobDialog = (JobParametersDialog) dialog;
    assertEquals(job, jobDialog.getBean());
    assertTrue(jobDialog.isReadOnly());
  }

  @Test
  @SuppressWarnings("unchecked")
  public void removeButtonListener() {
    when(jobService.all()).thenReturn(jobs, Collections.emptyList());
    presenter.init(view);
    ExportJob job = jobs.get(0);
    ComponentEventListener<ClickEvent<Button>> listener = presenter.removeButtonListener(job);
    listener.onComponentEvent(mock(ClickEvent.class));

    verify(jobService).delete(job);
    verify(jobService, times(2)).all();
    List<ExportJob> jobs = items(view.jobs);
    assertEquals(0, jobs.size());
  }

  @Test
  public void progress() {
    when(progressBar.getMessage()).thenReturn("123 KB", "Waiting");
    presenter.init(view);
    assertEquals("123 KB", presenter.progress(jobs.get(0)));
    assertEquals("Waiting", presenter.progress(jobs.get(1)));
    verify(progressBar, times(2)).getMessage();
  }

  @Test
  @SuppressWarnings("unchecked")
  public void removeMany() {
    view.removeMany = mock(Button.class);
    when(jobService.all()).thenReturn(jobs, Collections.emptyList());
    presenter.init(view);
    view.jobs.select(jobs.get(0));
    view.jobs.select(jobs.get(2));
    click(view.removeMany);

    verify(jobService).delete(jobsCaptor.capture());
    Collection<ExportJob> deletedJobs = jobsCaptor.getValue();
    assertEquals(2, deletedJobs.size());
    assertTrue(deletedJobs.contains(jobs.get(0)));
    assertFalse(deletedJobs.contains(jobs.get(1)));
    assertTrue(deletedJobs.contains(jobs.get(2)));
    verify(jobService, times(2)).all();
    List<ExportJob> jobs = items(view.jobs);
    assertEquals(0, jobs.size());
  }

  @Test
  public void removeMany_NoJob() {
    view.removeMany = mock(Button.class);
    presenter.init(view);
    click(view.removeMany);

    verify(view).showNotification(resources.message(JOBS_EMPTY));
  }

  @Test
  @SuppressWarnings("unchecked")
  public void removeDone() {
    view.removeDone = mock(Button.class);
    when(jobService.all()).thenReturn(jobs, Collections.emptyList());
    presenter.init(view);
    click(view.removeDone);

    verify(jobService).delete(jobsCaptor.capture());
    Collection<ExportJob> deletedJobs = jobsCaptor.getValue();
    assertEquals(1, deletedJobs.size());
    assertFalse(deletedJobs.contains(jobs.get(0)));
    assertTrue(deletedJobs.contains(jobs.get(1)));
    assertFalse(deletedJobs.contains(jobs.get(2)));
    verify(jobService, times(2)).all();
    List<ExportJob> jobs = items(view.jobs);
    assertEquals(0, jobs.size());
  }

  @Test
  @SuppressWarnings("unchecked")
  public void refresh() {
    view.refresh = mock(Button.class);
    when(jobService.all()).thenReturn(jobs, Collections.emptyList());
    presenter.init(view);
    click(view.refresh);

    verify(jobService, times(2)).all();
    List<ExportJob> jobs = items(view.jobs);
    assertEquals(0, jobs.size());
  }
}
