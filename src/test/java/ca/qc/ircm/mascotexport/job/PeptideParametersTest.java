/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class PeptideParametersTest {
  private PeptideParameters peptideParameters = new PeptideParameters();

  @Test
  public void commandLineParameters_AllFalse() {
    Map<String, List<String>> parameters = peptideParameters.commandLineParameters(true);

    assertEquals(0, parameters.size());
  }

  @Test
  public void commandLineParameters_AllTrue() {
    peptideParameters.setMaster(true);
    peptideParameters.setExpMr(true);
    peptideParameters.setExpZ(true);
    peptideParameters.setCalcMr(true);
    peptideParameters.setDelta(true);
    peptideParameters.setStart(true);
    peptideParameters.setEnd(true);
    peptideParameters.setMiss(true);
    peptideParameters.setScore(true);
    peptideParameters.setHomol(true);
    peptideParameters.setIdent(true);
    peptideParameters.setExpect(true);
    peptideParameters.setSeq(true);
    peptideParameters.setFrame(true);
    peptideParameters.setVarMod(true);
    peptideParameters.setNumMatch(true);
    peptideParameters.setScanTitle(true);
    peptideParameters.setShowUnassigned(true);
    peptideParameters.setShowPepDupes(true);

    Map<String, List<String>> parameters = peptideParameters.commandLineParameters(true);

    assertEquals(19, parameters.size());
    assertTrue(parameters.containsKey("peptide_master"));
    assertEquals(1, parameters.get("peptide_master").size());
    assertEquals("1", parameters.get("peptide_master").get(0));
    assertTrue(parameters.containsKey("pep_exp_mr"));
    assertEquals(1, parameters.get("pep_exp_mr").size());
    assertEquals("1", parameters.get("pep_exp_mr").get(0));
    assertTrue(parameters.containsKey("pep_exp_z"));
    assertEquals(1, parameters.get("pep_exp_z").size());
    assertEquals("1", parameters.get("pep_exp_z").get(0));
    assertTrue(parameters.containsKey("pep_calc_mr"));
    assertEquals(1, parameters.get("pep_calc_mr").size());
    assertEquals("1", parameters.get("pep_calc_mr").get(0));
    assertTrue(parameters.containsKey("pep_delta"));
    assertEquals(1, parameters.get("pep_delta").size());
    assertEquals("1", parameters.get("pep_delta").get(0));
    assertTrue(parameters.containsKey("pep_start"));
    assertEquals(1, parameters.get("pep_start").size());
    assertEquals("1", parameters.get("pep_start").get(0));
    assertTrue(parameters.containsKey("pep_end"));
    assertEquals(1, parameters.get("pep_end").size());
    assertEquals("1", parameters.get("pep_end").get(0));
    assertTrue(parameters.containsKey("pep_miss"));
    assertEquals(1, parameters.get("pep_miss").size());
    assertEquals("1", parameters.get("pep_miss").get(0));
    assertTrue(parameters.containsKey("pep_score"));
    assertEquals(1, parameters.get("pep_score").size());
    assertEquals("1", parameters.get("pep_score").get(0));
    assertTrue(parameters.containsKey("pep_homol"));
    assertEquals(1, parameters.get("pep_homol").size());
    assertEquals("1", parameters.get("pep_homol").get(0));
    assertTrue(parameters.containsKey("pep_ident"));
    assertEquals(1, parameters.get("pep_ident").size());
    assertEquals("1", parameters.get("pep_ident").get(0));
    assertTrue(parameters.containsKey("pep_expect"));
    assertEquals(1, parameters.get("pep_expect").size());
    assertEquals("1", parameters.get("pep_expect").get(0));
    assertTrue(parameters.containsKey("pep_seq"));
    assertEquals(1, parameters.get("pep_seq").size());
    assertEquals("1", parameters.get("pep_seq").get(0));
    assertTrue(parameters.containsKey("pep_frame"));
    assertEquals(1, parameters.get("pep_frame").size());
    assertEquals("1", parameters.get("pep_frame").get(0));
    assertTrue(parameters.containsKey("pep_var_mod"));
    assertEquals(1, parameters.get("pep_var_mod").size());
    assertEquals("1", parameters.get("pep_var_mod").get(0));
    assertTrue(parameters.containsKey("pep_num_match"));
    assertEquals(1, parameters.get("pep_num_match").size());
    assertEquals("1", parameters.get("pep_num_match").get(0));
    assertTrue(parameters.containsKey("pep_scan_title"));
    assertEquals(1, parameters.get("pep_scan_title").size());
    assertEquals("1", parameters.get("pep_scan_title").get(0));
    assertTrue(parameters.containsKey("show_unassigned"));
    assertEquals(1, parameters.get("show_unassigned").size());
    assertEquals("1", parameters.get("show_unassigned").get(0));
    assertTrue(parameters.containsKey("show_pep_dupes"));
    assertEquals(1, parameters.get("show_pep_dupes").size());
    assertEquals("1", parameters.get("show_pep_dupes").get(0));
  }

  @Test
  public void commandLineParameters_AllTrueMasterFalse() {
    peptideParameters.setMaster(false);
    peptideParameters.setExpMr(true);
    peptideParameters.setExpZ(true);
    peptideParameters.setCalcMr(true);
    peptideParameters.setDelta(true);
    peptideParameters.setStart(true);
    peptideParameters.setEnd(true);
    peptideParameters.setMiss(true);
    peptideParameters.setScore(true);
    peptideParameters.setHomol(true);
    peptideParameters.setIdent(true);
    peptideParameters.setExpect(true);
    peptideParameters.setSeq(true);
    peptideParameters.setFrame(true);
    peptideParameters.setVarMod(true);
    peptideParameters.setNumMatch(true);
    peptideParameters.setScanTitle(true);
    peptideParameters.setShowUnassigned(true);
    peptideParameters.setShowPepDupes(true);

    Map<String, List<String>> parameters = peptideParameters.commandLineParameters(true);

    assertEquals(0, parameters.size());
  }

  @Test
  public void commandLineParameters_AllTrueProteinMasterFalse() {
    peptideParameters.setMaster(true);
    peptideParameters.setExpMr(true);
    peptideParameters.setExpZ(true);
    peptideParameters.setCalcMr(true);
    peptideParameters.setDelta(true);
    peptideParameters.setStart(true);
    peptideParameters.setEnd(true);
    peptideParameters.setMiss(true);
    peptideParameters.setScore(true);
    peptideParameters.setHomol(true);
    peptideParameters.setIdent(true);
    peptideParameters.setExpect(true);
    peptideParameters.setSeq(true);
    peptideParameters.setFrame(true);
    peptideParameters.setVarMod(true);
    peptideParameters.setNumMatch(true);
    peptideParameters.setScanTitle(true);
    peptideParameters.setShowUnassigned(true);
    peptideParameters.setShowPepDupes(true);

    Map<String, List<String>> parameters = peptideParameters.commandLineParameters(false);

    assertEquals(0, parameters.size());
  }

  @Test
  public void commandLineParameters_Master() {
    peptideParameters.setMaster(true);

    Map<String, List<String>> parameters = peptideParameters.commandLineParameters(true);

    assertEquals(1, parameters.size());
    assertTrue(parameters.containsKey("peptide_master"));
    assertEquals(1, parameters.get("peptide_master").size());
    assertEquals("1", parameters.get("peptide_master").get(0));
  }

  @Test
  public void commandLineParameters_ExpMr() {
    peptideParameters.setMaster(true);
    peptideParameters.setExpMr(true);

    Map<String, List<String>> parameters = peptideParameters.commandLineParameters(true);

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("peptide_master"));
    assertEquals(1, parameters.get("peptide_master").size());
    assertEquals("1", parameters.get("peptide_master").get(0));
    assertTrue(parameters.containsKey("pep_exp_mr"));
    assertEquals(1, parameters.get("pep_exp_mr").size());
    assertEquals("1", parameters.get("pep_exp_mr").get(0));
  }

  @Test
  public void commandLineParameters_ExpZ() {
    peptideParameters.setMaster(true);
    peptideParameters.setExpZ(true);

    Map<String, List<String>> parameters = peptideParameters.commandLineParameters(true);

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("peptide_master"));
    assertEquals(1, parameters.get("peptide_master").size());
    assertEquals("1", parameters.get("peptide_master").get(0));
    assertTrue(parameters.containsKey("pep_exp_z"));
    assertEquals(1, parameters.get("pep_exp_z").size());
    assertEquals("1", parameters.get("pep_exp_z").get(0));
  }

  @Test
  public void commandLineParameters_CalcMr() {
    peptideParameters.setMaster(true);
    peptideParameters.setCalcMr(true);

    Map<String, List<String>> parameters = peptideParameters.commandLineParameters(true);

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("peptide_master"));
    assertEquals(1, parameters.get("peptide_master").size());
    assertEquals("1", parameters.get("peptide_master").get(0));
    assertTrue(parameters.containsKey("pep_calc_mr"));
    assertEquals(1, parameters.get("pep_calc_mr").size());
    assertEquals("1", parameters.get("pep_calc_mr").get(0));
  }

  @Test
  public void commandLineParameters_Delta() {
    peptideParameters.setMaster(true);
    peptideParameters.setDelta(true);

    Map<String, List<String>> parameters = peptideParameters.commandLineParameters(true);

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("peptide_master"));
    assertEquals(1, parameters.get("peptide_master").size());
    assertEquals("1", parameters.get("peptide_master").get(0));
    assertTrue(parameters.containsKey("pep_delta"));
    assertEquals(1, parameters.get("pep_delta").size());
    assertEquals("1", parameters.get("pep_delta").get(0));
  }

  @Test
  public void commandLineParameters_Start() {
    peptideParameters.setMaster(true);
    peptideParameters.setStart(true);

    Map<String, List<String>> parameters = peptideParameters.commandLineParameters(true);

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("peptide_master"));
    assertEquals(1, parameters.get("peptide_master").size());
    assertEquals("1", parameters.get("peptide_master").get(0));
    assertTrue(parameters.containsKey("pep_start"));
    assertEquals(1, parameters.get("pep_start").size());
    assertEquals("1", parameters.get("pep_start").get(0));
  }

  @Test
  public void commandLineParameters_End() {
    peptideParameters.setMaster(true);
    peptideParameters.setEnd(true);

    Map<String, List<String>> parameters = peptideParameters.commandLineParameters(true);

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("peptide_master"));
    assertEquals(1, parameters.get("peptide_master").size());
    assertEquals("1", parameters.get("peptide_master").get(0));
    assertTrue(parameters.containsKey("pep_end"));
    assertEquals(1, parameters.get("pep_end").size());
    assertEquals("1", parameters.get("pep_end").get(0));
  }

  @Test
  public void commandLineParameters_Miss() {
    peptideParameters.setMaster(true);
    peptideParameters.setMiss(true);

    Map<String, List<String>> parameters = peptideParameters.commandLineParameters(true);

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("peptide_master"));
    assertEquals(1, parameters.get("peptide_master").size());
    assertEquals("1", parameters.get("peptide_master").get(0));
    assertTrue(parameters.containsKey("pep_miss"));
    assertEquals(1, parameters.get("pep_miss").size());
    assertEquals("1", parameters.get("pep_miss").get(0));
  }

  @Test
  public void commandLineParameters_Score() {
    peptideParameters.setMaster(true);
    peptideParameters.setScore(true);

    Map<String, List<String>> parameters = peptideParameters.commandLineParameters(true);

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("peptide_master"));
    assertEquals(1, parameters.get("peptide_master").size());
    assertEquals("1", parameters.get("peptide_master").get(0));
    assertTrue(parameters.containsKey("pep_score"));
    assertEquals(1, parameters.get("pep_score").size());
    assertEquals("1", parameters.get("pep_score").get(0));
  }

  @Test
  public void commandLineParameters_Homol() {
    peptideParameters.setMaster(true);
    peptideParameters.setHomol(true);

    Map<String, List<String>> parameters = peptideParameters.commandLineParameters(true);

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("peptide_master"));
    assertEquals(1, parameters.get("peptide_master").size());
    assertEquals("1", parameters.get("peptide_master").get(0));
    assertTrue(parameters.containsKey("pep_homol"));
    assertEquals(1, parameters.get("pep_homol").size());
    assertEquals("1", parameters.get("pep_homol").get(0));
  }

  @Test
  public void commandLineParameters_Ident() {
    peptideParameters.setMaster(true);
    peptideParameters.setIdent(true);

    Map<String, List<String>> parameters = peptideParameters.commandLineParameters(true);

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("peptide_master"));
    assertEquals(1, parameters.get("peptide_master").size());
    assertEquals("1", parameters.get("peptide_master").get(0));
    assertTrue(parameters.containsKey("pep_ident"));
    assertEquals(1, parameters.get("pep_ident").size());
    assertEquals("1", parameters.get("pep_ident").get(0));
  }

  @Test
  public void commandLineParameters_Expect() {
    peptideParameters.setMaster(true);
    peptideParameters.setExpect(true);

    Map<String, List<String>> parameters = peptideParameters.commandLineParameters(true);

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("peptide_master"));
    assertEquals(1, parameters.get("peptide_master").size());
    assertEquals("1", parameters.get("peptide_master").get(0));
    assertTrue(parameters.containsKey("pep_expect"));
    assertEquals(1, parameters.get("pep_expect").size());
    assertEquals("1", parameters.get("pep_expect").get(0));
  }

  @Test
  public void commandLineParameters_Seq() {
    peptideParameters.setMaster(true);
    peptideParameters.setSeq(true);

    Map<String, List<String>> parameters = peptideParameters.commandLineParameters(true);

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("peptide_master"));
    assertEquals(1, parameters.get("peptide_master").size());
    assertEquals("1", parameters.get("peptide_master").get(0));
    assertTrue(parameters.containsKey("pep_seq"));
    assertEquals(1, parameters.get("pep_seq").size());
    assertEquals("1", parameters.get("pep_seq").get(0));
  }

  @Test
  public void commandLineParameters_Frame() {
    peptideParameters.setMaster(true);
    peptideParameters.setFrame(true);

    Map<String, List<String>> parameters = peptideParameters.commandLineParameters(true);

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("peptide_master"));
    assertEquals(1, parameters.get("peptide_master").size());
    assertEquals("1", parameters.get("peptide_master").get(0));
    assertTrue(parameters.containsKey("pep_frame"));
    assertEquals(1, parameters.get("pep_frame").size());
    assertEquals("1", parameters.get("pep_frame").get(0));
  }

  @Test
  public void commandLineParameters_VarMod() {
    peptideParameters.setMaster(true);
    peptideParameters.setVarMod(true);

    Map<String, List<String>> parameters = peptideParameters.commandLineParameters(true);

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("peptide_master"));
    assertEquals(1, parameters.get("peptide_master").size());
    assertEquals("1", parameters.get("peptide_master").get(0));
    assertTrue(parameters.containsKey("pep_var_mod"));
    assertEquals(1, parameters.get("pep_var_mod").size());
    assertEquals("1", parameters.get("pep_var_mod").get(0));
  }

  @Test
  public void commandLineParameters_NumMatch() {
    peptideParameters.setMaster(true);
    peptideParameters.setNumMatch(true);

    Map<String, List<String>> parameters = peptideParameters.commandLineParameters(true);

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("peptide_master"));
    assertEquals(1, parameters.get("peptide_master").size());
    assertEquals("1", parameters.get("peptide_master").get(0));
    assertTrue(parameters.containsKey("pep_num_match"));
    assertEquals(1, parameters.get("pep_num_match").size());
    assertEquals("1", parameters.get("pep_num_match").get(0));
  }

  @Test
  public void commandLineParameters_ScanTitle() {
    peptideParameters.setMaster(true);
    peptideParameters.setScanTitle(true);

    Map<String, List<String>> parameters = peptideParameters.commandLineParameters(true);

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("peptide_master"));
    assertEquals(1, parameters.get("peptide_master").size());
    assertEquals("1", parameters.get("peptide_master").get(0));
    assertTrue(parameters.containsKey("pep_scan_title"));
    assertEquals(1, parameters.get("pep_scan_title").size());
    assertEquals("1", parameters.get("pep_scan_title").get(0));
  }

  @Test
  public void commandLineParameters_ShowUnassigned() {
    peptideParameters.setMaster(true);
    peptideParameters.setShowUnassigned(true);

    Map<String, List<String>> parameters = peptideParameters.commandLineParameters(true);

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("peptide_master"));
    assertEquals(1, parameters.get("peptide_master").size());
    assertEquals("1", parameters.get("peptide_master").get(0));
    assertTrue(parameters.containsKey("show_unassigned"));
    assertEquals(1, parameters.get("show_unassigned").size());
    assertEquals("1", parameters.get("show_unassigned").get(0));
  }

  @Test
  public void commandLineParameters_ShowPepDupes() {
    peptideParameters.setMaster(true);
    peptideParameters.setShowPepDupes(true);

    Map<String, List<String>> parameters = peptideParameters.commandLineParameters(true);

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("peptide_master"));
    assertEquals(1, parameters.get("peptide_master").size());
    assertEquals("1", parameters.get("peptide_master").get(0));
    assertTrue(parameters.containsKey("show_pep_dupes"));
    assertEquals(1, parameters.get("show_pep_dupes").size());
    assertEquals("1", parameters.get("show_pep_dupes").get(0));
  }
}
