/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.web.JobsView.ADD;
import static ca.qc.ircm.mascotexport.job.web.JobsView.EXPORT;
import static ca.qc.ircm.mascotexport.job.web.JobsView.REFRESH;
import static ca.qc.ircm.mascotexport.job.web.JobsView.REMOVE_DONE;
import static ca.qc.ircm.mascotexport.job.web.JobsView.REMOVE_MANY;

import ca.qc.ircm.mascotexport.test.web.MultiSelectGridElement;
import com.vaadin.flow.component.button.testbench.ButtonElement;
import com.vaadin.flow.component.grid.testbench.GridElement;
import com.vaadin.testbench.TestBenchElement;
import com.vaadin.testbench.elementsbase.Element;

@Element("vaadin-vertical-layout")
public class JobsViewElement extends TestBenchElement {
  private static final int VIEW_COLUMN = 5;
  private static final int REMOVE_COLUMN = 6;

  public GridElement jobs() {
    return $(MultiSelectGridElement.class).waitForFirst();
  }

  public void clickView(int row) {
    jobs().getCell(row, VIEW_COLUMN).$(ButtonElement.class).waitForFirst().click();
  }

  public void clickRemove(int row) {
    jobs().getCell(row, REMOVE_COLUMN).$(ButtonElement.class).waitForFirst().click();
    jobs();
  }

  public void clickExport() {
    $(ButtonElement.class).attributeContains("class", EXPORT).first().click();
  }

  public void clickAdd() {
    $(ButtonElement.class).attributeContains("class", ADD).first().click();
  }

  public void clickRemoveMany() {
    $(ButtonElement.class).attributeContains("class", REMOVE_MANY).first().click();
    jobs();
  }

  public void clickRemoveDone() {
    $(ButtonElement.class).attributeContains("class", REMOVE_DONE).first().click();
    jobs();
  }

  public void clickRefresh() {
    $(ButtonElement.class).attributeContains("class", REFRESH).first().click();
  }
}
