/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.ExportJobProperties.DATE;
import static ca.qc.ircm.mascotexport.job.ExportJobProperties.DONE_DATE;
import static ca.qc.ircm.mascotexport.job.ExportJobProperties.FILE_PATH;
import static ca.qc.ircm.mascotexport.job.ExportJobProperties.INPUT;
import static ca.qc.ircm.mascotexport.job.web.JobParametersDialog.STYLE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.job.ExportJob;
import ca.qc.ircm.mascotexport.job.ExportParameters;
import ca.qc.ircm.mascotexport.job.PeptideParameters;
import ca.qc.ircm.mascotexport.job.ProteinParameters;
import ca.qc.ircm.mascotexport.job.QueryParameters;
import ca.qc.ircm.mascotexport.job.SearchParameters;
import ca.qc.ircm.mascotexport.test.config.AbstractViewTestCase;
import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import ca.qc.ircm.text.MessageResource;
import com.vaadin.flow.i18n.LocaleChangeEvent;
import java.time.LocalDateTime;
import java.util.Locale;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class JobParametersDialogTest extends AbstractViewTestCase {
  private JobParametersDialog view = new JobParametersDialog();
  @Mock
  private ExportParameters exportParameters;
  @Mock
  private PeptideParameters peptideParameters;
  @Mock
  private ProteinParameters proteinParameters;
  @Mock
  private SearchParameters searchParameters;
  @Mock
  private QueryParameters queryParameters;
  private Locale locale = Locale.ENGLISH;
  private MessageResource resources = new MessageResource(ExportJob.class, locale);
  private ExportJob job = new ExportJob();

  /**
   * Before test.
   */
  @Before
  public void beforeTest() {
    when(ui.getLocale()).thenReturn(locale);
    view.exportParametersForm = mock(ExportParametersForm.class);
    view.searchParametersForm = mock(SearchParametersForm.class);
    view.proteinParametersForm = mock(ProteinParametersForm.class);
    view.peptideParametersForm = mock(PeptideParametersForm.class);
    view.queryParametersForm = mock(QueryParametersForm.class);
    job.setDate(LocalDateTime.now());
    job.setExportParameters(exportParameters);
    job.setPeptideParameters(peptideParameters);
    job.setProteinParameters(proteinParameters);
    job.setSearchParameters(searchParameters);
    job.setQueryParameters(queryParameters);
    view.localeChange(mock(LocaleChangeEvent.class));
  }

  @Test
  public void styles() {
    assertTrue(view.layout.hasClassName(STYLE));
    assertTrue(view.file.hasClassName(FILE_PATH));
    assertTrue(view.input.hasClassName(INPUT));
    assertTrue(view.date.hasClassName(DATE));
    assertTrue(view.doneDate.hasClassName(DONE_DATE));
  }

  @Test
  public void labels() {
    assertEquals(resources.message(FILE_PATH), view.fileLabel.getText());
    assertEquals(resources.message(INPUT), view.inputLabel.getText());
    assertEquals(resources.message(DATE), view.dateLabel.getText());
    assertEquals(resources.message(DONE_DATE), view.doneDateLabel.getText());
  }

  @Test
  public void readOnly_Default() {
    assertFalse(view.isReadOnly());
  }

  @Test
  public void isReadOnly_True() {
    view.setReadOnly(true);

    assertTrue(view.isReadOnly());
  }

  @Test
  public void isReadOnly_False() {
    view.setReadOnly(false);

    assertFalse(view.isReadOnly());
  }

  @Test
  public void setReadOnly_True() {
    view.setReadOnly(true);

    verify(view.exportParametersForm).setReadOnly(true);
    verify(view.searchParametersForm).setReadOnly(true);
    verify(view.proteinParametersForm).setReadOnly(true);
    verify(view.peptideParametersForm).setReadOnly(true);
    verify(view.queryParametersForm).setReadOnly(true);
  }

  @Test
  public void setReadOnly_False() {
    view.setReadOnly(false);

    verify(view.exportParametersForm).setReadOnly(false);
    verify(view.searchParametersForm).setReadOnly(false);
    verify(view.proteinParametersForm).setReadOnly(false);
    verify(view.peptideParametersForm).setReadOnly(false);
    verify(view.queryParametersForm).setReadOnly(false);
  }

  @Test
  public void getBean() {
    view.setBean(job);

    assertSame(job, view.getBean());
  }

  @Test
  public void setBean() {
    job.setId("4");
    job.setFilePath("20110330/F017513.dat");
    job.setInput("VL_20110329_COU_01.raw");
    job.setDate(LocalDateTime.of(2012, 5, 6, 14, 42, 37));
    job.setDoneDate(LocalDateTime.of(2012, 5, 8, 6, 30, 45));

    view.setBean(job);

    assertEquals("20110330/F017513.dat", view.file.getText());
    assertEquals("VL_20110329_COU_01.raw", view.input.getText());
    assertEquals("2012-05-06", view.date.getText());
    assertEquals("2012-05-08T06:30:45", view.doneDate.getText());
    verify(view.exportParametersForm).setBean(exportParameters);
    verify(view.searchParametersForm).setBean(searchParameters);
    verify(view.proteinParametersForm).setBean(proteinParameters);
    verify(view.peptideParametersForm).setBean(peptideParameters);
    verify(view.queryParametersForm).setBean(queryParameters);
  }

  @Test
  public void setBean_NoDoneDate() {
    job.setId("4");
    job.setFilePath("20110330/F017513.dat");
    job.setInput("VL_20110329_COU_01.raw");
    job.setDate(LocalDateTime.of(2012, 5, 6, 14, 42, 37));
    job.setDoneDate(null);

    view.setBean(job);

    assertEquals("20110330/F017513.dat", view.file.getText());
    assertEquals("VL_20110329_COU_01.raw", view.input.getText());
    assertEquals("2012-05-06", view.date.getText());
    assertEquals("", view.doneDate.getText());
    verify(view.exportParametersForm).setBean(exportParameters);
    verify(view.searchParametersForm).setBean(searchParameters);
    verify(view.proteinParametersForm).setBean(proteinParameters);
    verify(view.peptideParametersForm).setBean(peptideParameters);
    verify(view.queryParametersForm).setBean(queryParameters);
  }
}
