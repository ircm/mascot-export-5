/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.CALC_MR;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.DELTA;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.END;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.EXPECT;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.EXP_MR;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.EXP_Z;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.FRAME;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.HOMOL;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.IDENT;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.MASTER;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.MISS;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.NUM_MATCH;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.SCAN_TITLE;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.SCORE;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.SEQ;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.SHOW_PEP_DUPES;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.SHOW_UNASSIGNED;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.START;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.VAR_MOD;

import com.vaadin.flow.component.checkbox.testbench.CheckboxElement;
import com.vaadin.testbench.TestBenchElement;
import com.vaadin.testbench.elementsbase.Element;

@Element("vaadin-vertical-layout")
public class PeptideParametersFormElement extends TestBenchElement {
  public CheckboxElement master() {
    return $(CheckboxElement.class).attributeContains("class", MASTER).first();
  }

  public CheckboxElement expMr() {
    return $(CheckboxElement.class).attributeContains("class", EXP_MR).first();
  }

  public CheckboxElement expZ() {
    return $(CheckboxElement.class).attributeContains("class", EXP_Z).first();
  }

  public CheckboxElement calcMr() {
    return $(CheckboxElement.class).attributeContains("class", CALC_MR).first();
  }

  public CheckboxElement delta() {
    return $(CheckboxElement.class).attributeContains("class", DELTA).first();
  }

  public CheckboxElement start() {
    return $(CheckboxElement.class).attributeContains("class", START).first();
  }

  public CheckboxElement end() {
    return $(CheckboxElement.class).attributeContains("class", END).first();
  }

  public CheckboxElement miss() {
    return $(CheckboxElement.class).attributeContains("class", MISS).first();
  }

  public CheckboxElement score() {
    return $(CheckboxElement.class).attributeContains("class", SCORE).first();
  }

  public CheckboxElement homol() {
    return $(CheckboxElement.class).attributeContains("class", HOMOL).first();
  }

  public CheckboxElement ident() {
    return $(CheckboxElement.class).attributeContains("class", IDENT).first();
  }

  public CheckboxElement expect() {
    return $(CheckboxElement.class).attributeContains("class", EXPECT).first();
  }

  public CheckboxElement seq() {
    return $(CheckboxElement.class).attributeContains("class", SEQ).first();
  }

  public CheckboxElement frame() {
    return $(CheckboxElement.class).attributeContains("class", FRAME).first();
  }

  public CheckboxElement varMod() {
    return $(CheckboxElement.class).attributeContains("class", VAR_MOD).first();
  }

  public CheckboxElement numMatch() {
    return $(CheckboxElement.class).attributeContains("class", NUM_MATCH).first();
  }

  public CheckboxElement scanTitle() {
    return $(CheckboxElement.class).attributeContains("class", SCAN_TITLE).first();
  }

  public CheckboxElement showUnassigned() {
    return $(CheckboxElement.class).attributeContains("class", SHOW_UNASSIGNED).first();
  }

  public CheckboxElement showPepDupes() {
    return $(CheckboxElement.class).attributeContains("class", SHOW_PEP_DUPES).first();
  }
}
