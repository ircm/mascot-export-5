/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.QueryParametersProperties.MASTER;
import static ca.qc.ircm.mascotexport.job.QueryParametersProperties.PARAMS;
import static ca.qc.ircm.mascotexport.job.QueryParametersProperties.PEAKS;
import static ca.qc.ircm.mascotexport.job.QueryParametersProperties.QUALIFIERS;
import static ca.qc.ircm.mascotexport.job.QueryParametersProperties.RAW;
import static ca.qc.ircm.mascotexport.job.QueryParametersProperties.TITLE;

import com.vaadin.flow.component.checkbox.testbench.CheckboxElement;
import com.vaadin.testbench.TestBenchElement;
import com.vaadin.testbench.elementsbase.Element;

@Element("vaadin-vertical-layout")
public class QueryParametersFormElement extends TestBenchElement {
  public CheckboxElement master() {
    return $(CheckboxElement.class).attributeContains("class", MASTER).first();
  }

  public CheckboxElement title() {
    return $(CheckboxElement.class).attributeContains("class", TITLE).first();
  }

  public CheckboxElement qualifiers() {
    return $(CheckboxElement.class).attributeContains("class", QUALIFIERS).first();
  }

  public CheckboxElement params() {
    return $(CheckboxElement.class).attributeContains("class", PARAMS).first();
  }

  public CheckboxElement peaks() {
    return $(CheckboxElement.class).attributeContains("class", PEAKS).first();
  }

  public CheckboxElement raw() {
    return $(CheckboxElement.class).attributeContains("class", RAW).first();
  }
}
