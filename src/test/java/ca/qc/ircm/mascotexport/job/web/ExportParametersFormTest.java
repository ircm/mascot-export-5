/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.ExportFormat.CSV;
import static ca.qc.ircm.mascotexport.job.ExportFormat.XML;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.EXPORT_FORMAT;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.GROUP_FAMILY;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.IGNORE_IONS_SCORE_BELOW;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.PREFER_TAXONOMY;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.REPORT;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.REQUIRE_BOLD_RED;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.SERVER_MUDPIT_SWITCH;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.SHOW_SAME_SETS;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.SHOW_SUBSETS;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.SIG_THRESHOLD;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.USE_HOMOLOGY;
import static ca.qc.ircm.mascotexport.job.web.ExportParametersForm.SERVER_MUDPIT_SWITCH_LABEL;
import static ca.qc.ircm.mascotexport.job.web.ExportParametersForm.STYLE;
import static ca.qc.ircm.mascotexport.job.web.ExportParametersForm.USE_HOMOLOGY_LABEL;
import static ca.qc.ircm.mascotexport.test.utils.SearchUtils.findValidationStatusByField;
import static ca.qc.ircm.mascotexport.test.utils.VaadinTestUtils.items;
import static ca.qc.ircm.mascotexport.text.Strings.property;
import static ca.qc.ircm.mascotexport.web.WebConstants.INVALID_INTEGER;
import static ca.qc.ircm.mascotexport.web.WebConstants.INVALID_NUMBER;
import static ca.qc.ircm.mascotexport.web.WebConstants.REQUIRED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.ProteinScoring;
import ca.qc.ircm.mascotexport.job.ExportFormat;
import ca.qc.ircm.mascotexport.job.ExportParameters;
import ca.qc.ircm.mascotexport.test.config.AbstractViewTestCase;
import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import ca.qc.ircm.mascotexport.web.WebConstants;
import ca.qc.ircm.text.MessageResource;
import com.vaadin.flow.data.binder.BinderValidationStatus;
import com.vaadin.flow.data.binder.BindingValidationStatus;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.i18n.LocaleChangeEvent;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class ExportParametersFormTest extends AbstractViewTestCase {
  private static final Double DELTA = 0.000000001;
  private ExportParametersForm view = new ExportParametersForm();
  private Locale locale = Locale.ENGLISH;
  private MessageResource resources = new MessageResource(ExportParameters.class, locale);
  private MessageResource generalResources = new MessageResource(WebConstants.class, locale);
  private ExportParameters parameters = new ExportParameters();

  @Before
  public void beforeTest() {
    when(ui.getLocale()).thenReturn(locale);
    view.localeChange(mock(LocaleChangeEvent.class));
  }

  @Test
  public void styles() {
    assertTrue(view.getContent().hasClassName(STYLE));
    assertTrue(view.exportFormat.hasClassName(EXPORT_FORMAT));
    assertTrue(view.sigThreshold.hasClassName(SIG_THRESHOLD));
    assertTrue(view.ignoreIonsScoreBelow.hasClassName(IGNORE_IONS_SCORE_BELOW));
    assertTrue(view.useHomology.hasClassName(USE_HOMOLOGY));
    assertTrue(view.useHomologyLabel.hasClassName(USE_HOMOLOGY_LABEL));
    assertTrue(view.report.hasClassName(REPORT));
    assertTrue(view.serverMudpitSwitch.hasClassName(SERVER_MUDPIT_SWITCH));
    assertTrue(view.serverMudpitSwitchLabel.hasClassName(SERVER_MUDPIT_SWITCH_LABEL));
    assertTrue(view.showSameSets.hasClassName(SHOW_SAME_SETS));
    assertTrue(view.showSubsets.hasClassName(SHOW_SUBSETS));
    assertTrue(view.groupFamily.hasClassName(GROUP_FAMILY));
    assertTrue(view.requireBoldRed.hasClassName(REQUIRE_BOLD_RED));
    assertTrue(view.preferTaxonomy.hasClassName(PREFER_TAXONOMY));
  }

  @Test
  public void labels() {
    assertEquals(resources.message(EXPORT_FORMAT), view.exportFormat.getLabel());
    assertEquals(resources.message(SIG_THRESHOLD), view.sigThreshold.getLabel());
    assertEquals(resources.message(IGNORE_IONS_SCORE_BELOW), view.ignoreIonsScoreBelow.getLabel());
    assertEquals(resources.message(USE_HOMOLOGY), view.useHomologyLabel.getText());
    assertEquals(resources.message(REPORT), view.report.getLabel());
    assertEquals(resources.message(SERVER_MUDPIT_SWITCH), view.serverMudpitSwitchLabel.getText());
    assertEquals(resources.message(SHOW_SAME_SETS), view.showSameSets.getLabel());
    assertEquals(resources.message(SHOW_SUBSETS), view.showSubsets.getLabel());
    assertEquals(resources.message(GROUP_FAMILY), view.groupFamily.getLabel());
    assertEquals(resources.message(REQUIRE_BOLD_RED), view.requireBoldRed.getLabel());
    assertEquals(resources.message(PREFER_TAXONOMY), view.preferTaxonomy.getText());
  }

  @Test
  public void exportFormat() {
    List<ExportFormat> values = items(view.exportFormat);
    assertEquals(ExportFormat.values().length, values.size());
    MessageResource formatResources = new MessageResource(ExportFormat.class, locale);
    for (ExportFormat format : ExportFormat.values()) {
      assertTrue(format.name(), values.contains(format));
      assertEquals(formatResources.message(format.name()),
          view.exportFormat.getItemLabelGenerator().apply(format));
    }
  }

  @Test
  public void useHomology() {
    List<Boolean> values = items(view.useHomology);
    boolean[] expectedValues = new boolean[] { true, false };
    assertEquals(expectedValues.length, values.size());
    for (boolean value : expectedValues) {
      assertTrue(String.valueOf(value), values.contains(value));
      assertEquals(resources.message(property(USE_HOMOLOGY, String.valueOf(value))),
          view.useHomology.getItemRenderer().createComponent(value).getElement().getText());
    }
  }

  @Test
  public void serverMudpitSwitch() {
    List<ProteinScoring> values = items(view.serverMudpitSwitch);
    assertEquals(ProteinScoring.values().length, values.size());
    MessageResource scoringResources = new MessageResource(ProteinScoring.class, locale);
    for (ProteinScoring scoring : ProteinScoring.values()) {
      assertTrue(scoring.name(), values.contains(scoring));
      assertEquals(scoringResources.message(scoring.name()), view.serverMudpitSwitch
          .getItemRenderer().createComponent(scoring).getElement().getText());
    }
  }

  @Test
  public void values() {
    assertEquals(CSV, view.exportFormat.getValue());
    assertEquals("", view.sigThreshold.getValue());
    assertEquals("", view.ignoreIonsScoreBelow.getValue());
    assertEquals(false, view.useHomology.getValue());
    assertEquals("", view.report.getValue());
    assertEquals(ProteinScoring.STANDARD, view.serverMudpitSwitch.getValue());
    assertEquals(false, view.showSameSets.getValue());
    assertEquals("", view.showSubsets.getValue());
    assertEquals(false, view.groupFamily.getValue());
    assertEquals(false, view.requireBoldRed.getValue());
  }

  @Test
  public void readOnly_Default() {
    assertFalse(view.isReadOnly());

    assertFalse(view.exportFormat.isReadOnly());
    assertFalse(view.sigThreshold.isReadOnly());
    assertFalse(view.ignoreIonsScoreBelow.isReadOnly());
    assertFalse(view.useHomology.isReadOnly());
    assertFalse(view.report.isReadOnly());
    assertFalse(view.serverMudpitSwitch.isReadOnly());
    assertFalse(view.showSameSets.isReadOnly());
    assertFalse(view.showSubsets.isReadOnly());
    assertFalse(view.groupFamily.isReadOnly());
    assertFalse(view.requireBoldRed.isReadOnly());
  }

  @Test
  public void isReadOnly_True() {
    view.setReadOnly(true);

    assertTrue(view.isReadOnly());
  }

  @Test
  public void isReadOnly_False() {
    view.setReadOnly(false);

    assertFalse(view.isReadOnly());
  }

  @Test
  public void setReadOnly_True() {
    view.setReadOnly(true);

    assertTrue(view.exportFormat.isReadOnly());
    assertTrue(view.sigThreshold.isReadOnly());
    assertTrue(view.ignoreIonsScoreBelow.isReadOnly());
    assertTrue(view.useHomology.isReadOnly());
    assertTrue(view.report.isReadOnly());
    assertTrue(view.serverMudpitSwitch.isReadOnly());
    assertTrue(view.showSameSets.isReadOnly());
    assertTrue(view.showSubsets.isReadOnly());
    assertTrue(view.groupFamily.isReadOnly());
    assertTrue(view.requireBoldRed.isReadOnly());
  }

  @Test
  public void setReadOnly_False() {
    view.setReadOnly(false);

    assertFalse(view.exportFormat.isReadOnly());
    assertFalse(view.sigThreshold.isReadOnly());
    assertFalse(view.ignoreIonsScoreBelow.isReadOnly());
    assertFalse(view.useHomology.isReadOnly());
    assertFalse(view.report.isReadOnly());
    assertFalse(view.serverMudpitSwitch.isReadOnly());
    assertFalse(view.showSameSets.isReadOnly());
    assertFalse(view.showSubsets.isReadOnly());
    assertFalse(view.groupFamily.isReadOnly());
    assertFalse(view.requireBoldRed.isReadOnly());
  }

  @Test
  public void getBean() {
    view.setBean(parameters);

    assertSame(parameters, view.getBean());
  }

  @Test
  public void setBean() {
    parameters.setExportFormat(XML);
    parameters.setSigThreshold(10.0);
    parameters.setIgnoreIonsScoreBelow(15);
    parameters.setUseHomology(false);
    parameters.setReport(20);
    parameters.setServerMudpitSwitch(ProteinScoring.MUDPIT);
    parameters.setShowSameSets(true);
    parameters.setShowSubsets(2);
    parameters.setGroupFamily(true);
    parameters.setRequireBoldRed(true);

    view.setBean(parameters);

    assertEquals(XML, view.exportFormat.getValue());
    assertEquals("10", view.sigThreshold.getValue());
    assertEquals("15", view.ignoreIonsScoreBelow.getValue());
    assertEquals(false, view.useHomology.getValue());
    assertEquals("20", view.report.getValue());
    assertEquals(ProteinScoring.MUDPIT, view.serverMudpitSwitch.getValue());
    assertEquals(true, view.showSameSets.getValue());
    assertEquals("2", view.showSubsets.getValue());
    assertEquals(true, view.groupFamily.getValue());
    assertEquals(true, view.requireBoldRed.getValue());
  }

  @Test
  public void setBean_BeforeChangeLocale() {
    parameters.setExportFormat(XML);
    parameters.setSigThreshold(10.0);
    parameters.setIgnoreIonsScoreBelow(15);
    parameters.setUseHomology(false);
    parameters.setReport(20);
    parameters.setServerMudpitSwitch(ProteinScoring.MUDPIT);
    parameters.setShowSameSets(true);
    parameters.setShowSubsets(2);
    parameters.setGroupFamily(true);
    parameters.setRequireBoldRed(true);

    view.setBean(parameters);
    view.localeChange(mock(LocaleChangeEvent.class));

    assertEquals(XML, view.exportFormat.getValue());
    assertEquals("10", view.sigThreshold.getValue());
    assertEquals("15", view.ignoreIonsScoreBelow.getValue());
    assertEquals(false, view.useHomology.getValue());
    assertEquals("20", view.report.getValue());
    assertEquals(ProteinScoring.MUDPIT, view.serverMudpitSwitch.getValue());
    assertEquals(true, view.showSameSets.getValue());
    assertEquals("2", view.showSubsets.getValue());
    assertEquals(true, view.groupFamily.getValue());
    assertEquals(true, view.requireBoldRed.getValue());
  }

  @Test
  public void validate() {
    BinderValidationStatus<ExportParameters> statuses = view.validate();

    assertTrue(statuses.isOk());
  }

  @Test
  public void validate_ExportFormatEmpty() throws Throwable {
    view.exportFormat.setValue(null);

    BinderValidationStatus<ExportParameters> statuses = view.validate();

    assertFalse(statuses.isOk());
    Optional<BindingValidationStatus<?>> optionalError =
        findValidationStatusByField(statuses, view.exportFormat);
    assertTrue(optionalError.isPresent());
    BindingValidationStatus<?> error = optionalError.get();
    assertEquals(Optional.of(generalResources.message(REQUIRED)), error.getMessage());
  }

  @Test
  public void validate_SigThresholdInvalid() throws Throwable {
    view.sigThreshold.setValue("a");

    BinderValidationStatus<ExportParameters> statuses = view.validate();

    assertFalse(statuses.isOk());
    Optional<BindingValidationStatus<?>> optionalError =
        findValidationStatusByField(statuses, view.sigThreshold);
    assertTrue(optionalError.isPresent());
    BindingValidationStatus<?> error = optionalError.get();
    assertEquals(Optional.of(generalResources.message(INVALID_NUMBER)), error.getMessage());
  }

  @Test
  public void validate_IgnoreIonsScoreBelowInvalid() throws Throwable {
    view.ignoreIonsScoreBelow.setValue("a");

    BinderValidationStatus<ExportParameters> statuses = view.validate();

    assertFalse(statuses.isOk());
    Optional<BindingValidationStatus<?>> optionalError =
        findValidationStatusByField(statuses, view.ignoreIonsScoreBelow);
    assertTrue(optionalError.isPresent());
    BindingValidationStatus<?> error = optionalError.get();
    assertEquals(Optional.of(generalResources.message(INVALID_INTEGER)), error.getMessage());
  }

  @Test
  public void validate_IgnoreIonsScoreBelowDouble() throws Throwable {
    view.ignoreIonsScoreBelow.setValue("2.3");

    BinderValidationStatus<ExportParameters> statuses = view.validate();

    assertFalse(statuses.isOk());
    Optional<BindingValidationStatus<?>> optionalError =
        findValidationStatusByField(statuses, view.ignoreIonsScoreBelow);
    assertTrue(optionalError.isPresent());
    BindingValidationStatus<?> error = optionalError.get();
    assertEquals(Optional.of(generalResources.message(INVALID_INTEGER)), error.getMessage());
  }

  @Test
  public void validate_ReportInvalid() throws Throwable {
    view.report.setValue("a");

    BinderValidationStatus<ExportParameters> statuses = view.validate();

    assertFalse(statuses.isOk());
    Optional<BindingValidationStatus<?>> optionalError =
        findValidationStatusByField(statuses, view.report);
    assertTrue(optionalError.isPresent());
    BindingValidationStatus<?> error = optionalError.get();
    assertEquals(Optional.of(generalResources.message(INVALID_INTEGER)), error.getMessage());
  }

  @Test
  public void validate_ReportDouble() throws Throwable {
    view.report.setValue("2.3");

    BinderValidationStatus<ExportParameters> statuses = view.validate();

    assertFalse(statuses.isOk());
    Optional<BindingValidationStatus<?>> optionalError =
        findValidationStatusByField(statuses, view.report);
    assertTrue(optionalError.isPresent());
    BindingValidationStatus<?> error = optionalError.get();
    assertEquals(Optional.of(generalResources.message(INVALID_INTEGER)), error.getMessage());
  }

  @Test
  public void validate_ShowSubsetsInvalid() throws Throwable {
    view.showSubsets.setValue("a");

    BinderValidationStatus<ExportParameters> statuses = view.validate();

    assertFalse(statuses.isOk());
    Optional<BindingValidationStatus<?>> optionalError =
        findValidationStatusByField(statuses, view.showSubsets);
    assertTrue(optionalError.isPresent());
    BindingValidationStatus<?> error = optionalError.get();
    assertEquals(Optional.of(generalResources.message(INVALID_INTEGER)), error.getMessage());
  }

  @Test
  public void validate_ShowSubsetsDouble() throws Throwable {
    view.showSubsets.setValue("2.3");

    BinderValidationStatus<ExportParameters> statuses = view.validate();

    assertFalse(statuses.isOk());
    Optional<BindingValidationStatus<?>> optionalError =
        findValidationStatusByField(statuses, view.showSubsets);
    assertTrue(optionalError.isPresent());
    BindingValidationStatus<?> error = optionalError.get();
    assertEquals(Optional.of(generalResources.message(INVALID_INTEGER)), error.getMessage());
  }

  @Test
  public void writeBean() throws Throwable {
    view.exportFormat.setValue(XML);
    view.sigThreshold.setValue("10");
    view.ignoreIonsScoreBelow.setValue("15");
    view.useHomology.setValue(false);
    view.report.setValue("20");
    view.serverMudpitSwitch.setValue(ProteinScoring.MUDPIT);
    view.showSameSets.setValue(true);
    view.showSubsets.setValue("2");
    view.groupFamily.setValue(true);
    view.requireBoldRed.setValue(true);

    view.writeBean(parameters);

    assertEquals(XML, parameters.getExportFormat());
    assertEquals(10.0, parameters.getSigThreshold(), DELTA);
    assertEquals((Integer) 15, parameters.getIgnoreIonsScoreBelow());
    assertEquals(false, parameters.isUseHomology());
    assertEquals((Integer) 20, parameters.getReport());
    assertEquals(ProteinScoring.MUDPIT, parameters.getServerMudpitSwitch());
    assertEquals(true, parameters.isShowSameSets());
    assertEquals((Integer) 2, parameters.getShowSubsets());
    assertEquals(true, parameters.isGroupFamily());
    assertEquals(true, parameters.isRequireBoldRed());
  }

  @Test
  public void writeBean_ExportFormatEmpty() throws Throwable {
    view.exportFormat.setValue(null);

    try {
      view.writeBean(parameters);
      fail("Expected ValidationException");
    } catch (ValidationException e) {
      Optional<BindingValidationStatus<?>> optionalError =
          findValidationStatusByField(e, view.exportFormat);
      assertTrue(optionalError.isPresent());
      BindingValidationStatus<?> error = optionalError.get();
      assertEquals(Optional.of(generalResources.message(REQUIRED)), error.getMessage());
    }
  }

  @Test
  public void writeBean_SigThresholdInvalid() throws Throwable {
    view.sigThreshold.setValue("a");

    try {
      view.writeBean(parameters);
      fail("Expected ValidationException");
    } catch (ValidationException e) {
      Optional<BindingValidationStatus<?>> optionalError =
          findValidationStatusByField(e, view.sigThreshold);
      assertTrue(optionalError.isPresent());
      BindingValidationStatus<?> error = optionalError.get();
      assertEquals(Optional.of(generalResources.message(INVALID_NUMBER)), error.getMessage());
    }
  }

  @Test
  public void writeBean_IgnoreIonsScoreBelowInvalid() throws Throwable {
    view.ignoreIonsScoreBelow.setValue("a");

    try {
      view.writeBean(parameters);
      fail("Expected ValidationException");
    } catch (ValidationException e) {
      Optional<BindingValidationStatus<?>> optionalError =
          findValidationStatusByField(e, view.ignoreIonsScoreBelow);
      assertTrue(optionalError.isPresent());
      BindingValidationStatus<?> error = optionalError.get();
      assertEquals(Optional.of(generalResources.message(INVALID_INTEGER)), error.getMessage());
    }
  }

  @Test
  public void writeBean_IgnoreIonsScoreBelowDouble() throws Throwable {
    view.ignoreIonsScoreBelow.setValue("2.3");

    try {
      view.writeBean(parameters);
      fail("Expected ValidationException");
    } catch (ValidationException e) {
      Optional<BindingValidationStatus<?>> optionalError =
          findValidationStatusByField(e, view.ignoreIonsScoreBelow);
      assertTrue(optionalError.isPresent());
      BindingValidationStatus<?> error = optionalError.get();
      assertEquals(Optional.of(generalResources.message(INVALID_INTEGER)), error.getMessage());
    }
  }

  @Test
  public void writeBean_ReportInvalid() throws Throwable {
    view.report.setValue("a");

    try {
      view.writeBean(parameters);
      fail("Expected ValidationException");
    } catch (ValidationException e) {
      Optional<BindingValidationStatus<?>> optionalError =
          findValidationStatusByField(e, view.report);
      assertTrue(optionalError.isPresent());
      BindingValidationStatus<?> error = optionalError.get();
      assertEquals(Optional.of(generalResources.message(INVALID_INTEGER)), error.getMessage());
    }
  }

  @Test
  public void writeBean_ReportDouble() throws Throwable {
    view.report.setValue("2.3");

    try {
      view.writeBean(parameters);
      fail("Expected ValidationException");
    } catch (ValidationException e) {
      Optional<BindingValidationStatus<?>> optionalError =
          findValidationStatusByField(e, view.report);
      assertTrue(optionalError.isPresent());
      BindingValidationStatus<?> error = optionalError.get();
      assertEquals(Optional.of(generalResources.message(INVALID_INTEGER)), error.getMessage());
    }
  }

  @Test
  public void writeBean_ShowSubsetsInvalid() throws Throwable {
    view.showSubsets.setValue("a");

    try {
      view.writeBean(parameters);
      fail("Expected ValidationException");
    } catch (ValidationException e) {
      Optional<BindingValidationStatus<?>> optionalError =
          findValidationStatusByField(e, view.showSubsets);
      assertTrue(optionalError.isPresent());
      BindingValidationStatus<?> error = optionalError.get();
      assertEquals(Optional.of(generalResources.message(INVALID_INTEGER)), error.getMessage());
    }
  }

  @Test
  public void writeBean_ShowSubsetsDouble() throws Throwable {
    view.showSubsets.setValue("2.3");

    try {
      view.writeBean(parameters);
      fail("Expected ValidationException");
    } catch (ValidationException e) {
      Optional<BindingValidationStatus<?>> optionalError =
          findValidationStatusByField(e, view.showSubsets);
      assertTrue(optionalError.isPresent());
      BindingValidationStatus<?> error = optionalError.get();
      assertEquals(Optional.of(generalResources.message(INVALID_INTEGER)), error.getMessage());
    }
  }
}
