/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.web.WebConstants.APPLICATION_NAME;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import ca.qc.ircm.mascotexport.job.ExportJob;
import ca.qc.ircm.mascotexport.job.ExportJobService;
import ca.qc.ircm.mascotexport.job.RunningExportJobs;
import ca.qc.ircm.mascotexport.test.config.AbstractTestBenchTestCase;
import ca.qc.ircm.mascotexport.test.config.TestBenchTestAnnotations;
import ca.qc.ircm.mascotexport.test.utils.SearchUtils;
import ca.qc.ircm.mascotexport.web.WebConstants;
import ca.qc.ircm.text.MessageResource;
import com.vaadin.flow.component.dialog.testbench.DialogElement;
import com.vaadin.flow.component.orderedlayout.testbench.VerticalLayoutElement;
import java.util.List;
import java.util.Locale;
import javax.inject.Inject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@TestBenchTestAnnotations
public class JobsViewIntegrationTest extends AbstractTestBenchTestCase {
  @Inject
  private ExportJobService exportJobService;
  @Inject
  private RunningExportJobs runningJobs;

  @Test
  public void title() throws Throwable {
    openJobsView();
    MessageResource resources = new MessageResource(JobsView.class, Locale.getDefault());
    MessageResource generalResources = new MessageResource(WebConstants.class, Locale.getDefault());
    assertEquals(resources.message(WebConstants.TITLE, generalResources.message(APPLICATION_NAME)),
        getDriver().getTitle());
  }

  @Test
  public void view() throws Throwable {
    JobsViewElement jobsView = openJobsView();
    jobsView.clickView(0);

    DialogElement dialog = $(DialogElement.class).waitForFirst();
    VerticalLayoutElement layout = dialog.$(VerticalLayoutElement.class).first();
    assertTrue(layout.hasClassName(JobParametersDialog.STYLE));
  }

  @Test
  public void remove() {
    JobsViewElement jobsView = openJobsView();
    jobsView.clickRemove(0);

    List<ExportJob> jobs = exportJobService.all();
    assertEquals(3, jobs.size());
    assertFalse(SearchUtils.find(jobs, "1").isPresent());
    assertTrue(SearchUtils.find(jobs, "2").isPresent());
    assertTrue(SearchUtils.find(jobs, "3").isPresent());
    assertTrue(SearchUtils.find(jobs, "4").isPresent());
  }

  @Test
  public void export() throws Throwable {
    JobsViewElement jobsView = openJobsView();
    jobsView.jobs().select(0);
    jobsView.jobs().select(2);

    jobsView.clickExport();
    Thread.sleep(1000); // TODO Replace by waitForFirst in Vaadin 14.1.

    assertNotNull(runningJobs.get(exportJobService.get("1")));
    assertNotNull(runningJobs.get(exportJobService.get("3")));
  }

  @Test
  public void add() {
    JobsViewElement jobsView = openJobsView();
    jobsView.clickAdd();

    assertEquals(viewUrl(AddJobsView.VIEW_NAME), getDriver().getCurrentUrl());
  }

  @Test
  public void removeMany() throws Throwable {
    JobsViewElement jobsView = openJobsView();
    jobsView.jobs().select(0);
    jobsView.jobs().select(2);
    jobsView.clickRemoveMany();

    List<ExportJob> jobs = exportJobService.all();
    assertEquals(2, jobs.size());
    assertFalse(SearchUtils.find(jobs, "1").isPresent());
    assertTrue(SearchUtils.find(jobs, "2").isPresent());
    assertFalse(SearchUtils.find(jobs, "3").isPresent());
    assertTrue(SearchUtils.find(jobs, "4").isPresent());
  }

  @Test
  public void removeDone() throws Throwable {
    JobsViewElement jobsView = openJobsView();
    jobsView.clickRemoveDone();

    List<ExportJob> jobs = exportJobService.all();
    assertEquals(3, jobs.size());
    assertTrue(SearchUtils.find(jobs, "1").isPresent());
    assertTrue(SearchUtils.find(jobs, "2").isPresent());
    assertTrue(SearchUtils.find(jobs, "3").isPresent());
    assertFalse(SearchUtils.find(jobs, "4").isPresent());
  }

  @Test
  public void refresh() throws Throwable {
    JobsViewElement jobsView = openJobsView();

    exportJobService.delete(exportJobService.all().get(0));

    jobsView.clickRefresh();

    assertEquals(3, jobsView.jobs().getRowCount());
  }
}
