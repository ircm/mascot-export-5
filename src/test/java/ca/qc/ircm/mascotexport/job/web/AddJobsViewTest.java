/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.ExportJobProperties.INPUT;
import static ca.qc.ircm.mascotexport.job.web.AddJobsView.FILES;
import static ca.qc.ircm.mascotexport.job.web.AddJobsView.HEADER;
import static ca.qc.ircm.mascotexport.job.web.AddJobsView.NAME;
import static ca.qc.ircm.mascotexport.job.web.AddJobsView.PARENT;
import static ca.qc.ircm.mascotexport.job.web.AddJobsView.VIEW_NAME;
import static ca.qc.ircm.mascotexport.web.WebConstants.ALL;
import static ca.qc.ircm.mascotexport.web.WebConstants.APPLICATION_NAME;
import static ca.qc.ircm.mascotexport.web.WebConstants.CANCEL;
import static ca.qc.ircm.mascotexport.web.WebConstants.SAVE;
import static ca.qc.ircm.mascotexport.web.WebConstants.TITLE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.job.ExportJob;
import ca.qc.ircm.mascotexport.mascot.MascotFile;
import ca.qc.ircm.mascotexport.test.config.AbstractViewTestCase;
import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import ca.qc.ircm.mascotexport.web.WebConstants;
import ca.qc.ircm.text.MessageResource;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.Column;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.grid.HeaderRow.HeaderCell;
import com.vaadin.flow.function.ValueProvider;
import com.vaadin.flow.i18n.LocaleChangeEvent;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class AddJobsViewTest extends AbstractViewTestCase {
  private AddJobsView view;
  @Mock
  private AddJobsViewPresenter presenter;
  @Captor
  private ArgumentCaptor<ValueProvider<MascotFile, String>> valueProviderCaptor;
  private Locale locale = Locale.ENGLISH;
  private MessageResource resources = new MessageResource(AddJobsView.class, locale);
  private MessageResource jobResources = new MessageResource(ExportJob.class, locale);
  private MessageResource generalResources = new MessageResource(WebConstants.class, locale);
  private List<MascotFile> mascotFiles = new ArrayList<>();

  /**
   * Before test.
   */
  @Before
  public void beforeTest() throws Throwable {
    when(ui.getLocale()).thenReturn(locale);
    view = new AddJobsView(presenter);
    mascotFiles.add(new MascotFile(Paths.get("20181011", "F20013.dat"), "VL_20181011_COU_01.RAW"));
    mascotFiles.add(new MascotFile(Paths.get("20181011", "F20014.dat"), "VL_20181011_COU_02.RAW"));
    mascotFiles.add(new MascotFile(Paths.get("20181012", "F20016.dat"), "VL_20181012_COU_01.RAW"));
    mascotFiles.add(new MascotFile(Paths.get("F00116.dat"), "VL_20181012_COU_01.RAW"));
  }

  @SuppressWarnings("unchecked")
  private void mockColumns() {
    view.files = mock(Grid.class);
    HeaderRow headerRow = mock(HeaderRow.class);
    when(view.files.appendHeaderRow()).thenReturn(headerRow);
    when(headerRow.getCell(any())).thenReturn(mock(HeaderCell.class));
    view.nameColumn = mock(Column.class);
    when(view.files.addColumn(any(ValueProvider.class), eq(NAME))).thenReturn(view.nameColumn);
    when(view.nameColumn.setKey(any())).thenReturn(view.nameColumn);
    view.parentColumn = mock(Column.class);
    when(view.files.addColumn(any(ValueProvider.class), eq(PARENT))).thenReturn(view.parentColumn);
    when(view.parentColumn.setKey(any())).thenReturn(view.parentColumn);
    view.inputColumn = mock(Column.class);
    when(view.files.addColumn(any(ValueProvider.class), eq(INPUT))).thenReturn(view.inputColumn);
    when(view.inputColumn.setKey(any())).thenReturn(view.inputColumn);
  }

  @Test
  public void presenter_Init() {
    view.onAttach(mock(AttachEvent.class));
    verify(presenter).init(view);
  }

  @Test
  public void styles() {
    assertTrue(view.getContent().getId().orElse("").equals(VIEW_NAME));
    assertTrue(view.header.hasClassName(HEADER));
    assertTrue(view.files.hasClassName(FILES));
    assertTrue(view.filesError.hasClassName("error-text"));
    assertTrue(view.filesError.hasClassName("font-size-xs"));
    assertTrue(view.save.hasClassName(SAVE));
    assertTrue(view.cancel.hasClassName(CANCEL));
  }

  @Test
  public void labels() {
    mockColumns();
    view.initFiles();
    view.localeChange(mock(LocaleChangeEvent.class));
    assertEquals(resources.message(HEADER), view.header.getText());
    verify(view.nameColumn).setHeader(resources.message(NAME));
    verify(view.parentColumn).setHeader(resources.message(PARENT));
    verify(view.inputColumn).setHeader(jobResources.message(INPUT));
    assertEquals(generalResources.message(ALL), view.nameFilter.getPlaceholder());
    assertEquals(generalResources.message(ALL), view.parentFilter.getPlaceholder());
    assertEquals(generalResources.message(ALL), view.inputFilter.getPlaceholder());
    assertEquals(generalResources.message(SAVE), view.save.getText());
    assertEquals(generalResources.message(CANCEL), view.cancel.getText());
  }

  @Test
  public void localeChange() {
    mockColumns();
    view.initFiles();
    view.localeChange(mock(LocaleChangeEvent.class));
    Locale locale = Locale.FRENCH;
    final MessageResource resources = new MessageResource(AddJobsView.class, locale);
    final MessageResource jobResources = new MessageResource(ExportJob.class, locale);
    final MessageResource generalResources = new MessageResource(WebConstants.class, locale);
    when(ui.getLocale()).thenReturn(locale);
    view.localeChange(mock(LocaleChangeEvent.class));
    assertEquals(resources.message(HEADER), view.header.getText());
    verify(view.nameColumn, atLeastOnce()).setHeader(resources.message(NAME));
    verify(view.parentColumn, atLeastOnce()).setHeader(resources.message(PARENT));
    verify(view.inputColumn, atLeastOnce()).setHeader(jobResources.message(INPUT));
    assertEquals(generalResources.message(ALL), view.nameFilter.getPlaceholder());
    assertEquals(generalResources.message(ALL), view.parentFilter.getPlaceholder());
    assertEquals(generalResources.message(ALL), view.inputFilter.getPlaceholder());
    assertEquals(generalResources.message(SAVE), view.save.getText());
    assertEquals(generalResources.message(CANCEL), view.cancel.getText());
  }

  @Test
  public void getPageTitle() {
    assertEquals(resources.message(TITLE, generalResources.message(APPLICATION_NAME)),
        view.getPageTitle());
  }

  @Test
  public void files_SelectionMode() {
    mockColumns();
    view.initFiles();
    verify(view.files).setSelectionMode(SelectionMode.MULTI);
  }

  @Test
  public void files_Columns() {
    view.initFiles();
    assertEquals(3, view.files.getColumns().size());
    assertNotNull(view.files.getColumnByKey(NAME));
    assertNotNull(view.files.getColumnByKey(PARENT));
    assertNotNull(view.files.getColumnByKey(INPUT));
  }

  @Test
  public void files_ColumnsValueProvider() {
    mockColumns();
    view.initFiles();
    verify(view.files).addColumn(valueProviderCaptor.capture(), eq(NAME));
    ValueProvider<MascotFile, String> valueProvider = valueProviderCaptor.getValue();
    for (MascotFile file : mascotFiles) {
      assertEquals(file.file.getFileName().toString(), valueProvider.apply(file));
    }
    verify(view.files).addColumn(valueProviderCaptor.capture(), eq(PARENT));
    valueProvider = valueProviderCaptor.getValue();
    for (MascotFile file : mascotFiles) {
      if (file.file.getParent() != null) {
        assertEquals(file.file.getParent().getFileName().toString(), valueProvider.apply(file));
      } else {
        assertEquals("", valueProvider.apply(file));
      }
    }
    verify(view.files).addColumn(valueProviderCaptor.capture(), eq(INPUT));
    valueProvider = valueProviderCaptor.getValue();
    for (MascotFile file : mascotFiles) {
      assertEquals(file.inputFile, valueProvider.apply(file));
    }
  }
}
