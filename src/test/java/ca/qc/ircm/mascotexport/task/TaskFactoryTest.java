/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.task;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;

import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import ca.qc.ircm.task.Command;
import ca.qc.ircm.task.ExecutorTask;
import ca.qc.ircm.task.Task;
import ca.qc.ircm.task.TaskContext;
import java.util.concurrent.ExecutorService;
import javax.inject.Inject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Tests {@link TaskFactory}.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class TaskFactoryTest {
  @Inject
  @InjectMocks
  private TaskFactory taskFactory;
  @Mock
  private Command command;
  @Mock
  private TaskContext context;
  @Mock
  private ExecutorService executor;

  @Before
  public void beforeTest() {
    taskFactory = new TaskFactory();
  }

  @Test
  public void create() {
    Task task = taskFactory.create(command, context, executor);

    assertEquals(ExecutorTask.class, task.getClass());
    verify(context).setTask(task);
  }
}
