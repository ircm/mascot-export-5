/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.test.config;

import ca.qc.ircm.mascotexport.job.web.AddJobsView;
import ca.qc.ircm.mascotexport.job.web.AddJobsViewElement;
import ca.qc.ircm.mascotexport.job.web.JobsView;
import ca.qc.ircm.mascotexport.job.web.JobsViewElement;
import ca.qc.ircm.mascotexport.web.MainView;
import ca.qc.ircm.mascotexport.web.component.NotificationComponentTestView;
import com.vaadin.flow.component.orderedlayout.testbench.VerticalLayoutElement;
import com.vaadin.testbench.TestBenchTestCase;
import org.springframework.beans.factory.annotation.Value;

/**
 * Additional functions for TestBenchTestCase.
 */
public abstract class AbstractTestBenchTestCase extends TestBenchTestCase {
  @Value("http://localhost:${local.server.port}")
  protected String baseUrl;

  protected String homeUrl() {
    return baseUrl + "/";
  }

  protected String viewUrl(String view) {
    return baseUrl + "/" + view;
  }

  protected String viewUrl(String view, String parameters) {
    return baseUrl + "/" + view + "/" + parameters;
  }

  protected void openMainView() {
    openView(MainView.VIEW_NAME);
  }

  protected JobsViewElement openJobsView() {
    openView(JobsView.VIEW_NAME);
    return $(JobsViewElement.class).onPage().id(JobsView.VIEW_NAME);
  }

  protected AddJobsViewElement openAddJobsView() {
    openView(AddJobsView.VIEW_NAME);
    return $(AddJobsViewElement.class).onPage().id(AddJobsView.VIEW_NAME);
  }

  protected VerticalLayoutElement openNotificationComponentTestView() {
    openView(NotificationComponentTestView.VIEW_NAME);
    return $(VerticalLayoutElement.class).onPage().id(NotificationComponentTestView.VIEW_NAME);
  }

  protected void openView(String view) {
    openView(view, null);
  }

  protected void openView(String view, String parameters) {
    String url = viewUrl(view);
    if (parameters != null && !parameters.isEmpty()) {
      url += "/" + parameters;
    }
    if (url.equals(getDriver().getCurrentUrl())) {
      getDriver().navigate().refresh();
    } else {
      getDriver().get(url);
    }
  }
}
