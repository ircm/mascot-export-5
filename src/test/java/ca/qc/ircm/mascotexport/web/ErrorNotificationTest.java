/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.web;

import static org.junit.Assert.assertEquals;

import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import com.vaadin.flow.component.notification.Notification.Position;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class ErrorNotificationTest {
  @Test
  public void constructor() {
    ErrorNotification notification = new ErrorNotification();

    assertEquals("", notification.getText());
    assertEquals(ErrorNotification.DEFAULT_DURATION, notification.getDuration());
    assertEquals(ErrorNotification.DEFAULT_POSITION, notification.getPosition());
  }

  @Test
  public void constructor_Text() {
    ErrorNotification notification = new ErrorNotification("test");

    assertEquals("test", notification.getText());
    assertEquals(ErrorNotification.DEFAULT_DURATION, notification.getDuration());
    assertEquals(ErrorNotification.DEFAULT_POSITION, notification.getPosition());
  }

  @Test
  public void constructor_TextDuration() {
    ErrorNotification notification = new ErrorNotification("test", 500);

    assertEquals("test", notification.getText());
    assertEquals(500, notification.getDuration());
    assertEquals(ErrorNotification.DEFAULT_POSITION, notification.getPosition());
  }

  @Test
  public void constructor_TextDurationPosition() {
    ErrorNotification notification = new ErrorNotification("test", 500, Position.MIDDLE);

    assertEquals("test", notification.getText());
    assertEquals(500, notification.getDuration());
    assertEquals(Position.MIDDLE, notification.getPosition());
  }

  @Test
  public void getText() {
    ErrorNotification notification = new ErrorNotification("test");

    assertEquals("test", notification.getText());
  }

  @Test
  public void setText() {
    ErrorNotification notification = new ErrorNotification();
    notification.setText("test");

    assertEquals("test", notification.getText());
  }
}
