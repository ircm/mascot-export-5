/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.web.component;

import static org.junit.Assert.assertEquals;

import ca.qc.ircm.mascotexport.test.config.AbstractTestBenchTestCase;
import ca.qc.ircm.mascotexport.test.config.TestBenchTestAnnotations;
import com.vaadin.flow.component.button.testbench.ButtonElement;
import com.vaadin.flow.component.notification.testbench.NotificationElement;
import com.vaadin.flow.component.orderedlayout.testbench.VerticalLayoutElement;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@TestBenchTestAnnotations
public class NotificationComponentIntegrationTest extends AbstractTestBenchTestCase {
  @Test
  public void showNotification() throws Throwable {
    VerticalLayoutElement layout = openNotificationComponentTestView();
    layout.$(ButtonElement.class).first().click();
    NotificationElement notification = $(NotificationElement.class).waitForFirst();
    assertEquals(NotificationComponentTestView.NOTIFICATION_TEXT, notification.getText());
  }
}
