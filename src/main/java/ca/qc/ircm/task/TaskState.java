/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.task;

import java.util.Date;

/**
 * State of a {@link Task}.
 */
public class TaskState {
  private final TaskExecutionState state;
  private final boolean cancelled;
  private final Throwable exception;
  private final Date completionMoment;

  protected TaskState(TaskExecutionState state, boolean cancelled, Throwable exception,
      Date completionMoment) {
    if (state == null) {
      throw new NullPointerException("state cannot be null");
    }
    this.state = state;
    this.cancelled = cancelled;
    this.exception = exception;
    this.completionMoment = completionMoment;
  }

  /**
   * Returns true if task is currently running.
   *
   * @return true if task is currently running
   */
  public boolean isRunning() {
    return state == TaskExecutionState.RUNNING;
  }

  /**
   * Returns true if this task completed. <br>
   * Completion may be due to normal termination, an exception, or cancellation -- in all of these
   * cases, this method will return true.
   *
   * @return true if this task completed
   */
  public boolean isDone() {
    return state == TaskExecutionState.COMPLETED;
  }

  /**
   * Returns true if this task was cancelled before it completed normally.
   *
   * @return true if this task was cancelled before it completed normally
   */
  public boolean isCancelled() {
    return cancelled;
  }

  /**
   * Returns exception that prevented task to complete normally or null if it completed normally.
   *
   * @return exception that prevented task to complete normally or null if it completed normally
   */
  public Throwable getException() {
    return exception;
  }

  /**
   * Returns moment when task completed.
   *
   * @return moment when task completed
   */
  public Date getCompletionMoment() {
    return completionMoment != null ? (Date) completionMoment.clone() : null;
  }
}
