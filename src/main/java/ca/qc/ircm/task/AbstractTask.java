/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.task;

import java.util.Date;
import java.util.Locale;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Partial implementation of {@link Task} that handles {@link TaskState}.
 */
public abstract class AbstractTask implements Task {
  private static final Logger logger = LoggerFactory.getLogger(AbstractTask.class);

  private class StateUpdater implements Runnable {
    private final ExceptionRunnable runnable;

    private StateUpdater(ExceptionRunnable runnable) {
      this.runnable = runnable;
    }

    @Override
    public void run() {
      state = TaskExecutionState.RUNNING;
      try {
        runnable.run();
      } catch (InterruptedException exception) {
        cancelled = true;
      } catch (Exception exception) {
        logger.error("Could not complete task {}", id, exception);
        AbstractTask.this.exception = exception;
      } finally {
        state = TaskExecutionState.COMPLETED;
        completionMoment = new Date();
      }
    }
  }

  private final UUID id;
  protected final TaskContext context;
  protected final Command command;
  private TaskExecutionState state;
  private boolean cancelled;
  private Throwable exception;
  private Date completionMoment;

  protected AbstractTask(Command command, TaskContext context) {
    id = UUID.randomUUID();
    this.context = context;
    this.command = command;
    context.setTask(this);
    state = TaskExecutionState.NEW;
  }

  @Override
  public UUID getId() {
    return id;
  }

  @Override
  public TaskContext getContext() {
    return context;
  }

  @Override
  public TaskState getState() {
    return new TaskState(state, cancelled, exception, completionMoment);
  }

  protected Runnable createRunnable(final Command command, final boolean undo) {
    cancelled = false;
    state = TaskExecutionState.NEW;
    return new StateUpdater(new ExceptionRunnable() {
      @Override
      public void run() throws InterruptedException, Exception {
        if (undo) {
          command.undo();
        } else {
          command.execute();
        }
      }
    });
  }

  protected void wasCancelled() {
    cancelled = true;
  }

  @Override
  public String getDescription(Locale locale) {
    return command.getDescription(locale);
  }

  @Override
  public String getErrorDescription(Locale locale) {
    return command.getErrorDescription(getState().getException(), locale);
  }
}