/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.task;

import java.util.Locale;
import java.util.UUID;

/**
 * Represents a complex task that the program can execute.
 */
public interface Task {
  /**
   * Returns task identifier.
   *
   * @return task identifier
   */
  public UUID getId();

  /**
   * Execute task.
   */
  public void execute();

  /**
   * Cancel task.
   */
  public void cancel();

  /**
   * Waits until task completes.
   *
   * @throws InterruptedException
   *           thread was interrupted
   */
  public void join() throws InterruptedException;

  /**
   * Returns true if task can be undone, false otherwise.
   *
   * @return true if task can be undone, false otherwise
   */
  public boolean canUndo();

  /**
   * Undo task. <br>
   * <strong>After calling {@link #undo()}, {@link #getState()} will return the state of the undo
   * operation rather than the state of the normal execution.</strong>
   *
   * @throws UnsupportedOperationException
   *           if task cannot be undone
   */
  public void undo();

  /**
   * Returns task execution state.
   *
   * @return task execution state
   */
  public TaskState getState();

  /**
   * Returns task's context.
   *
   * @return task's context
   */
  public TaskContext getContext();

  /**
   * Returns the description of command.
   *
   * @param locale
   *          user's locale
   * @return description of command
   */
  public String getDescription(Locale locale);

  /**
   * Returns the description of the error that occurred with command, if any.
   *
   * @param locale
   *          user's locale
   * @return description of the error that occurred with command, if any
   */
  public String getErrorDescription(Locale locale);
}
