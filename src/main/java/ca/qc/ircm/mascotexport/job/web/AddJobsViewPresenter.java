/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.ExportFormat.CSV;
import static ca.qc.ircm.mascotexport.job.web.AddJobsView.FILES_EMPTY;
import static ca.qc.ircm.mascotexport.job.web.AddJobsView.LOAD_IO_EXCEPTION;
import static ca.qc.ircm.mascotexport.web.WebConstants.FIELD_NOTIFICATION;

import ca.qc.ircm.mascotexport.ProteinScoring;
import ca.qc.ircm.mascotexport.job.ExportJob;
import ca.qc.ircm.mascotexport.job.ExportJobService;
import ca.qc.ircm.mascotexport.job.ExportParameters;
import ca.qc.ircm.mascotexport.job.PeptideParameters;
import ca.qc.ircm.mascotexport.job.ProteinParameters;
import ca.qc.ircm.mascotexport.job.QueryParameters;
import ca.qc.ircm.mascotexport.job.SearchParameters;
import ca.qc.ircm.mascotexport.mascot.MascotFile;
import ca.qc.ircm.mascotexport.mascot.MascotFileFilter;
import ca.qc.ircm.mascotexport.mascot.MascotService;
import ca.qc.ircm.mascotexport.web.WebConstants;
import ca.qc.ircm.text.MessageResource;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.spring.annotation.SpringComponent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

/**
 * Add jobs view presenter.
 */
@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AddJobsViewPresenter {
  private static final Logger logger = LoggerFactory.getLogger(AddJobsViewPresenter.class);
  private AddJobsView view;
  private MascotFileFilter filter = new MascotFileFilter();
  @Inject
  private ExportJobService jobService;
  @Inject
  private MascotService mascotService;

  protected AddJobsViewPresenter() {
  }

  protected AddJobsViewPresenter(ExportJobService jobService, MascotService mascotService) {
    this.jobService = jobService;
    this.mascotService = mascotService;
  }

  /**
   * Initializes presenter.
   *
   * @param view
   *          view
   */
  void init(AddJobsView view) {
    this.view = view;
    try {
      List<MascotFile> files = mascotService.allFilesWithInput();
      Collections.sort(files, (o1, o2) -> o2.file.compareTo(o1.file));
      ListDataProvider<MascotFile> filesDataProvider = DataProvider.ofCollection(files);
      filesDataProvider.setFilter(filter);
      view.files.setDataProvider(filesDataProvider);
    } catch (IOException e) {
      final MessageResource resources = new MessageResource(AddJobsView.class, view.getLocale());
      view.showError(resources.message(LOAD_IO_EXCEPTION));
    }
    view.files.addSelectionListener(e -> validateFiles());
    view.nameFilter.addValueChangeListener(e -> {
      filter.nameContains = e.getValue();
      view.files.getDataProvider().refreshAll();
    });
    view.nameFilter.setValueChangeMode(ValueChangeMode.EAGER);
    view.parentFilter.addValueChangeListener(e -> {
      filter.parentContains = e.getValue();
      view.files.getDataProvider().refreshAll();
    });
    view.parentFilter.setValueChangeMode(ValueChangeMode.EAGER);
    view.inputFilter.addValueChangeListener(e -> {
      filter.inputFileContains = e.getValue();
      view.files.getDataProvider().refreshAll();
    });
    view.inputFilter.setValueChangeMode(ValueChangeMode.EAGER);
    view.save.addClickListener(e -> save());
    view.cancel.addClickListener(e -> cancel());
    ExportJob job = defaultValues();
    view.exportParametersForm.setBean(job.getExportParameters());
    view.searchParametersForm.setBean(job.getSearchParameters());
    view.proteinParametersForm.setBean(job.getProteinParameters());
    view.peptideParametersForm.setBean(job.getPeptideParameters());
    view.queryParametersForm.setBean(job.getQueryParameters());
    validateFiles();
  }

  private ExportJob defaultValues() {
    final ExportJob job = new ExportJob();
    ExportParameters exportParameters = new ExportParameters();
    exportParameters.setExportFormat(CSV);
    exportParameters.setSigThreshold(0.05);
    exportParameters.setIgnoreIonsScoreBelow(15);
    exportParameters.setUseHomology(false);
    exportParameters.setReport(200);
    exportParameters.setServerMudpitSwitch(ProteinScoring.STANDARD);
    exportParameters.setShowSameSets(true);
    exportParameters.setShowSubsets(1);
    exportParameters.setGroupFamily(false);
    exportParameters.setRequireBoldRed(true);
    job.setExportParameters(exportParameters);
    SearchParameters searchParameters = new SearchParameters();
    searchParameters.setMaster(true);
    searchParameters.setShowHeader(true);
    searchParameters.setShowMods(true);
    searchParameters.setShowParams(true);
    searchParameters.setShowFormat(true);
    searchParameters.setShowMasses(false);
    job.setSearchParameters(searchParameters);
    ProteinParameters proteinParameters = new ProteinParameters();
    proteinParameters.setMaster(true);
    proteinParameters.setScore(true);
    proteinParameters.setDesc(true);
    proteinParameters.setMass(true);
    proteinParameters.setMatches(true);
    proteinParameters.setCover(true);
    proteinParameters.setLen(true);
    proteinParameters.setPi(true);
    proteinParameters.setTaxStr(true);
    proteinParameters.setTaxId(true);
    proteinParameters.setSeq(false);
    proteinParameters.setEmpai(true);
    job.setProteinParameters(proteinParameters);
    PeptideParameters peptideParameters = new PeptideParameters();
    peptideParameters.setMaster(true);
    peptideParameters.setExpMr(true);
    peptideParameters.setExpZ(true);
    peptideParameters.setCalcMr(true);
    peptideParameters.setDelta(true);
    peptideParameters.setStart(true);
    peptideParameters.setEnd(true);
    peptideParameters.setMiss(true);
    peptideParameters.setScore(true);
    peptideParameters.setHomol(false);
    peptideParameters.setIdent(false);
    peptideParameters.setExpect(true);
    peptideParameters.setSeq(true);
    peptideParameters.setFrame(false);
    peptideParameters.setVarMod(true);
    peptideParameters.setNumMatch(true);
    peptideParameters.setScanTitle(true);
    peptideParameters.setShowUnassigned(false);
    peptideParameters.setShowPepDupes(false);
    job.setPeptideParameters(peptideParameters);
    QueryParameters queryParameters = new QueryParameters();
    queryParameters.setMaster(false);
    queryParameters.setTitle(false);
    queryParameters.setQualifiers(false);
    queryParameters.setParams(false);
    queryParameters.setPeaks(false);
    queryParameters.setRaw(false);
    job.setQueryParameters(queryParameters);
    return job;
  }

  private void validateFiles() {
    if (view.files.getSelectedItems().isEmpty()) {
      final MessageResource viewResources =
          new MessageResource(AddJobsView.class, view.getLocale());
      view.filesError.setVisible(true);
      view.filesError.setText(viewResources.message(FILES_EMPTY));
    } else {
      view.filesError.setVisible(false);
    }
  }

  private boolean validateSave() {
    logger.debug("Validate jobs");
    boolean valid = true;
    valid = !view.files.getSelectedItems().isEmpty();
    valid &= view.exportParametersForm.validate().isOk();
    valid &= view.searchParametersForm.validate().isOk();
    valid &= view.proteinParametersForm.validate().isOk();
    valid &= view.peptideParametersForm.validate().isOk();
    valid &= view.queryParametersForm.validate().isOk();
    if (!valid) {
      final MessageResource generalResources =
          new MessageResource(WebConstants.class, view.getLocale());
      view.showError(generalResources.message(FIELD_NOTIFICATION));
    }
    return valid;
  }

  /**
   * Saves jobs.
   */
  void save() {
    if (validateSave()) {
      Set<MascotFile> files = view.files.getSelectedItems();
      logger.debug("Save jobs for {} files to database", files.size());
      try {
        List<ExportJob> jobs = new ArrayList<>();
        for (MascotFile file : files) {
          ExportJob job = createJob(file);
          jobs.add(job);
        }
        jobService.insert(jobs);
        view.navigate(JobsView.class);
      } catch (ValidationException e) {
        final MessageResource generalResources =
            new MessageResource(WebConstants.class, view.getLocale());
        view.showError(generalResources.message(FIELD_NOTIFICATION));
      }
    }
  }

  private ExportJob createJob(MascotFile file) throws ValidationException {
    ExportJob job = new ExportJob();
    job.setFilePath(file.file.toString());
    job.setInput(file.inputFile);
    job.setExportParameters(new ExportParameters());
    view.exportParametersForm.writeBean(job.getExportParameters());
    job.setSearchParameters(new SearchParameters());
    view.searchParametersForm.writeBean(job.getSearchParameters());
    job.setProteinParameters(new ProteinParameters());
    view.proteinParametersForm.writeBean(job.getProteinParameters());
    job.setPeptideParameters(new PeptideParameters());
    view.peptideParametersForm.writeBean(job.getPeptideParameters());
    job.setQueryParameters(new QueryParameters());
    view.queryParametersForm.writeBean(job.getQueryParameters());
    return job;
  }

  /**
   * Cancel jobs addition.
   */
  void cancel() {
    view.navigate(JobsView.class);
  }

  MascotFileFilter getFilter() {
    return filter;
  }
}
