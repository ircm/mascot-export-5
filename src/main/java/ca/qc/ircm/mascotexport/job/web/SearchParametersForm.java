/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.SearchParametersProperties.MASTER;
import static ca.qc.ircm.mascotexport.job.SearchParametersProperties.SHOW_FORMAT;
import static ca.qc.ircm.mascotexport.job.SearchParametersProperties.SHOW_HEADER;
import static ca.qc.ircm.mascotexport.job.SearchParametersProperties.SHOW_MASSES;
import static ca.qc.ircm.mascotexport.job.SearchParametersProperties.SHOW_MODS;
import static ca.qc.ircm.mascotexport.job.SearchParametersProperties.SHOW_PARAMS;

import ca.qc.ircm.mascotexport.job.SearchParameters;
import ca.qc.ircm.text.MessageResource;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.BinderValidationStatus;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.i18n.LocaleChangeEvent;
import com.vaadin.flow.i18n.LocaleChangeObserver;

/**
 * Search parameters form.
 */
public class SearchParametersForm extends Composite<VerticalLayout>
    implements LocaleChangeObserver {
  public static final String STYLE = "search-parameters";
  private static final long serialVersionUID = 3178924928182492823L;
  protected Checkbox master = new Checkbox();
  protected Checkbox showHeader = new Checkbox();
  protected Checkbox showMods = new Checkbox();
  protected Checkbox showParams = new Checkbox();
  protected Checkbox showFormat = new Checkbox();
  protected Checkbox showMasses = new Checkbox();
  private Binder<SearchParameters> binder = new Binder<>(SearchParameters.class);
  private boolean readOnly;

  /**
   * Creates search parameter form.
   */
  public SearchParametersForm() {
    VerticalLayout root = getContent();
    root.setPadding(false);
    root.setSpacing(false);
    root.addClassName(STYLE);
    root.add(master);
    master.addClassName(MASTER);
    master.addClassName("large");
    binder.bind(master, MASTER);
    VerticalLayout subElements = new VerticalLayout();
    root.add(subElements);
    subElements.setSpacing(false);
    subElements.add(showHeader);
    showHeader.addClassName(SHOW_HEADER);
    binder.bind(showHeader, SHOW_HEADER);
    subElements.add(showMods);
    showMods.addClassName(SHOW_MODS);
    binder.bind(showMods, SHOW_MODS);
    subElements.add(showParams);
    showParams.addClassName(SHOW_PARAMS);
    binder.bind(showParams, SHOW_PARAMS);
    subElements.add(showFormat);
    showFormat.addClassName(SHOW_FORMAT);
    binder.bind(showFormat, SHOW_FORMAT);
    subElements.add(showMasses);
    showMasses.addClassName(SHOW_MASSES);
    binder.bind(showMasses, SHOW_MASSES);
  }

  @Override
  public void localeChange(LocaleChangeEvent event) {
    final MessageResource resources = new MessageResource(SearchParameters.class, getLocale());
    master.setLabel(resources.message(MASTER));
    showHeader.setLabel(resources.message(SHOW_HEADER));
    showMods.setLabel(resources.message(SHOW_MODS));
    showParams.setLabel(resources.message(SHOW_PARAMS));
    showFormat.setLabel(resources.message(SHOW_FORMAT));
    showMasses.setLabel(resources.message(SHOW_MASSES));
  }

  public boolean isReadOnly() {
    return readOnly;
  }

  public void setReadOnly(boolean readOnly) {
    this.readOnly = readOnly;
    binder.setReadOnly(readOnly);
  }

  public SearchParameters getBean() {
    return binder.getBean();
  }

  public void setBean(SearchParameters searchParameters) {
    binder.setBean(searchParameters);
  }

  public BinderValidationStatus<SearchParameters> validate() {
    return binder.validate();
  }

  public void writeBean(SearchParameters searchParameters) throws ValidationException {
    binder.writeBean(searchParameters);
  }
}
