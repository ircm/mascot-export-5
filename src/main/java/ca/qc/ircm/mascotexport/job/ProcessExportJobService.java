/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job;

import ca.qc.ircm.mascotexport.MascotConfiguration;
import ca.qc.ircm.mascotexport.MascotExportConfiguration;
import ca.qc.ircm.mascotexport.mascot.MascotParser;
import ca.qc.ircm.mascotexport.mascot.MascotService;
import ca.qc.ircm.mascotexport.task.TaskFactory;
import ca.qc.ircm.mascotexport.utils.ExceptionUtils;
import ca.qc.ircm.progressbar.ProgressBar;
import ca.qc.ircm.progressbar.ProgressBarWithListeners;
import ca.qc.ircm.task.AbstractCommand;
import ca.qc.ircm.task.Command;
import ca.qc.ircm.task.Task;
import ca.qc.ircm.task.TaskContext;
import ca.qc.ircm.text.MessageResource;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.inject.Inject;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.output.TeeOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Services for processing jobs.
 */
@Component
public class ProcessExportJobService {
  private static final String INTERRUPTED_MESSAGE = "Interrupted job processing";
  private final Logger logger = LoggerFactory.getLogger(ProcessExportJobService.class);
  @Inject
  private ExportJobService jobService;
  @Inject
  private MascotService mascotService;
  @Inject
  private ExecutorService executorService;
  @Inject
  private TaskFactory taskFactory;
  @Inject
  private RunningExportJobs runningJobs;
  @Inject
  private MascotConfiguration mascotConfiguration;
  @Inject
  private MascotExportConfiguration mascotExportConfiguration;
  @Inject
  private MascotParser mascotParser;

  protected ProcessExportJobService() {
  }

  protected ProcessExportJobService(ExportJobService jobService, MascotService mascotService,
      ExecutorService executorService, TaskFactory taskFactory, RunningExportJobs runningJobs,
      MascotConfiguration mascotConfiguration, MascotExportConfiguration mascotExportConfiguration,
      MascotParser mascotParser) {
    this.jobService = jobService;
    this.mascotService = mascotService;
    this.executorService = executorService;
    this.taskFactory = taskFactory;
    this.runningJobs = runningJobs;
    this.mascotConfiguration = mascotConfiguration;
    this.mascotExportConfiguration = mascotExportConfiguration;
    this.mascotParser = mascotParser;
  }

  private MessageResource getResources(Locale locale) {
    return new MessageResource(ProcessExportJobService.class, locale);
  }

  private String getFormat(ExportJob job) {
    return job.getExportParameters().getExportFormat().name().toLowerCase();
  }

  private void process(ExportJob job, Locale locale) {
    MessageResource resources = getResources(locale);
    final ProgressBarWithListeners progressBar = new ProgressBarWithListeners();
    progressBar.setTitle(resources.message("job", job.getFilePath()));
    progressBar.setMessage(resources.message("waiting"));
    final TaskContext taskContext = new ExportJobTaskContext(progressBar, job);
    Command command = new AbstractCommand() {
      @Override
      public void execute() throws InterruptedException, Exception {
        MessageResource resources = getResources(Locale.getDefault());
        startProcessTask(job, progressBar, resources, taskContext.getTask());
      }
    };
    Task task = taskFactory.create(command, taskContext, executorService);
    task.execute();
    runningJobs.add(job, task);
  }

  /**
   * Process jobs.
   *
   * @param jobs
   *          jobs to process
   * @param locale
   *          locale
   */
  public void process(final Collection<ExportJob> jobs, final Locale locale) {
    for (ExportJob job : jobs) {
      process(job, locale);
    }
  }

  private void startProcessTask(final ExportJob job, final ProgressBar progressBar,
      final MessageResource resources, final Task task) throws IOException, InterruptedException {
    Path root = mascotConfiguration.getData();
    Path jobFile = root.resolve(job.getFilePath());

    ExceptionUtils.throwIfInterrupted(INTERRUPTED_MESSAGE);

    // Prepare output file.
    String inputFilename = mascotParser.parseFilename(jobFile);
    Path baseOutputDirectory = mascotExportConfiguration.getOutput();
    Path outputDirectory = baseOutputDirectory.resolve(job.getFilePath()).getParent();
    if (outputDirectory == null) {
      throw new IllegalStateException("parent directory for "
          + baseOutputDirectory.resolve(job.getFilePath()) + " cannot be null");
    }
    Files.createDirectories(outputDirectory);
    // Add RAW file name without extension.
    StringBuilder newFileName = new StringBuilder();
    newFileName.append(FilenameUtils.getBaseName(inputFilename));
    newFileName.append("__");
    // Add DAT file name without extension.
    newFileName.append(FilenameUtils.getBaseName(job.getFilePath()));
    // Add appropriate extension.
    newFileName.append(".");
    newFileName.append(getFormat(job));
    Path newFile = outputDirectory.resolve(newFileName.toString());

    ExceptionUtils.throwIfInterrupted(INTERRUPTED_MESSAGE);

    logger.debug("Convert file {} to {}", new Object[] { jobFile, newFile });

    // Copy exported content to file.
    try (
        BufferedOutputStream bufferedFileOutput =
            new BufferedOutputStream(Files.newOutputStream(newFile, StandardOpenOption.CREATE));
        FileChannel channel = FileChannel.open(newFile, StandardOpenOption.WRITE)) {
      try (FileLock lock = channel.tryLock()) {
        if (lock == null) {
          throw new FileLockException("File " + newFile + " is already locked");
        }
        OutputStream output = new TeeOutputStream(bufferedFileOutput, new OutputStream() {
          private long count;

          @Override
          public void write(int oneByte) throws IOException {
            ++count;
            if (count % 1024 == 0) {
              progressBar.setMessage(resources.message("data", count / 1024));
            }
          }
        });
        Map<String, List<String>> parameters =
            job.commandLineParameters(mascotConfiguration.getMinimumPeptideLenghtSummary());
        mascotService.export(jobFile, parameters, output);
        jobService.jobDone(job);
        progressBar.setMessage(resources.message("done"));
      } catch (OverlappingFileLockException exception) {
        throw new FileLockException("File " + newFile + " is already locked");
      }
    } catch (IOException | InterruptedException | RuntimeException exception) {
      progressBar.setMessage(exception.getMessage());
      throw exception;
    }
  }

  /**
   * Interrupt job.
   *
   * @param job
   *          job to interrupt
   */
  public void interrupt(ExportJob job) {
    Task task = runningJobs.get(job);
    if (task != null) {
      task.cancel();
    }
  }

  /**
   * Returns job's current output length in bytes.
   *
   * @param job
   *          job
   * @return job's current output length in bytes
   */
  public long outputLength(ExportJob job) {
    Task task = runningJobs.get(job);
    if (task != null) {
      String dataMessage = task.getContext().getProgressBar().getMessage();
      MessageResource resources = getResources(Locale.getDefault());
      Pattern pattern = Pattern.compile(resources.message("data.pattern"));
      Matcher matcher = pattern.matcher(dataMessage);
      if (matcher.matches()) {
        return Long.parseLong(matcher.group(1)) * 1024;
      } else {
        return -1L;
      }
    } else {
      return -1L;
    }
  }
}
