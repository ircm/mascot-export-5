/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job;

import ca.qc.ircm.progressbar.ProgressBar;
import ca.qc.ircm.task.TaskContext;
import java.util.Arrays;
import java.util.Collection;

/**
 * Task context for processing jobs.
 */
public class ExportJobTaskContext extends TaskContext {
  private final Collection<ExportJob> jobs;

  public ExportJobTaskContext(ProgressBar progressBar, ExportJob... jobs) {
    super(progressBar);
    this.jobs = Arrays.asList(jobs);
  }

  public ExportJobTaskContext(ProgressBar progressBar, Collection<ExportJob> jobs) {
    super(progressBar);
    this.jobs = jobs;
  }

  /**
   * Returns task's jobs.
   *
   * @return task's jobs
   */
  public Collection<ExportJob> getJobs() {
    return jobs;
  }
}
