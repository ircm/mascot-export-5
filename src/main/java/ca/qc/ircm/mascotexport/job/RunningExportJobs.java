/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job;

import ca.qc.ircm.task.Task;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Contains jobs that are running.
 */
@Component
public class RunningExportJobs {
  private static final Duration OLD_DURATION = Duration.ofDays(7);
  private static final Logger logger = LoggerFactory.getLogger(RunningExportJobs.class);
  private Map<String, Task> jobs = new ConcurrentHashMap<>();

  public Task get(ExportJob job) {
    return jobs.get(job.getId());
  }

  public void add(ExportJob job, Task task) {
    jobs.put(job.getId(), task);
  }

  public void remove(ExportJob job) {
    jobs.remove(job.getId());
  }

  /**
   * Removes jobs that are older than a few days.
   */
  public void removeOldJobs() {
    logger.debug("Jobs and tasks {}", jobs);
    List<String> remove = jobs.entrySet().stream().filter(entry -> {
      Date date = entry.getValue().getState().getCompletionMoment();
      return date != null
          && date.toInstant().isBefore(Instant.now().minusMillis(OLD_DURATION.toMillis()));
    }).map(entry -> entry.getKey()).collect(Collectors.toList());
    remove.forEach(key -> jobs.remove(key));
  }
}
