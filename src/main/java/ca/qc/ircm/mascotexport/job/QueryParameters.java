/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job;

import ca.qc.ircm.processing.GeneratePropertyNames;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Export job parameters.
 */
@GeneratePropertyNames
public class QueryParameters implements Serializable {
  private static final long serialVersionUID = 468905419225298927L;
  private boolean master;
  private boolean title;
  private boolean qualifiers;
  private boolean params;
  private boolean peaks;
  private boolean raw;

  /**
   * Returns command line parameters for Mascot.
   *
   * @return command line parameters for Mascot
   */
  public Map<String, List<String>> commandLineParameters() {
    Map<String, List<String>> parameters = new HashMap<>();
    addParameterIfTrue(parameters, master, "query_master");
    addParameterIfTrue(parameters, title, "query_title");
    addParameterIfTrue(parameters, qualifiers, "query_qualifiers");
    addParameterIfTrue(parameters, params, "query_params");
    addParameterIfTrue(parameters, peaks, "query_peaks");
    addParameterIfTrue(parameters, raw, "query_raw");
    return parameters;
  }

  private void addParameterIfTrue(Map<String, List<String>> parameters, boolean value,
      String name) {
    if (master && value) {
      parameters.put(name, parameter(value));
    }
  }

  private List<String> parameter(boolean value) {
    return Arrays.asList(value ? "1" : "0");
  }

  public boolean isMaster() {
    return master;
  }

  public void setMaster(boolean master) {
    this.master = master;
  }

  public boolean isTitle() {
    return title;
  }

  public void setTitle(boolean title) {
    this.title = title;
  }

  public boolean isQualifiers() {
    return qualifiers;
  }

  public void setQualifiers(boolean qualifiers) {
    this.qualifiers = qualifiers;
  }

  public boolean isParams() {
    return params;
  }

  public void setParams(boolean params) {
    this.params = params;
  }

  public boolean isPeaks() {
    return peaks;
  }

  public void setPeaks(boolean peaks) {
    this.peaks = peaks;
  }

  public boolean isRaw() {
    return raw;
  }

  public void setRaw(boolean raw) {
    this.raw = raw;
  }
}
