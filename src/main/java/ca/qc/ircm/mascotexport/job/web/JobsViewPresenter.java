/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.web.JobsView.EXPORT_DONE;
import static ca.qc.ircm.mascotexport.job.web.JobsView.JOBS_EMPTY;

import ca.qc.ircm.mascotexport.job.ExportJob;
import ca.qc.ircm.mascotexport.job.ExportJobService;
import ca.qc.ircm.mascotexport.job.ProcessExportJobService;
import ca.qc.ircm.mascotexport.job.RunningExportJobs;
import ca.qc.ircm.task.Task;
import ca.qc.ircm.text.MessageResource;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.provider.ListDataProvider;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * Jobs presenter.
 */
@Controller
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class JobsViewPresenter {
  private static final Logger logger = LoggerFactory.getLogger(JobsViewPresenter.class);
  private JobsView view;
  private ListDataProvider<ExportJob> jobsDataProvider;
  @Inject
  private ExportJobService exportJobService;
  @Inject
  private ProcessExportJobService processExportJobService;
  @Inject
  private RunningExportJobs runningJobs;

  protected JobsViewPresenter() {
  }

  protected JobsViewPresenter(ExportJobService exportJobService,
      ProcessExportJobService processExportJobService, RunningExportJobs runningJobs) {
    this.exportJobService = exportJobService;
    this.processExportJobService = processExportJobService;
    this.runningJobs = runningJobs;
  }

  void init(JobsView view) {
    this.view = view;
    refreshJobs();
    view.export.addClickListener(e -> export());
    view.cancel.addClickListener(e -> cancel());
    view.add.addClickListener(e -> add());
    view.removeMany.addClickListener(e -> removeMany());
    view.removeDone.addClickListener(e -> removeDone());
    view.refresh.addClickListener(e -> refresh());
    runningJobs.removeOldJobs();
  }

  private void refreshJobs() {
    jobsDataProvider = DataProvider.ofCollection(exportJobService.all());
    view.jobs.setDataProvider(jobsDataProvider);
  }

  private void export() {
    Set<ExportJob> jobs = view.jobs.getSelectedItems();
    if (!jobs.isEmpty()) {
      logger.debug("Export jobs {}", jobs);
      processExportJobService.process(jobs, view.getLocale());
      MessageResource resources = new MessageResource(JobsView.class, view.getLocale());
      view.showNotification(resources.message(EXPORT_DONE, jobs.size()));
    } else {
      MessageResource resources = new MessageResource(JobsView.class, view.getLocale());
      view.showNotification(resources.message(JOBS_EMPTY));
    }
  }

  private void cancel() {
    Set<ExportJob> jobs = view.jobs.getSelectedItems();
    logger.debug("Cancel jobs {}", jobs);
    for (ExportJob job : jobs) {
      Task task = runningJobs.get(job);
      if (task != null) {
        task.cancel();
      }
    }
  }

  private void add() {
    view.navigate(AddJobsView.class);
  }

  ComponentEventListener<ClickEvent<Button>> viewButtonListener(ExportJob job) {
    return e -> {
      logger.debug("View job {}", job);
      JobParametersDialog dialog = new JobParametersDialog();
      dialog.setBean(job);
      dialog.setReadOnly(true);
      dialog.open();
    };
  }

  ComponentEventListener<ClickEvent<Button>> removeButtonListener(ExportJob job) {
    return e -> {
      logger.debug("Remove job {}", job);
      exportJobService.delete(job);
      refreshJobs();
    };
  }

  String progress(ExportJob job) {
    Task task = runningJobs.get(job);
    if (task != null) {
      return task.getContext().getProgressBar().getMessage();
    } else {
      return "";
    }
  }

  private void removeMany() {
    Set<ExportJob> jobs = view.jobs.getSelectedItems();
    if (!jobs.isEmpty()) {
      logger.debug("Remove jobs {}", jobs);
      exportJobService.delete(jobs);
      refreshJobs();
    } else {
      MessageResource resources = new MessageResource(JobsView.class, view.getLocale());
      view.showNotification(resources.message(JOBS_EMPTY));
    }
  }

  private void removeDone() {
    Set<ExportJob> jobs = jobsDataProvider.getItems().stream()
        .filter(job -> job.getDoneDate() != null).collect(Collectors.toSet());
    logger.debug("Remove completed jobs {}", jobs);
    exportJobService.delete(jobs);
    refreshJobs();
  }

  private void refresh() {
    logger.debug("Refresh jobs");
    refreshJobs();
  }
}
