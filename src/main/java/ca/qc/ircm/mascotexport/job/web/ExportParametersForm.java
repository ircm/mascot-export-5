/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.ExportFormat.CSV;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.EXPORT_FORMAT;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.GROUP_FAMILY;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.IGNORE_IONS_SCORE_BELOW;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.PREFER_TAXONOMY;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.REPORT;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.REQUIRE_BOLD_RED;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.SERVER_MUDPIT_SWITCH;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.SHOW_SAME_SETS;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.SHOW_SUBSETS;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.SIG_THRESHOLD;
import static ca.qc.ircm.mascotexport.job.ExportParametersProperties.USE_HOMOLOGY;
import static ca.qc.ircm.mascotexport.text.Strings.property;
import static ca.qc.ircm.mascotexport.text.Strings.styleName;
import static ca.qc.ircm.mascotexport.web.WebConstants.INVALID_INTEGER;
import static ca.qc.ircm.mascotexport.web.WebConstants.INVALID_NUMBER;
import static ca.qc.ircm.mascotexport.web.WebConstants.PLACEHOLDER;
import static ca.qc.ircm.mascotexport.web.WebConstants.REQUIRED;
import static ca.qc.ircm.mascotexport.web.WebConstants.TITLE;

import ca.qc.ircm.mascotexport.ProteinScoring;
import ca.qc.ircm.mascotexport.job.ExportFormat;
import ca.qc.ircm.mascotexport.job.ExportParameters;
import ca.qc.ircm.mascotexport.web.WebConstants;
import ca.qc.ircm.text.MessageResource;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.BinderValidationStatus;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.converter.StringToDoubleConverter;
import com.vaadin.flow.data.converter.StringToIntegerConverter;
import com.vaadin.flow.data.renderer.TextRenderer;
import com.vaadin.flow.i18n.LocaleChangeEvent;
import com.vaadin.flow.i18n.LocaleChangeObserver;

/**
 * Export parameters form.
 */
public class ExportParametersForm extends Composite<VerticalLayout>
    implements LocaleChangeObserver {
  public static final String STYLE = "export-parameters";
  public static final String USE_HOMOLOGY_LABEL = styleName(USE_HOMOLOGY, "label");
  public static final String SERVER_MUDPIT_SWITCH_LABEL = styleName(SERVER_MUDPIT_SWITCH, "label");
  private static final long serialVersionUID = -8825475669661550424L;
  protected ComboBox<ExportFormat> exportFormat = new ComboBox<>();
  protected TextField sigThreshold = new TextField();
  protected TextField ignoreIonsScoreBelow = new TextField();
  protected FormLayout useHomologyLayout = new FormLayout();
  protected RadioButtonGroup<Boolean> useHomology = new RadioButtonGroup<>();
  protected Label useHomologyLabel = new Label();
  protected TextField report = new TextField();
  protected FormLayout serverMudpitSwitchLayout = new FormLayout();
  protected RadioButtonGroup<ProteinScoring> serverMudpitSwitch = new RadioButtonGroup<>();
  protected Label serverMudpitSwitchLabel = new Label();
  protected Checkbox showSameSets = new Checkbox();
  protected TextField showSubsets = new TextField();
  protected Checkbox groupFamily = new Checkbox();
  protected Checkbox requireBoldRed = new Checkbox();
  protected Div preferTaxonomy = new Div();
  private Binder<ExportParameters> binder = new Binder<>(ExportParameters.class);
  private boolean readOnly;

  /**
   * Create export parameter form.
   */
  public ExportParametersForm() {
    VerticalLayout root = getContent();
    root.setPadding(false);
    root.setSpacing(false);
    root.addClassName(STYLE);
    root.add(exportFormat);
    exportFormat.addClassName(EXPORT_FORMAT);
    exportFormat.setItems(ExportFormat.values());
    exportFormat.setValue(CSV);
    root.add(sigThreshold);
    sigThreshold.addClassName(SIG_THRESHOLD);
    root.add(ignoreIonsScoreBelow);
    ignoreIonsScoreBelow.addClassName(IGNORE_IONS_SCORE_BELOW);
    root.add(useHomologyLayout);
    useHomologyLayout.addFormItem(useHomology, useHomologyLabel);
    useHomology.addClassName(USE_HOMOLOGY);
    useHomology.setItems(false, true);
    useHomology.setValue(false);
    useHomologyLabel.addClassName(USE_HOMOLOGY_LABEL);
    binder.bind(useHomology, USE_HOMOLOGY);
    root.add(report);
    report.addClassName(REPORT);
    root.add(serverMudpitSwitchLayout);
    serverMudpitSwitchLayout.addFormItem(serverMudpitSwitch, serverMudpitSwitchLabel);
    serverMudpitSwitch.addClassName(SERVER_MUDPIT_SWITCH);
    serverMudpitSwitch.setItems(ProteinScoring.values());
    serverMudpitSwitch.setValue(ProteinScoring.STANDARD);
    serverMudpitSwitchLabel.addClassName(SERVER_MUDPIT_SWITCH_LABEL);
    binder.bind(serverMudpitSwitch, SERVER_MUDPIT_SWITCH);
    root.add(showSameSets);
    showSameSets.addClassName(SHOW_SAME_SETS);
    binder.bind(showSameSets, SHOW_SAME_SETS);
    root.add(showSubsets);
    showSubsets.addClassName(SHOW_SUBSETS);
    root.add(groupFamily);
    groupFamily.addClassName(GROUP_FAMILY);
    binder.bind(groupFamily, GROUP_FAMILY);
    root.add(requireBoldRed);
    requireBoldRed.addClassName(REQUIRE_BOLD_RED);
    binder.bind(requireBoldRed, REQUIRE_BOLD_RED);
    root.add(preferTaxonomy);
    preferTaxonomy.addClassName(PREFER_TAXONOMY);
  }

  @Override
  public void localeChange(LocaleChangeEvent event) {
    final MessageResource resources = new MessageResource(ExportParameters.class, getLocale());
    final MessageResource generalResources = new MessageResource(WebConstants.class, getLocale());
    exportFormat.setLabel(resources.message(EXPORT_FORMAT));
    MessageResource exportFormatResources = new MessageResource(ExportFormat.class, getLocale());
    exportFormat.setItemLabelGenerator(value -> exportFormatResources.message(value.name()));
    binder.forField(exportFormat).asRequired(generalResources.message(REQUIRED))
        .bind(EXPORT_FORMAT);
    sigThreshold.setLabel(resources.message(SIG_THRESHOLD));
    sigThreshold.setPlaceholder(resources.message(property(SIG_THRESHOLD, PLACEHOLDER)));
    binder.forField(sigThreshold).withNullRepresentation("")
        .withConverter(new StringToDoubleConverter(generalResources.message(INVALID_NUMBER)))
        .bind(SIG_THRESHOLD);
    ignoreIonsScoreBelow.setLabel(resources.message(IGNORE_IONS_SCORE_BELOW));
    ignoreIonsScoreBelow
        .setPlaceholder(resources.message(property(IGNORE_IONS_SCORE_BELOW, PLACEHOLDER)));
    binder.forField(ignoreIonsScoreBelow).withNullRepresentation("")
        .withConverter(new StringToIntegerConverter(generalResources.message(INVALID_INTEGER)))
        .bind(IGNORE_IONS_SCORE_BELOW);
    useHomologyLabel.setText(resources.message(USE_HOMOLOGY));
    useHomology.setRenderer(new TextRenderer<>(
        value -> resources.message(property(USE_HOMOLOGY, String.valueOf(value)))));
    report.setLabel(resources.message(REPORT));
    report.setPlaceholder(resources.message(property(REPORT, PLACEHOLDER)));
    binder.forField(report).withNullRepresentation("")
        .withConverter(new StringToIntegerConverter(generalResources.message(INVALID_INTEGER)))
        .bind(REPORT);
    serverMudpitSwitchLabel.setText(resources.message(SERVER_MUDPIT_SWITCH));
    MessageResource serverMudpitSwitchResources =
        new MessageResource(ProteinScoring.class, getLocale());
    serverMudpitSwitch
        .setRenderer(new TextRenderer<>(ps -> serverMudpitSwitchResources.message(ps.name())));
    showSameSets.setLabel(resources.message(SHOW_SAME_SETS));
    showSameSets.getElement().setAttribute(TITLE,
        resources.message(property(SHOW_SAME_SETS, TITLE)));
    showSubsets.setLabel(resources.message(SHOW_SUBSETS));
    showSubsets.setPlaceholder(resources.message(property(SHOW_SUBSETS, PLACEHOLDER)));
    showSubsets.getElement().setAttribute(TITLE, resources.message(property(SHOW_SUBSETS, TITLE)));
    binder.forField(showSubsets).withNullRepresentation("")
        .withConverter(new StringToIntegerConverter(generalResources.message(INVALID_INTEGER)))
        .bind(SHOW_SUBSETS);
    groupFamily.setLabel(resources.message(GROUP_FAMILY));
    requireBoldRed.setLabel(resources.message(REQUIRE_BOLD_RED));
    preferTaxonomy.setText(resources.message(PREFER_TAXONOMY));
  }

  public boolean isReadOnly() {
    return readOnly;
  }

  public void setReadOnly(boolean readOnly) {
    this.readOnly = readOnly;
    binder.setReadOnly(readOnly);
  }

  public ExportParameters getBean() {
    return binder.getBean();
  }

  public void setBean(ExportParameters exportParameters) {
    binder.setBean(exportParameters);
  }

  public BinderValidationStatus<ExportParameters> validate() {
    return binder.validate();
  }

  public void writeBean(ExportParameters exportParameters) throws ValidationException {
    binder.writeBean(exportParameters);
  }
}
