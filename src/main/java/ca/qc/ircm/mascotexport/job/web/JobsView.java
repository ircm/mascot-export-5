/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.ExportJobProperties.DATE;
import static ca.qc.ircm.mascotexport.job.ExportJobProperties.DONE_DATE;
import static ca.qc.ircm.mascotexport.job.ExportJobProperties.FILE_PATH;
import static ca.qc.ircm.mascotexport.job.ExportJobProperties.INPUT;
import static ca.qc.ircm.mascotexport.web.WebConstants.APPLICATION_NAME;
import static ca.qc.ircm.mascotexport.web.WebConstants.CANCEL;
import static ca.qc.ircm.mascotexport.web.WebConstants.TITLE;

import ca.qc.ircm.mascotexport.job.ExportJob;
import ca.qc.ircm.mascotexport.web.WebConstants;
import ca.qc.ircm.mascotexport.web.component.BaseComponent;
import ca.qc.ircm.text.MessageResource;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.Column;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.i18n.LocaleChangeEvent;
import com.vaadin.flow.i18n.LocaleChangeObserver;
import com.vaadin.flow.router.HasDynamicTitle;
import com.vaadin.flow.router.Route;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Jobs view.
 */
@HtmlImport("styles/shared-styles.html")
@Route(JobsView.VIEW_NAME)
public class JobsView extends Composite<VerticalLayout>
    implements BaseComponent, LocaleChangeObserver, HasDynamicTitle {
  public static final String VIEW_NAME = "jobs";
  public static final String HEADER = "header";
  public static final String JOBS = "jobs";
  public static final String JOBS_EMPTY = "jobs.empty";
  public static final String VIEW = "view";
  public static final String REMOVE = "remove";
  public static final String PROGRESS = "progress";
  public static final String EXPORT = "export";
  public static final String EXPORT_DONE = EXPORT + ".done";
  public static final String ADD = "add";
  public static final String REMOVE_MANY = "removeMany";
  public static final String REMOVE_DONE = "removeDone";
  public static final String REFRESH = "refresh";
  private static final Logger logger = LoggerFactory.getLogger(JobsView.class);
  private static final long serialVersionUID = -4841506964348874685L;
  protected H1 header = new H1();
  protected Grid<ExportJob> jobs = new Grid<>();
  protected Column<ExportJob> fileColumn;
  protected Column<ExportJob> inputColumn;
  protected Column<ExportJob> dateColumn;
  protected Column<ExportJob> doneColumn;
  protected Column<ExportJob> viewColumn;
  protected Column<ExportJob> removeColumn;
  protected Column<ExportJob> progressColumn;
  protected Button export = new Button();
  protected Button cancel = new Button();
  protected Button add = new Button();
  protected Button removeMany = new Button();
  protected Button removeDone = new Button();
  protected Button refresh = new Button();
  @Inject
  private transient JobsViewPresenter presenter;

  /**
   * Create jobs view.
   */
  public JobsView() {
    logger.debug("Jobs view");
    VerticalLayout root = getContent();
    root.setId(VIEW_NAME);
    root.add(header);
    root.add(jobs);
    jobs.addClassName(JOBS);
    HorizontalLayout buttons = new HorizontalLayout();
    root.add(buttons);
    buttons.add(export);
    export.addClassName(EXPORT);
    export.setIcon(VaadinIcon.FILE_PROCESS.create());
    export.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    buttons.add(cancel);
    cancel.addClassName(CANCEL);
    cancel.setIcon(VaadinIcon.STOP.create());
    buttons.add(add);
    add.addClassName(ADD);
    add.setIcon(VaadinIcon.PLUS.create());
    add.addThemeVariants(ButtonVariant.LUMO_SUCCESS);
    buttons.add(removeMany);
    removeMany.addClassName(REMOVE_MANY);
    removeMany.setIcon(VaadinIcon.TRASH.create());
    removeMany.addThemeVariants(ButtonVariant.LUMO_ERROR);
    buttons.add(removeDone);
    removeDone.addClassName(REMOVE_DONE);
    removeDone.setIcon(VaadinIcon.TRASH.create());
    removeDone.addThemeVariants(ButtonVariant.LUMO_ERROR);
    buttons.add(refresh);
    refresh.addClassName(REFRESH);
    refresh.setIcon(VaadinIcon.REFRESH.create());
    refresh.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
  }

  public JobsView(JobsViewPresenter presenter) {
    this();
    this.presenter = presenter;
  }

  /**
   * Initializes jobs grid.
   */
  @PostConstruct
  protected void initJobs() {
    jobs.setSelectionMode(SelectionMode.MULTI);
    fileColumn = jobs.addColumn(job -> job.getFilePath(), FILE_PATH).setKey(FILE_PATH);
    inputColumn = jobs.addColumn(job -> job.getInput(), INPUT).setKey(INPUT);
    DateTimeFormatter dateFormatter = DateTimeFormatter.ISO_DATE;
    dateColumn =
        jobs.addColumn(job -> dateFormatter.format(job.getDate().toLocalDate()), DATE).setKey(DATE);
    doneColumn = jobs.addColumn(
        job -> job.getDoneDate() != null ? dateFormatter.format(job.getDoneDate().toLocalDate())
            : "",
        DONE_DATE).setKey(DONE_DATE);
    viewColumn = jobs.addComponentColumn(job -> viewButton(job)).setKey(VIEW);
    removeColumn = jobs.addComponentColumn(job -> removeButton(job)).setKey(REMOVE);
    progressColumn = jobs.addColumn(job -> presenter.progress(job)).setKey(PROGRESS);
  }

  @Override
  protected void onAttach(AttachEvent event) {
    super.onAttach(event);
    presenter.init(this);
  }

  @Override
  public void localeChange(LocaleChangeEvent event) {
    final MessageResource resources = new MessageResource(JobsView.class, getLocale());
    final MessageResource jobResources = new MessageResource(ExportJob.class, getLocale());
    final MessageResource generalResources = new MessageResource(WebConstants.class, getLocale());
    String fileHeader = jobResources.message(FILE_PATH);
    fileColumn.setHeader(fileHeader).setFooter(fileHeader);
    String inputHeader = jobResources.message(INPUT);
    inputColumn.setHeader(inputHeader).setFooter(inputHeader);
    String dateHeader = jobResources.message(DATE);
    dateColumn.setHeader(dateHeader).setFooter(dateHeader);
    String completedHeader = jobResources.message(DONE_DATE);
    doneColumn.setHeader(completedHeader).setFooter(completedHeader);
    String viewHeader = resources.message(VIEW);
    viewColumn.setHeader(viewHeader).setFooter(viewHeader);
    String removeHeader = resources.message(REMOVE);
    removeColumn.setHeader(removeHeader).setFooter(removeHeader);
    String progressHeader = resources.message(PROGRESS);
    progressColumn.setHeader(progressHeader).setFooter(progressHeader);
    header.setText(resources.message(HEADER));
    export.setText(resources.message(EXPORT));
    cancel.setText(generalResources.message(CANCEL));
    add.setText(resources.message(ADD));
    removeMany.setText(resources.message(REMOVE_MANY));
    removeDone.setText(resources.message(REMOVE_DONE));
    refresh.setText(resources.message(REFRESH));
  }

  private Button viewButton(ExportJob job) {
    Button button = new Button();
    button.addClassName(VIEW);
    button.setIcon(VaadinIcon.MODAL.create());
    button.addClickListener(presenter.viewButtonListener(job));
    return button;
  }

  private Button removeButton(ExportJob job) {
    Button button = new Button();
    button.addClassName(REMOVE);
    button.setIcon(VaadinIcon.TRASH.create());
    button.addClickListener(presenter.removeButtonListener(job));
    return button;
  }

  @Override
  public String getPageTitle() {
    final MessageResource resources = new MessageResource(JobsView.class, getLocale());
    final MessageResource generalResources = new MessageResource(WebConstants.class, getLocale());
    return resources.message(TITLE, generalResources.message(APPLICATION_NAME));
  }

  @Override
  public Locale getLocale() {
    return super.getLocale();
  }
}
