/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job;

import ca.qc.ircm.mascotexport.ProteinScoring;
import ca.qc.ircm.processing.GeneratePropertyNames;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Export job parameters.
 */
@GeneratePropertyNames
public class ExportParameters implements Serializable {
  private static final long serialVersionUID = -6380343531511654652L;
  private ExportFormat exportFormat;
  private Double sigThreshold;
  private Integer ignoreIonsScoreBelow;
  private boolean useHomology;
  private Integer report;
  private ProteinScoring serverMudpitSwitch;
  private boolean showSameSets;
  private Integer showSubsets;
  private boolean groupFamily;
  private boolean requireBoldRed;
  private Integer preferTaxonomy;

  /**
   * Returns command line parameters for Mascot.
   *
   * @return command line parameters for Mascot
   */
  public Map<String, List<String>> commandLineParameters() {
    Map<String, List<String>> parameters = new HashMap<>();
    addParameterIfNotNull(parameters, exportFormat, "export_format");
    addParameterIfNotNull(parameters, sigThreshold, "_sigthreshold");
    addParameterIfNotNull(parameters, ignoreIonsScoreBelow, "_ignoreionsscorebelow");
    addParameterIfTrue(parameters, useHomology, "use_homology");
    addParameterIfNotNull(parameters, report, "report");
    addParameterIfNotNull(parameters, serverMudpitSwitch != null ? serverMudpitSwitch.value : null,
        "_server_mudpit_switch");
    addParameterIfTrue(parameters, showSameSets, "show_same_sets");
    addParameterIfNotNull(parameters, showSubsets, "_showsubsets");
    addParameterIfTrue(parameters, groupFamily, "group_family");
    if (!groupFamily) {
      addParameterIfTrue(parameters, requireBoldRed, "_requireboldred");
    }
    addParameterIfNotNull(parameters, preferTaxonomy, "_prefertaxonomy");
    return parameters;
  }

  private void addParameterIfTrue(Map<String, List<String>> parameters, boolean value,
      String name) {
    if (value) {
      parameters.put(name, parameter(value));
    }
  }

  private void addParameterIfNotNull(Map<String, List<String>> parameters, Object value,
      String name) {
    if (value != null) {
      parameters.put(name, parameter(value));
    }
  }

  private List<String> parameter(boolean value) {
    return Arrays.asList(value ? "1" : "0");
  }

  private List<String> parameter(Object value) {
    return Arrays.asList(String.valueOf(value));
  }

  public ExportFormat getExportFormat() {
    return exportFormat;
  }

  public void setExportFormat(ExportFormat exportFormat) {
    this.exportFormat = exportFormat;
  }

  public Double getSigThreshold() {
    return sigThreshold;
  }

  public void setSigThreshold(Double sigThreshold) {
    this.sigThreshold = sigThreshold;
  }

  public Integer getIgnoreIonsScoreBelow() {
    return ignoreIonsScoreBelow;
  }

  public void setIgnoreIonsScoreBelow(Integer ignoreIonsScoreBelow) {
    this.ignoreIonsScoreBelow = ignoreIonsScoreBelow;
  }

  public boolean isUseHomology() {
    return useHomology;
  }

  public void setUseHomology(boolean useHomology) {
    this.useHomology = useHomology;
  }

  public Integer getReport() {
    return report;
  }

  public void setReport(Integer report) {
    this.report = report;
  }

  public ProteinScoring getServerMudpitSwitch() {
    return serverMudpitSwitch;
  }

  public void setServerMudpitSwitch(ProteinScoring serverMudpitSwitch) {
    this.serverMudpitSwitch = serverMudpitSwitch;
  }

  public boolean isShowSameSets() {
    return showSameSets;
  }

  public void setShowSameSets(boolean showSameSets) {
    this.showSameSets = showSameSets;
  }

  public Integer getShowSubsets() {
    return showSubsets;
  }

  public void setShowSubsets(Integer showSubsets) {
    this.showSubsets = showSubsets;
  }

  public boolean isGroupFamily() {
    return groupFamily;
  }

  public void setGroupFamily(boolean groupFamily) {
    this.groupFamily = groupFamily;
  }

  public boolean isRequireBoldRed() {
    return requireBoldRed;
  }

  public void setRequireBoldRed(boolean requireBoldRed) {
    this.requireBoldRed = requireBoldRed;
  }

  public Integer getPreferTaxonomy() {
    return preferTaxonomy;
  }

  public void setPreferTaxonomy(Integer preferTaxonomy) {
    this.preferTaxonomy = preferTaxonomy;
  }
}
