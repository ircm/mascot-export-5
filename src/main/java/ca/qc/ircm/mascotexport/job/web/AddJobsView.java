/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.ExportJobProperties.INPUT;
import static ca.qc.ircm.mascotexport.web.WebConstants.ALL;
import static ca.qc.ircm.mascotexport.web.WebConstants.APPLICATION_NAME;
import static ca.qc.ircm.mascotexport.web.WebConstants.CANCEL;
import static ca.qc.ircm.mascotexport.web.WebConstants.SAVE;
import static ca.qc.ircm.mascotexport.web.WebConstants.TITLE;

import ca.qc.ircm.mascotexport.job.ExportJob;
import ca.qc.ircm.mascotexport.mascot.MascotFile;
import ca.qc.ircm.mascotexport.web.WebConstants;
import ca.qc.ircm.mascotexport.web.component.BaseComponent;
import ca.qc.ircm.text.MessageResource;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.Column;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.i18n.LocaleChangeEvent;
import com.vaadin.flow.i18n.LocaleChangeObserver;
import com.vaadin.flow.router.HasDynamicTitle;
import com.vaadin.flow.router.Route;
import java.util.Locale;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Adds job view.
 */
@HtmlImport("styles/shared-styles.html")
@Route(AddJobsView.VIEW_NAME)
public class AddJobsView extends Composite<VerticalLayout>
    implements LocaleChangeObserver, HasDynamicTitle, BaseComponent {
  public static final String VIEW_NAME = "add-jobs";
  public static final String HEADER = "header";
  public static final String FILES = "files";
  public static final String NAME = "name";
  public static final String PARENT = "parent";
  public static final String LOAD_IO_EXCEPTION = "load.IOException";
  public static final String FILES_EMPTY = "files.empty";
  private static final long serialVersionUID = -7631894147348647172L;
  private static final Logger logger = LoggerFactory.getLogger(AddJobsView.class);
  protected H2 header = new H2();
  protected Grid<MascotFile> files = new Grid<>();
  protected Div filesError = new Div();
  protected Column<MascotFile> nameColumn;
  protected Column<MascotFile> parentColumn;
  protected Column<MascotFile> inputColumn;
  protected TextField nameFilter = new TextField();
  protected TextField parentFilter = new TextField();
  protected TextField inputFilter = new TextField();
  protected ExportParametersForm exportParametersForm = new ExportParametersForm();
  protected SearchParametersForm searchParametersForm = new SearchParametersForm();
  protected ProteinParametersForm proteinParametersForm = new ProteinParametersForm();
  protected PeptideParametersForm peptideParametersForm = new PeptideParametersForm();
  protected QueryParametersForm queryParametersForm = new QueryParametersForm();
  protected Button save = new Button();
  protected Button cancel = new Button();
  @Inject
  private transient AddJobsViewPresenter presenter;

  /**
   * Initializes add jobs view.
   */
  protected AddJobsView() {
    logger.debug("Add jobs view");
    VerticalLayout root = getContent();
    root.setId(VIEW_NAME);
    root.add(header);
    header.addClassName(HEADER);
    VerticalLayout jobParameters = new VerticalLayout();
    root.add(jobParameters);
    jobParameters.setPadding(false);
    jobParameters.setSpacing(false);
    jobParameters.add(files);
    files.addClassName(FILES);
    jobParameters.add(filesError);
    filesError.addClassName("error-text");
    filesError.addClassName("font-size-xs");
    VerticalLayout otherParameters = new VerticalLayout();
    root.add(otherParameters);
    otherParameters.setPadding(false);
    otherParameters.add(exportParametersForm);
    otherParameters.add(searchParametersForm);
    otherParameters.add(proteinParametersForm);
    otherParameters.add(peptideParametersForm);
    otherParameters.add(queryParametersForm);
    HorizontalLayout buttonLayout = new HorizontalLayout();
    root.add(buttonLayout);
    buttonLayout.add(save);
    save.addClassName(SAVE);
    save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    save.setIcon(VaadinIcon.CHECK.create());
    buttonLayout.add(cancel);
    cancel.addClassName(CANCEL);
    cancel.setIcon(VaadinIcon.CLOSE.create());
  }

  protected AddJobsView(AddJobsViewPresenter presenter) {
    this();
    this.presenter = presenter;
  }

  /**
   * Initializes files grid.
   */
  @PostConstruct
  protected void initFiles() {
    files.setSelectionMode(SelectionMode.MULTI);
    nameColumn = files.addColumn(mf -> mf.file.getFileName().toString(), NAME).setKey(NAME);
    parentColumn = files.addColumn(
        mf -> mf.file.getParent() != null ? mf.file.getParent().getFileName().toString() : "",
        PARENT).setKey(PARENT);
    inputColumn = files.addColumn(mf -> mf.inputFile, INPUT).setKey(INPUT);
    files.appendHeaderRow(); // Header / sort row
    HeaderRow filterRow = files.appendHeaderRow();
    filterRow.getCell(nameColumn).setComponent(nameFilter);
    nameFilter.setSizeFull();
    filterRow.getCell(parentColumn).setComponent(parentFilter);
    parentFilter.setSizeFull();
    filterRow.getCell(inputColumn).setComponent(inputFilter);
    inputFilter.setSizeFull();
  }

  @Override
  protected void onAttach(AttachEvent attachEvent) {
    super.onAttach(attachEvent);
    presenter.init(this);
  }

  @Override
  public void localeChange(LocaleChangeEvent event) {
    final MessageResource resources = new MessageResource(AddJobsView.class, getLocale());
    final MessageResource jobResources = new MessageResource(ExportJob.class, getLocale());
    final MessageResource generalResources = new MessageResource(WebConstants.class, getLocale());
    header.setText(resources.message(HEADER));
    String nameHeader = resources.message(NAME);
    nameColumn.setHeader(nameHeader);
    String parentHeader = resources.message(PARENT);
    parentColumn.setHeader(parentHeader);
    String inputHeader = jobResources.message(INPUT);
    inputColumn.setHeader(inputHeader);
    nameFilter.setPlaceholder(generalResources.message(ALL));
    parentFilter.setPlaceholder(generalResources.message(ALL));
    inputFilter.setPlaceholder(generalResources.message(ALL));
    save.setText(generalResources.message(SAVE));
    cancel.setText(generalResources.message(CANCEL));
  }

  @Override
  public String getPageTitle() {
    final MessageResource resources = new MessageResource(AddJobsView.class, getLocale());
    final MessageResource generalResources = new MessageResource(WebConstants.class, getLocale());
    return resources.message(TITLE, generalResources.message(APPLICATION_NAME));
  }

  @Override
  protected Locale getLocale() {
    return super.getLocale();
  }
}
