/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.CALC_MR;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.DELTA;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.END;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.EXPECT;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.EXP_MR;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.EXP_Z;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.FRAME;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.HOMOL;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.IDENT;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.MASTER;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.MISS;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.NUM_MATCH;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.SCAN_TITLE;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.SCORE;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.SEQ;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.SHOW_PEP_DUPES;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.SHOW_UNASSIGNED;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.START;
import static ca.qc.ircm.mascotexport.job.PeptideParametersProperties.VAR_MOD;

import ca.qc.ircm.mascotexport.job.PeptideParameters;
import ca.qc.ircm.text.MessageResource;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.BinderValidationStatus;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.i18n.LocaleChangeEvent;
import com.vaadin.flow.i18n.LocaleChangeObserver;

/**
 * Peptide parameters form.
 */
public class PeptideParametersForm extends Composite<VerticalLayout>
    implements LocaleChangeObserver {
  public static final String STYLE = "peptide-parameters";
  private static final long serialVersionUID = 3566981214013562662L;
  protected Checkbox master = new Checkbox();
  protected Checkbox expMr = new Checkbox();
  protected Checkbox expZ = new Checkbox();
  protected Checkbox calcMr = new Checkbox();
  protected Checkbox delta = new Checkbox();
  protected Checkbox start = new Checkbox();
  protected Checkbox end = new Checkbox();
  protected Checkbox miss = new Checkbox();
  protected Checkbox score = new Checkbox();
  protected Checkbox homol = new Checkbox();
  protected Checkbox ident = new Checkbox();
  protected Checkbox expect = new Checkbox();
  protected Checkbox seq = new Checkbox();
  protected Checkbox frame = new Checkbox();
  protected Checkbox varMod = new Checkbox();
  protected Checkbox numMatch = new Checkbox();
  protected Checkbox scanTitle = new Checkbox();
  protected Checkbox showUnassigned = new Checkbox();
  protected Checkbox showPepDupes = new Checkbox();
  private Binder<PeptideParameters> binder = new Binder<>(PeptideParameters.class);
  private boolean readOnly;

  /**
   * Creates peptide parameter form.
   */
  public PeptideParametersForm() {
    VerticalLayout root = getContent();
    root.setPadding(false);
    root.setSpacing(false);
    root.addClassName(STYLE);
    root.add(master);
    master.addClassName(MASTER);
    master.addClassName("large");
    binder.bind(master, MASTER);
    VerticalLayout subElements = new VerticalLayout();
    root.add(subElements);
    subElements.setSpacing(false);
    subElements.add(expMr);
    expMr.addClassName(EXP_MR);
    binder.bind(expMr, EXP_MR);
    subElements.add(expZ);
    expZ.addClassName(EXP_Z);
    binder.bind(expZ, EXP_Z);
    subElements.add(calcMr);
    calcMr.addClassName(CALC_MR);
    binder.bind(calcMr, CALC_MR);
    subElements.add(delta);
    delta.addClassName(DELTA);
    binder.bind(delta, DELTA);
    subElements.add(start);
    start.addClassName(START);
    binder.bind(start, START);
    subElements.add(end);
    end.addClassName(END);
    binder.bind(end, END);
    subElements.add(miss);
    miss.addClassName(MISS);
    binder.bind(miss, MISS);
    subElements.add(score);
    score.addClassName(SCORE);
    binder.bind(score, SCORE);
    subElements.add(homol);
    homol.addClassName(HOMOL);
    binder.bind(homol, HOMOL);
    subElements.add(ident);
    ident.addClassName(IDENT);
    binder.bind(ident, IDENT);
    subElements.add(expect);
    expect.addClassName(EXPECT);
    binder.bind(expect, EXPECT);
    subElements.add(seq);
    seq.addClassName(SEQ);
    binder.bind(seq, SEQ);
    subElements.add(frame);
    frame.addClassName(FRAME);
    binder.bind(frame, FRAME);
    subElements.add(varMod);
    varMod.addClassName(VAR_MOD);
    binder.bind(varMod, VAR_MOD);
    subElements.add(numMatch);
    numMatch.addClassName(NUM_MATCH);
    binder.bind(numMatch, NUM_MATCH);
    subElements.add(scanTitle);
    scanTitle.addClassName(SCAN_TITLE);
    binder.bind(scanTitle, SCAN_TITLE);
    subElements.add(showUnassigned);
    showUnassigned.addClassName(SHOW_UNASSIGNED);
    binder.bind(showUnassigned, SHOW_UNASSIGNED);
    subElements.add(showPepDupes);
    showPepDupes.addClassName(SHOW_PEP_DUPES);
    binder.bind(showPepDupes, SHOW_PEP_DUPES);
  }

  @Override
  public void localeChange(LocaleChangeEvent event) {
    final MessageResource resources = new MessageResource(PeptideParameters.class, getLocale());
    master.setLabel(resources.message(MASTER));
    expMr.setLabel(resources.message(EXP_MR));
    expZ.setLabel(resources.message(EXP_Z));
    calcMr.setLabel(resources.message(CALC_MR));
    delta.setLabel(resources.message(DELTA));
    start.setLabel(resources.message(START));
    end.setLabel(resources.message(END));
    miss.setLabel(resources.message(MISS));
    score.setLabel(resources.message(SCORE));
    homol.setLabel(resources.message(HOMOL));
    ident.setLabel(resources.message(IDENT));
    expect.setLabel(resources.message(EXPECT));
    seq.setLabel(resources.message(SEQ));
    frame.setLabel(resources.message(FRAME));
    varMod.setLabel(resources.message(VAR_MOD));
    numMatch.setLabel(resources.message(NUM_MATCH));
    scanTitle.setLabel(resources.message(SCAN_TITLE));
    showUnassigned.setLabel(resources.message(SHOW_UNASSIGNED));
    showPepDupes.setLabel(resources.message(SHOW_PEP_DUPES));
  }

  public boolean isReadOnly() {
    return readOnly;
  }

  public void setReadOnly(boolean readOnly) {
    this.readOnly = readOnly;
    binder.setReadOnly(readOnly);
  }

  public PeptideParameters getBean() {
    return binder.getBean();
  }

  public void setBean(PeptideParameters peptideParameters) {
    binder.setBean(peptideParameters);
  }

  public BinderValidationStatus<PeptideParameters> validate() {
    return binder.validate();
  }

  public void writeBean(PeptideParameters peptideParameters) throws ValidationException {
    binder.writeBean(peptideParameters);
  }
}
