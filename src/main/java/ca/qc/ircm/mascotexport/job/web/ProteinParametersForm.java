/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.ProteinParameters.SLOW;
import static ca.qc.ircm.mascotexport.job.ProteinParameters.VERY_SLOW;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.COVER;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.DESC;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.EMPAI;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.LEN;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.MASS;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.MASTER;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.MATCHES;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.PI;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.SCORE;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.SEQ;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.TAX_ID;
import static ca.qc.ircm.mascotexport.job.ProteinParametersProperties.TAX_STR;

import ca.qc.ircm.mascotexport.job.ProteinParameters;
import ca.qc.ircm.text.MessageResource;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.BinderValidationStatus;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.i18n.LocaleChangeEvent;
import com.vaadin.flow.i18n.LocaleChangeObserver;

/**
 * Protein parameters form.
 */
public class ProteinParametersForm extends Composite<VerticalLayout>
    implements LocaleChangeObserver {
  public static final String STYLE = "protein-parameters";
  private static final long serialVersionUID = 2827926099103517263L;
  protected Checkbox master = new Checkbox();
  protected Checkbox score = new Checkbox();
  protected Checkbox desc = new Checkbox();
  protected Checkbox mass = new Checkbox();
  protected Checkbox matches = new Checkbox();
  protected Checkbox cover = new Checkbox();
  protected Checkbox len = new Checkbox();
  protected Checkbox pi = new Checkbox();
  protected Checkbox taxStr = new Checkbox();
  protected Checkbox taxId = new Checkbox();
  protected Checkbox seq = new Checkbox();
  protected Checkbox empai = new Checkbox();
  protected Div slow = new Div();
  protected Div verySlow = new Div();
  private Binder<ProteinParameters> binder = new Binder<>(ProteinParameters.class);
  private boolean readOnly;

  /**
   * Creates protein parameter form.
   */
  public ProteinParametersForm() {
    VerticalLayout root = getContent();
    root.setPadding(false);
    root.setSpacing(false);
    root.addClassName(STYLE);
    root.add(master);
    master.addClassName(MASTER);
    master.addClassName("large");
    binder.bind(master, MASTER);
    VerticalLayout subElements = new VerticalLayout();
    root.add(subElements);
    subElements.setSpacing(false);
    subElements.add(score);
    score.addClassName(SCORE);
    binder.bind(score, SCORE);
    subElements.add(desc);
    desc.addClassName(DESC);
    binder.bind(desc, DESC);
    subElements.add(mass);
    mass.addClassName(MASS);
    binder.bind(mass, MASS);
    subElements.add(matches);
    matches.addClassName(MATCHES);
    binder.bind(matches, MATCHES);
    subElements.add(cover);
    cover.addClassName(COVER);
    binder.bind(cover, COVER);
    subElements.add(len);
    len.addClassName(LEN);
    binder.bind(len, LEN);
    subElements.add(pi);
    pi.addClassName(PI);
    binder.bind(pi, PI);
    subElements.add(taxStr);
    taxStr.addClassName(TAX_STR);
    binder.bind(taxStr, TAX_STR);
    subElements.add(taxId);
    taxId.addClassName(TAX_ID);
    binder.bind(taxId, TAX_ID);
    subElements.add(seq);
    seq.addClassName(SEQ);
    binder.bind(seq, SEQ);
    subElements.add(empai);
    empai.addClassName(EMPAI);
    binder.bind(empai, EMPAI);
    subElements.add(slow);
    slow.addClassName(SLOW);
    subElements.add(verySlow);
    verySlow.addClassName(VERY_SLOW);
  }

  @Override
  public void localeChange(LocaleChangeEvent event) {
    final MessageResource resources = new MessageResource(ProteinParameters.class, getLocale());
    master.setLabel(resources.message(MASTER));
    score.setLabel(resources.message(SCORE));
    desc.setLabel(resources.message(DESC));
    mass.setLabel(resources.message(MASS));
    matches.setLabel(resources.message(MATCHES));
    cover.setLabel(resources.message(COVER));
    len.setLabel(resources.message(LEN));
    pi.setLabel(resources.message(PI));
    taxStr.setLabel(resources.message(TAX_STR));
    taxId.setLabel(resources.message(TAX_ID));
    seq.setLabel(resources.message(SEQ));
    empai.setLabel(resources.message(EMPAI));
    slow.setText(resources.message(SLOW));
    verySlow.setText(resources.message(VERY_SLOW));
  }

  public boolean isReadOnly() {
    return readOnly;
  }

  public void setReadOnly(boolean readOnly) {
    this.readOnly = readOnly;
    binder.setReadOnly(readOnly);
  }

  public ProteinParameters getBean() {
    return binder.getBean();
  }

  public void setBean(ProteinParameters proteinParameters) {
    binder.setBean(proteinParameters);
  }

  public BinderValidationStatus<ProteinParameters> validate() {
    return binder.validate();
  }

  public void writeBean(ProteinParameters proteinParameters) throws ValidationException {
    binder.writeBean(proteinParameters);
  }
}
