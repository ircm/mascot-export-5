/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job;

import ca.qc.ircm.processing.GeneratePropertyNames;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Export job parameters.
 */
@GeneratePropertyNames
public class ProteinParameters implements Serializable {
  private static final long serialVersionUID = 7756549908006585281L;
  public static final String SLOW = "slow";
  public static final String VERY_SLOW = "verySlow";
  private boolean master;
  private boolean score;
  private boolean desc;
  private boolean mass;
  private boolean matches;
  private boolean cover;
  private boolean len;
  private boolean pi;
  private boolean taxStr;
  private boolean taxId;
  private boolean seq;
  private boolean empai;

  /**
   * Returns command line parameters for Mascot.
   *
   * @return command line parameters for Mascot
   */
  public Map<String, List<String>> commandLineParameters() {
    Map<String, List<String>> parameters = new HashMap<>();
    addParameterIfTrue(parameters, master, "protein_master");
    addParameterIfTrue(parameters, score, "prot_score");
    addParameterIfTrue(parameters, desc, "prot_desc");
    addParameterIfTrue(parameters, mass, "prot_mass");
    addParameterIfTrue(parameters, matches, "prot_matches");
    addParameterIfTrue(parameters, cover, "prot_cover");
    addParameterIfTrue(parameters, len, "prot_len");
    addParameterIfTrue(parameters, pi, "prot_pi");
    addParameterIfTrue(parameters, taxStr, "prot_tax_str");
    addParameterIfTrue(parameters, taxId, "prot_tax_id");
    addParameterIfTrue(parameters, seq, "prot_seq");
    addParameterIfTrue(parameters, empai, "prot_empai");
    return parameters;
  }

  private void addParameterIfTrue(Map<String, List<String>> parameters, boolean value,
      String name) {
    if (master && value) {
      parameters.put(name, parameter(value));
    }
  }

  private List<String> parameter(boolean value) {
    return Arrays.asList(value ? "1" : "0");
  }

  public boolean isMaster() {
    return master;
  }

  public void setMaster(boolean master) {
    this.master = master;
  }

  public boolean isScore() {
    return score;
  }

  public void setScore(boolean score) {
    this.score = score;
  }

  public boolean isDesc() {
    return desc;
  }

  public void setDesc(boolean desc) {
    this.desc = desc;
  }

  public boolean isMass() {
    return mass;
  }

  public void setMass(boolean mass) {
    this.mass = mass;
  }

  public boolean isMatches() {
    return matches;
  }

  public void setMatches(boolean matches) {
    this.matches = matches;
  }

  public boolean isCover() {
    return cover;
  }

  public void setCover(boolean cover) {
    this.cover = cover;
  }

  public boolean isLen() {
    return len;
  }

  public void setLen(boolean len) {
    this.len = len;
  }

  public boolean isPi() {
    return pi;
  }

  public void setPi(boolean pi) {
    this.pi = pi;
  }

  public boolean isTaxStr() {
    return taxStr;
  }

  public void setTaxStr(boolean taxStr) {
    this.taxStr = taxStr;
  }

  public boolean isTaxId() {
    return taxId;
  }

  public void setTaxId(boolean taxId) {
    this.taxId = taxId;
  }

  public boolean isSeq() {
    return seq;
  }

  public void setSeq(boolean seq) {
    this.seq = seq;
  }

  public boolean isEmpai() {
    return empai;
  }

  public void setEmpai(boolean empai) {
    this.empai = empai;
  }
}
