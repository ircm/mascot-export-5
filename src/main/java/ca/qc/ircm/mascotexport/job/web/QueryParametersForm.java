/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.QueryParametersProperties.MASTER;
import static ca.qc.ircm.mascotexport.job.QueryParametersProperties.PARAMS;
import static ca.qc.ircm.mascotexport.job.QueryParametersProperties.PEAKS;
import static ca.qc.ircm.mascotexport.job.QueryParametersProperties.QUALIFIERS;
import static ca.qc.ircm.mascotexport.job.QueryParametersProperties.RAW;
import static ca.qc.ircm.mascotexport.job.QueryParametersProperties.TITLE;

import ca.qc.ircm.mascotexport.job.QueryParameters;
import ca.qc.ircm.text.MessageResource;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.BinderValidationStatus;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.i18n.LocaleChangeEvent;
import com.vaadin.flow.i18n.LocaleChangeObserver;

/**
 * Query parameters form.
 */
public class QueryParametersForm extends Composite<VerticalLayout> implements LocaleChangeObserver {
  public static final String STYLE = "query-parameters";
  private static final long serialVersionUID = -9188258712936232135L;
  protected Checkbox master = new Checkbox();
  protected Checkbox title = new Checkbox();
  protected Checkbox qualifiers = new Checkbox();
  protected Checkbox params = new Checkbox();
  protected Checkbox peaks = new Checkbox();
  protected Checkbox raw = new Checkbox();
  private Binder<QueryParameters> binder = new Binder<>(QueryParameters.class);
  private boolean readOnly;

  /**
   * Creates query parameter form.
   */
  public QueryParametersForm() {
    VerticalLayout root = getContent();
    root.setPadding(false);
    root.setSpacing(false);
    root.addClassName(STYLE);
    root.add(master);
    master.addClassName(MASTER);
    master.addClassName("large");
    binder.bind(master, MASTER);
    VerticalLayout subElements = new VerticalLayout();
    root.add(subElements);
    subElements.setSpacing(false);
    subElements.add(title);
    title.addClassName(TITLE);
    binder.bind(title, TITLE);
    subElements.add(qualifiers);
    qualifiers.addClassName(QUALIFIERS);
    binder.bind(qualifiers, QUALIFIERS);
    subElements.add(params);
    params.addClassName(PARAMS);
    binder.bind(params, PARAMS);
    subElements.add(peaks);
    peaks.addClassName(PEAKS);
    binder.bind(peaks, PEAKS);
    subElements.add(raw);
    raw.addClassName(RAW);
    binder.bind(raw, RAW);
  }

  @Override
  public void localeChange(LocaleChangeEvent event) {
    final MessageResource resources = new MessageResource(QueryParameters.class, getLocale());
    master.setLabel(resources.message(MASTER));
    title.setLabel(resources.message(TITLE));
    qualifiers.setLabel(resources.message(QUALIFIERS));
    params.setLabel(resources.message(PARAMS));
    peaks.setLabel(resources.message(PEAKS));
    raw.setLabel(resources.message(RAW));
  }

  public boolean isReadOnly() {
    return readOnly;
  }

  public void setReadOnly(boolean readOnly) {
    this.readOnly = readOnly;
    binder.setReadOnly(readOnly);
  }

  public QueryParameters getBean() {
    return binder.getBean();
  }

  public void setBean(QueryParameters queryParameters) {
    binder.setBean(queryParameters);
  }

  public BinderValidationStatus<QueryParameters> validate() {
    return binder.validate();
  }

  public void writeBean(QueryParameters queryParameters) throws ValidationException {
    binder.writeBean(queryParameters);
  }
}
