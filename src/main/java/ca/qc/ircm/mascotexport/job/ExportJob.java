/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job;

import ca.qc.ircm.mascotexport.Data;
import ca.qc.ircm.processing.GeneratePropertyNames;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Job to export a Mascot result file into another format.
 */
@Document(collection = "exportjob")
@GeneratePropertyNames
public class ExportJob implements Data, Serializable {
  private static final long serialVersionUID = 388736954442076342L;
  /**
   * Database identifier.
   */
  @Id
  private String id;
  /**
   * Input file.
   */
  private String input;
  /**
   * Path of file to convert. This is a relative path based on Mascot files location.
   */
  private String filePath;
  /**
   * Job's creation date.
   */
  private LocalDateTime date;
  /**
   * Job's completion date.
   */
  private LocalDateTime doneDate;
  private ExportParameters exportParameters;
  private SearchParameters searchParameters;
  private ProteinParameters proteinParameters;
  private PeptideParameters peptideParameters;
  private QueryParameters queryParameters;

  /**
   * Returns command line parameters for Mascot.
   *
   * @param minimumPeptideLenghtSummary
   *          minimum peptide lenght
   * @return command line parameters for Mascot
   */
  public Map<String, List<String>> commandLineParameters(Integer minimumPeptideLenghtSummary) {
    Map<String, List<String>> parameters = new HashMap<>();
    parameters.putAll(exportParameters.commandLineParameters());
    parameters.putAll(searchParameters.commandLineParameters());
    parameters.putAll(proteinParameters.commandLineParameters());
    parameters.putAll(peptideParameters.commandLineParameters(proteinParameters.isMaster()));
    parameters.putAll(queryParameters.commandLineParameters());
    parameters.put("_minpeplen", parameter(minimumPeptideLenghtSummary));
    parameters.put("do_export", parameter("1"));
    parameters.put("pep_exp_mz", parameter("1"));
    parameters.put("pep_isbold", parameter("1"));
    parameters.put("pep_isunique", parameter("1"));
    parameters.put("pep_query", parameter("1"));
    parameters.put("pep_rank", parameter("1"));
    parameters.put("prot_acc", parameter("1"));
    parameters.put("prot_hit_num", parameter("1"));
    parameters.put("sessionid", parameter("all_secdisabledsession"));
    return parameters;
  }

  private List<String> parameter(Object value) {
    return Arrays.asList(String.valueOf(value));
  }

  @Override
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getFilePath() {
    return filePath;
  }

  public void setFilePath(String filePath) {
    this.filePath = filePath;
  }

  public LocalDateTime getDate() {
    return date;
  }

  public void setDate(LocalDateTime date) {
    this.date = date;
  }

  public LocalDateTime getDoneDate() {
    return doneDate;
  }

  public void setDoneDate(LocalDateTime doneDate) {
    this.doneDate = doneDate;
  }

  public ExportParameters getExportParameters() {
    return exportParameters;
  }

  public void setExportParameters(ExportParameters exportParameters) {
    this.exportParameters = exportParameters;
  }

  public SearchParameters getSearchParameters() {
    return searchParameters;
  }

  public void setSearchParameters(SearchParameters searchParameters) {
    this.searchParameters = searchParameters;
  }

  public ProteinParameters getProteinParameters() {
    return proteinParameters;
  }

  public void setProteinParameters(ProteinParameters proteinParameters) {
    this.proteinParameters = proteinParameters;
  }

  public PeptideParameters getPeptideParameters() {
    return peptideParameters;
  }

  public void setPeptideParameters(PeptideParameters peptideParameters) {
    this.peptideParameters = peptideParameters;
  }

  public QueryParameters getQueryParameters() {
    return queryParameters;
  }

  public void setQueryParameters(QueryParameters queryParameters) {
    this.queryParameters = queryParameters;
  }

  public String getInput() {
    return input;
  }

  public void setInput(String input) {
    this.input = input;
  }
}
