/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.ExportJobProperties.DATE;
import static ca.qc.ircm.mascotexport.job.ExportJobProperties.DONE_DATE;
import static ca.qc.ircm.mascotexport.job.ExportJobProperties.FILE_PATH;
import static ca.qc.ircm.mascotexport.job.ExportJobProperties.INPUT;

import ca.qc.ircm.mascotexport.job.ExportJob;
import ca.qc.ircm.text.MessageResource;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.i18n.LocaleChangeEvent;
import com.vaadin.flow.i18n.LocaleChangeObserver;
import java.time.format.DateTimeFormatter;

/**
 * Job parameters form.
 */
public class JobParametersDialog extends Dialog implements LocaleChangeObserver {
  public static final String STYLE = "job-parameters-dialog";
  private static final long serialVersionUID = 1461566308004887327L;
  protected VerticalLayout layout;
  protected Div fileLabel = new Div();
  protected Div file = new Div();
  protected Div inputLabel = new Div();
  protected Div input = new Div();
  protected Div dateLabel = new Div();
  protected Div date = new Div();
  protected Div doneDateLabel = new Div();
  protected Div doneDate = new Div();
  protected ExportParametersForm exportParametersForm = new ExportParametersForm();
  protected SearchParametersForm searchParametersForm = new SearchParametersForm();
  protected ProteinParametersForm proteinParametersForm = new ProteinParametersForm();
  protected PeptideParametersForm peptideParametersForm = new PeptideParametersForm();
  protected QueryParametersForm queryParametersForm = new QueryParametersForm();
  private ExportJob job;
  private boolean readOnly;

  /**
   * Initializes job parameters form.
   */
  public JobParametersDialog() {
    layout = new VerticalLayout();
    add(layout);
    layout.addClassName(STYLE);
    layout.setPadding(false);
    layout.setSpacing(false);
    VerticalLayout jobParameters = new VerticalLayout();
    layout.add(jobParameters);
    jobParameters.setPadding(false);
    jobParameters.add(parameterLayout(fileLabel, file));
    fileLabel.addClassName("font-size-s");
    file.addClassName(FILE_PATH);
    jobParameters.add(parameterLayout(inputLabel, input));
    inputLabel.addClassName("font-size-s");
    input.addClassName(INPUT);
    jobParameters.add(parameterLayout(dateLabel, date));
    dateLabel.addClassName("font-size-s");
    date.addClassName(DATE);
    jobParameters.add(parameterLayout(doneDateLabel, doneDate));
    doneDateLabel.addClassName("font-size-s");
    doneDate.addClassName(DONE_DATE);
    VerticalLayout otherParameters = new VerticalLayout();
    layout.add(otherParameters);
    otherParameters.setPadding(false);
    otherParameters.add(exportParametersForm);
    otherParameters.add(searchParametersForm);
    otherParameters.add(proteinParametersForm);
    otherParameters.add(peptideParametersForm);
    otherParameters.add(queryParametersForm);
  }

  private VerticalLayout parameterLayout(Component... components) {
    VerticalLayout layout = new VerticalLayout();
    layout.setPadding(false);
    layout.setSpacing(false);
    layout.add(components);
    return layout;
  }

  @Override
  public void localeChange(LocaleChangeEvent event) {
    final MessageResource resources = new MessageResource(ExportJob.class, getLocale());
    fileLabel.setText(resources.message(FILE_PATH));
    inputLabel.setText(resources.message(INPUT));
    dateLabel.setText(resources.message(DATE));
    doneDateLabel.setText(resources.message(DONE_DATE));
  }

  public boolean isReadOnly() {
    return readOnly;
  }

  /**
   * Sets read only.
   *
   * @param readOnly
   *          read only
   */
  public void setReadOnly(boolean readOnly) {
    this.readOnly = readOnly;
    exportParametersForm.setReadOnly(readOnly);
    searchParametersForm.setReadOnly(readOnly);
    proteinParametersForm.setReadOnly(readOnly);
    peptideParametersForm.setReadOnly(readOnly);
    queryParametersForm.setReadOnly(readOnly);
  }

  public ExportJob getBean() {
    return job;
  }

  /**
   * Sets job.
   *
   * @param job
   *          job
   */
  public void setBean(ExportJob job) {
    this.job = job;
    file.setText(job.getFilePath());
    input.setText(job.getInput());
    date.setText(DateTimeFormatter.ISO_LOCAL_DATE.format(job.getDate()));
    doneDate.setText(
        job.getDoneDate() != null ? DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(job.getDoneDate())
            : "");
    exportParametersForm.setBean(job.getExportParameters());
    searchParametersForm.setBean(job.getSearchParameters());
    proteinParametersForm.setBean(job.getProteinParameters());
    peptideParametersForm.setBean(job.getPeptideParameters());
    queryParametersForm.setBean(job.getQueryParameters());
  }
}
