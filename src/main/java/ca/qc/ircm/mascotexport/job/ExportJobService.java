/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job;

import ca.qc.ircm.mascotexport.MascotConfiguration;
import ca.qc.ircm.mascotexport.mascot.MascotParser;
import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Services for jobs.
 */
@Service
public class ExportJobService {
  @SuppressWarnings("unused")
  private final Logger logger = LoggerFactory.getLogger(ExportJobService.class);
  @Inject
  private ExportJobRepository exportJobRepository;
  @Inject
  private MascotParser mascotParser;
  @Inject
  private MascotConfiguration mascotConfiguration;

  protected ExportJobService() {
  }

  protected ExportJobService(ExportJobRepository exportJobRepository, MascotParser mascotParser,
      MascotConfiguration mascotConfiguration) {
    this.exportJobRepository = exportJobRepository;
    this.mascotParser = mascotParser;
    this.mascotConfiguration = mascotConfiguration;
  }

  /**
   * Selects a job from database.
   *
   * @param id
   *          job's id
   * @return job
   */
  public ExportJob get(String id) {
    return id != null ? exportJobRepository.findById(id).orElse(null) : null;
  }

  /**
   * Selects all jobs from database.
   *
   * @return all jobs
   */
  public List<ExportJob> all() {
    return exportJobRepository.findAll();
  }

  /**
   * Selects all jobs to execute from database.
   *
   * @return all jobs to execute
   */
  public List<ExportJob> allToExecute() {
    return exportJobRepository.findByDoneDateNull();
  }

  /**
   * Returns input file name of job. <br>
   * {@link Optional#empty()} is returned if input filename cannot be determined. This may be cause
   * by an IOExcpetion or missing parameter in Mascot DAT file.
   *
   * @param job
   *          job
   * @return input file name of job
   */
  public Optional<String> inputFilename(ExportJob job) {
    Path file = mascotConfiguration.getData().resolve(job.getFilePath());
    try {
      return Optional.of(mascotParser.parseFilename(file));
    } catch (IOException e) {
      return Optional.empty();
    }
  }

  /**
   * Inserts job in database.
   *
   * @param job
   *          job to insert
   */
  public void insert(ExportJob job) {
    job.setDate(LocalDateTime.now());
    exportJobRepository.save(job);
  }

  /**
   * Inserts jobs in database.
   *
   * @param jobs
   *          jobs to insert
   */
  public void insert(Collection<ExportJob> jobs) {
    jobs.forEach(job -> job.setDate(LocalDateTime.now()));
    exportJobRepository.saveAll(jobs);
  }

  /**
   * Updates job's done date to mark it as done in database.
   *
   * @param job
   *          job
   */
  public void jobDone(ExportJob job) {
    job.setDoneDate(LocalDateTime.now());
    exportJobRepository.save(job);
  }

  /**
   * Deletes job from database.
   *
   * @param job
   *          job to delete
   */
  public void delete(ExportJob job) {
    exportJobRepository.delete(job);
  }

  /**
   * Deletes jobs from database.
   *
   * @param jobs
   *          jobs to delete
   */
  public void delete(Collection<ExportJob> jobs) {
    exportJobRepository.deleteAll(jobs);
  }
}
