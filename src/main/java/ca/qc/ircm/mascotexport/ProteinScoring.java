/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport;

/**
 * Choices of protein scoring.
 */
public enum ProteinScoring {
  STANDARD("99999999"), MUDPIT("0.000000001");
  /**
   * Protein scoring on Mascot server.
   */
  public final String value;

  ProteinScoring(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

  /**
   * Returns {@link ProteinScoring} matching mascot value.
   *
   * @param value
   *          mascot value
   * @return {@link ProteinScoring} matching mascot value
   */
  public static ProteinScoring fromMascotValue(String value) {
    for (ProteinScoring proteinScoring : ProteinScoring.values()) {
      if (proteinScoring.value.equals(value)) {
        return proteinScoring;
      }
    }
    return null;
  }
}
