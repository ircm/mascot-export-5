/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.web;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.notification.Notification;

/**
 * Shows error messages.
 *
 * <p>
 * Shows error for {@value #DEFAULT_DURATION} milliseconds and is positioned at
 * {@link #DEFAULT_POSITION} by default.
 * </p>
 */
public class ErrorNotification extends Notification {
  public static final int DEFAULT_DURATION = 2000;
  public static final Position DEFAULT_POSITION = Position.BOTTOM_START;
  private static final long serialVersionUID = -5385468777913151394L;
  protected Div label = new Div();

  /**
   * Creates an error notification.
   */
  public ErrorNotification() {
    label.addClassName("error-text");
    add(label);
    setDuration(DEFAULT_DURATION);
    setPosition(DEFAULT_POSITION);
  }

  /**
   * Creates an error notification.
   *
   * @param text
   *          error message
   */
  public ErrorNotification(String text) {
    this();
    setText(text);
  }

  /**
   * Creates an error notification.
   *
   * @param text
   *          error message
   * @param duration
   *          number of milliseconds to show error
   */
  public ErrorNotification(String text, int duration) {
    this();
    setText(text);
    setDuration(duration);
  }

  /**
   * Creates an error notification.
   *
   * @param text
   *          error message
   * @param duration
   *          number of milliseconds to show error
   * @param position
   *          position of notification
   */
  public ErrorNotification(String text, int duration, Position position) {
    this();
    setText(text);
    setDuration(duration);
    setPosition(position);
  }

  public String getText() {
    return label.getText();
  }

  @Override
  public void setText(String text) {
    label.setText(text);
  }
}
