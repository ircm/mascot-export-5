/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.mascot;

import java.nio.file.Path;

/**
 * Mascot search result file.
 */
public class MascotFile {
  /**
   * Search result file.
   */
  public Path file;
  /**
   * Input file (often RAW) used in Mascot search.
   */
  public String inputFile;

  public MascotFile() {
  }

  public MascotFile(Path file, String inputFile) {
    this.file = file;
    this.inputFile = inputFile;
  }

  @Override
  public String toString() {
    return "MascotFile [file=" + file + ", inputFile=" + inputFile + "]";
  }
}
