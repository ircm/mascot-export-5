/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.mascot;

import ca.qc.ircm.mascotexport.MascotConfiguration;
import ca.qc.ircm.mascotexport.MascotExportConfiguration;
import ca.qc.ircm.mascotexport.exec.ExecutorProvider;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.Executor;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Connects to Mascot installation.
 */
@Component
public class MascotService {
  private static final String PARAMETER_FORMAT = "{0}={1}";
  private static final String FILE_PARAMETER = "file";
  private final Logger logger = LoggerFactory.getLogger(MascotService.class);
  @Inject
  private MascotConfiguration mascotConfiguration;
  @Inject
  private MascotParser mascotParser;
  @Inject
  private MascotExportConfiguration mascotExportConfiguration;
  @Inject
  private ExecutorProvider executorProvider;

  protected MascotService() {
  }

  protected MascotService(MascotConfiguration mascotConfiguration, MascotParser mascotParser,
      MascotExportConfiguration mascotExportConfiguration, ExecutorProvider executorProvider) {
    this.mascotConfiguration = mascotConfiguration;
    this.mascotParser = mascotParser;
    this.mascotExportConfiguration = mascotExportConfiguration;
    this.executorProvider = executorProvider;
  }

  /**
   * Returns all Mascot result files.
   *
   * @return all Mascot result files
   * @throws IOException
   *           Could find files
   */
  public List<Path> allFiles() throws IOException {
    Path root = mascotConfiguration.getData();
    if (Files.exists(root)) {
      return Files.walk(root, FileVisitOption.FOLLOW_LINKS)
          .filter(p -> p.getFileName().toString().endsWith(".dat"))
          .map(file -> root.relativize(file)).collect(Collectors.toList());
    } else {
      return new ArrayList<>();
    }
  }

  /**
   * Returns all Mascot search result files.
   *
   * @return all Mascot search result files
   * @throws IOException
   *           Could find files
   */
  public List<MascotFile> allFilesWithInput() throws IOException {
    Path root = mascotConfiguration.getData();
    List<Path> files = allFiles();
    List<MascotFile> mascotFiles = new ArrayList<>(files.size());
    for (Path file : files) {
      try {
        MascotFile mascotFile = new MascotFile();
        mascotFile.file = file;
        mascotFile.inputFile = mascotParser.parseFilename(root.resolve(file));
        mascotFiles.add(mascotFile);
      } catch (IOException e) {
        logger.info("Could not parse file {}", file, e);
      }
    }
    return mascotFiles;
  }

  /**
   * Returns all Mascot result files' path.
   *
   * @return all Mascot result files' path
   * @throws IOException
   *           Could find files
   */
  public List<String> allFilesPath() throws IOException {
    List<Path> files = allFiles();
    List<String> paths = new ArrayList<>();
    for (Path file : files) {
      paths.add(FilenameUtils.normalize(file.toString(), true));
    }
    return paths;
  }

  /**
   * Calls Mascot executable to export file using parameters.
   *
   * @param file
   *          Mascot dat file to export
   * @param parameters
   *          parameters
   * @param exported
   *          exported content is written to this output
   * @throws IOException
   *           could not export file
   * @throws InterruptedException
   *           export was interrupted
   */
  public void export(Path file, Map<String, List<String>> parameters, OutputStream exported)
      throws IOException, InterruptedException {
    Executor executor = executorProvider.get();
    Path exportWorkingDirectory = mascotExportConfiguration.getWorking();
    executor.setWorkingDirectory(exportWorkingDirectory.toFile());
    executor.setStreamHandler(new PumpStreamHandler(exported, System.err));
    Path executable = mascotExportConfiguration.getExecutable();
    List<String> arguments = mascotExportConfiguration.getArgs();
    CommandLine commandLine = new CommandLine(executable.toString());
    for (String argument : arguments) {
      commandLine.addArgument(argument);
    }
    commandLine.addArgument(MessageFormat.format(PARAMETER_FORMAT, FILE_PARAMETER,
        FilenameUtils.separatorsToUnix(file.toString())));
    for (Map.Entry<String, List<String>> entry : parameters.entrySet()) {
      for (String value : entry.getValue()) {
        commandLine.addArgument(MessageFormat.format(PARAMETER_FORMAT, entry.getKey(), value));
      }
    }
    logger.info("Executing: {} in directory {}", commandLine, executor.getWorkingDirectory());
    executor.setWatchdog(new ExecuteWatchdog(ExecuteWatchdog.INFINITE_TIMEOUT));
    DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
    executor.execute(commandLine, resultHandler);
    try {
      resultHandler.waitFor();
      logger.debug("Finished: {} in directory {}, exit {}", commandLine,
          executor.getWorkingDirectory(), resultHandler.getExitValue());
      if (executor.isFailure(resultHandler.getExitValue())) {
        throw new IOException(executable + " did not complete normally");
      }
    } catch (InterruptedException exception) {
      logger.debug("Interrupted: {} in directory {}", commandLine, executor.getWorkingDirectory());
      executor.getWatchdog().destroyProcess();
      throw exception;
    }
  }
}
