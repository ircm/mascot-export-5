/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.task;

import ca.qc.ircm.task.Command;
import ca.qc.ircm.task.ExecutorTask;
import ca.qc.ircm.task.Task;
import ca.qc.ircm.task.TaskContext;
import java.util.concurrent.ExecutorService;
import org.springframework.stereotype.Component;

/**
 * Creates instances of {@link Task}.
 */
@Component
public class TaskFactory {
  /**
   * Creates a new task that uses {@link ExecutorService} to run {@link Command Command's} methods.
   *
   * @param command
   *          command
   * @param context
   *          task's context
   * @param executor
   *          executor
   * @return new task
   */
  public Task create(Command command, TaskContext context, ExecutorService executor) {
    Task task = new ExecutorTask(command, executor, context);
    return task;
  }
}
