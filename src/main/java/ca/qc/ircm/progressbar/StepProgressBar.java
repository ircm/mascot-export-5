/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.progressbar;

class StepProgressBar implements ProgressBar {
  private final ProgressBar parent;
  private final double startProgress;
  private final double step;
  private double progress;

  StepProgressBar(ProgressBar parent, double startProgress, double step) {
    this.parent = parent;
    this.startProgress = startProgress;
    this.step = step;
  }

  @Override
  public double getProgress() {
    return progress;
  }

  @Override
  public void setProgress(double progress) {
    progress = Math.max(0.0, progress);
    progress = Math.min(1.0, progress);
    this.progress = progress;
    parent.setProgress(startProgress + progress * step);
  }

  @Override
  public String getMessage() {
    return parent.getMessage();
  }

  @Override
  public void setMessage(String message) {
    parent.setMessage(message);
  }

  @Override
  public String getTitle() {
    return parent.getTitle();
  }

  @Override
  public void setTitle(String title) {
    parent.setTitle(title);
  }

  @Override
  public ProgressBar step(double step) {
    return new StepProgressBar(this, progress, step);
  }
}