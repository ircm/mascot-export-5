# mascot-export

Web application that simplifies exportation of multiple Mascot files.


## Repo moved to github

https://github.com/IRCM/mascot-export


## Build and installation

1. Clone the project: `git clone https://bitbucket.org/ircm/mascot-export.git`.
2. Build the project: `mvn package`. You may add `-DskipTests` parameter to skip tests for faster build.
3. Run the war or install as a service.


## Configuration

Before running the war, create an application.yml file in the same folder as the war file.

**You must set the location of the Mascot Server directory under the mascot section.**
```
mascot:
  home: ${user.home}/mascot
```

*You can set the port of the application, defaults to 8080.*
```
server:
  port: 8080
```

### Database - MongoDB is required
*You can change the database settings in the application.yml file.*
The configuration will look like this.
```
spring:
  data:
    mongodb:
      host: localhost
      port: 27017
      database: mascot-export
```
See [https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#boot-features-connecting-to-mongodb](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#boot-features-connecting-to-mongodb)

### Other
Other configuration properties have reasonable default values that should work in most cases.


## Running

You can start the war directly or by using the java command line.
```
java -jar mascot-export.war
```

### As a service or cloud
See [http://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#deployment](http://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#deployment)

